﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField]
    private float m_TopLimit;
    [SerializeField]
    private float m_BottomLimit;
    [SerializeField]
    private float m_Speed;

    [Header("Shooting")]
    [SerializeField]
    private GameObject m_BulletOrigin;
    [SerializeField]
    private float m_BulletIncRate;
    [SerializeField]
    private float[] m_RatesBetweenBullets;
    [SerializeField]
    private int[] m_RateModifierScores;

    private float m_BulletRate;
    private float m_CurrentRateBetweenBullets;

    [Header("UI")]
    [SerializeField]
    private Text m_Score;
    [SerializeField]
    private Image[] m_Lives;
    [SerializeField]
    private Text m_FireRateUpdateText;
    [SerializeField]
    private float m_MessageTime;

    private int m_DamageTaken;
    public int DamageTaken { get { return m_DamageTaken; } }

    private int m_CurrentScore;
    public int CurrentScore { get { return m_CurrentScore; } set { m_CurrentScore = value; } }
    
    [SerializeField]
    private bool m_ShouldShowFireRateUpdateMessage;

    private GameOver m_GameOverAccess;

    private bool m_IsFlat = true;

    void Start()
    {
        m_CurrentRateBetweenBullets = m_RatesBetweenBullets[0];
        m_FireRateUpdateText.gameObject.SetActive(false);
        m_GameOverAccess = GameObject.Find("GameOverController").GetComponent<GameOver>();
    }

    void Update()
    {
        m_Score.text = "Pontos: " + m_CurrentScore.ToString();

        if (m_ShouldShowFireRateUpdateMessage)
        {
            ShowSomeUIMessage(m_FireRateUpdateText.gameObject, true);
            StartCoroutine(DisableMessage(m_FireRateUpdateText.gameObject));
        }
        else
        {
            ShowSomeUIMessage(m_FireRateUpdateText.gameObject, false);
        }

        if (m_CurrentScore < 0)
        {
            m_CurrentScore = 0;
        }

        if (m_CurrentScore == m_RateModifierScores[0])
        {
            ChangeBulletRate(1);
        }
        if (m_CurrentScore == m_RateModifierScores[1])
        {
            ChangeBulletRate(2);
        }
        if (m_CurrentScore == m_RateModifierScores[2])
        {
            ChangeBulletRate(3);
        }

        switch (m_DamageTaken)
        {
            case 1:
                m_Lives[0].gameObject.SetActive(false);
                break;
            case 2:
                m_Lives[1].gameObject.SetActive(false);
                break;
            case 3:
                m_Lives[2].gameObject.SetActive(false);
                break;
        }

        float actualSpeed = m_Speed * Time.deltaTime;
        m_BulletRate += m_BulletIncRate;

        //Mobile Inputs
        

        if (!m_GameOverAccess.IsGameOver)
        {
            //-------------------------------------------MOVING WITH TOUCH--------------------------------------------------------
            //if (Input.touchCount > 0)
            //{
            //    Touch touch = Input.GetTouch(0);
            //    Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            //    touchPosition.z = 0.0f;

            //    if (touchPosition.x < 0.0f && touchPosition.y > 0.0f)
            //    {
            //        transform.position += Vector3.up * actualSpeed;
            //    }

            //    if (touchPosition.x < 0.0f && touchPosition.y < 0.0f)
            //    {
            //        transform.position -= Vector3.up * actualSpeed;
            //    }

            //    if (touchPosition.x > 0.0f)
            //    {
            //        Shoot();
            //    }
            //}
            //if (Input.touchCount > 1)
            //{
            //    Touch secondTouch = Input.GetTouch(1);
            //    Vector3 secondTouchPosition = Camera.main.ScreenToWorldPoint(secondTouch.position);
            //    secondTouchPosition.z = 0.0f;

            //    if (secondTouchPosition.x < 0.0f && secondTouchPosition.y > 0.0f)
            //    {
            //        transform.position += Vector3.up * actualSpeed;
            //    }

            //    if (secondTouchPosition.x < 0.0f && secondTouchPosition.y < 0.0f)
            //    {
            //        transform.position -= Vector3.up * actualSpeed;
            //    }

            //    if (secondTouchPosition.x > 0.0f)
            //    {
            //        Shoot();
            //    }
            //}

            //-----------------------------------------------------MOVING WITH ACCELERATION(MOVING THE DEVICE)--------------------------
            Vector3 dir = Input.acceleration;
            if (m_IsFlat)
            {
                dir = Quaternion.Euler(45.0f, 0.0f, 0.0f) * dir;
            }

            transform.Translate(0.0f, dir.y * actualSpeed, 0.0f);

            if (Input.touchCount > 0)
            {
                Shoot();
            }

            //Keyboard Inputs for testing
            transform.position += Vector3.up * Input.GetAxis("Vertical") * actualSpeed;

            if (Input.GetKey(KeyCode.Space))
            {
                Shoot();
            }
        }
    }

    void Shoot()
    {
        if (m_BulletRate > m_CurrentRateBetweenBullets)
        {
            GameObject bullet = ObjectPool.SharedInstance.GetPooledObject("Bullet");
            if (bullet != null)
            {
                bullet.transform.position = m_BulletOrigin.transform.position;
                bullet.transform.rotation = m_BulletOrigin.transform.rotation;
                bullet.SetActive(true);
                m_BulletRate = 0.0f;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("RookieEnemy") || other.gameObject.CompareTag("SoldierEnemy") || other.gameObject.CompareTag("CommanderEnemy"))
        {
            ++m_DamageTaken;
        }
    }

    void ChangeBulletRate(int newRateIndex)
    {
        m_CurrentRateBetweenBullets = m_RatesBetweenBullets[newRateIndex];
        m_ShouldShowFireRateUpdateMessage = true;
    }

    void ShowSomeUIMessage(GameObject obj, bool isActivated)
    {
        obj.SetActive(isActivated);
    }

    IEnumerator DisableMessage(GameObject message)
    {
        yield return new WaitForSeconds(m_MessageTime);
        m_ShouldShowFireRateUpdateMessage = false;
    }
}
