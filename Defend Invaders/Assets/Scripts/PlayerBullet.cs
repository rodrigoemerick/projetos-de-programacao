﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;

    void OnEnable() //nos objetos que são usados pelo ObjectPooler, é melhor usar OnEnable() em vez de Start()
    {

    }

    void Update()
    {
        transform.position += Vector3.right * m_Speed * Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("RookieEnemy") || other.gameObject.CompareTag("SoldierEnemy") || other.gameObject.CompareTag("CommanderEnemy"))
        {
            gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("GameArea"))
        {
            gameObject.SetActive(false);
        }
    }
}