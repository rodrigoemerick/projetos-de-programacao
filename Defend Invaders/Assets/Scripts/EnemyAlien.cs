﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAlien : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField]
    protected float m_Speed;
    [SerializeField]
    private float m_DefeatedSpeed;

    [Header("UI")]
    [SerializeField]
    private int m_InitialHP;
    [SerializeField]
    private int m_AlienHP;
    [SerializeField]
    private Text m_CurrentHP;

    [Header("Damage")]
    [SerializeField]
    private int[] m_PossibleDamages;
    [SerializeField]
    private GameObject m_Explosion;
    [SerializeField]
    private AudioSource m_AudioSource;

    private bool m_CanPlayAudio;

    private bool m_IgnorePlayerCollision;

    private int m_DamageToBarrier;
    public int DamageToBarrier { get { return m_DamageToBarrier; } }

    protected Player m_PlayerAccess;
    private GameOver m_GameOverAccess;

    protected virtual void OnEnable() //nos objetos que são usados pelo ObjectPooler, é melhor usar OnEnable() em vez de Start()
    {
        m_AlienHP = m_InitialHP;
        m_DamageToBarrier = Random.Range(m_PossibleDamages[0], m_PossibleDamages[m_PossibleDamages.Length - 1]);
        m_PlayerAccess = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        m_CanPlayAudio = true;
        m_GameOverAccess = GameObject.Find("GameOverController").GetComponent<GameOver>();
    }
    
    protected virtual void Update()
    {
        Physics2D.IgnoreLayerCollision(8, 9, true);
        Physics2D.IgnoreLayerCollision(9, 9, true);

        m_CurrentHP.text = m_AlienHP.ToString();

        if (m_AlienHP <= 0)
        {
            m_AlienHP = 0;
            Physics2D.IgnoreLayerCollision(9, 11, true);
            if (!m_AudioSource.isPlaying && m_CanPlayAudio)
            {
                m_AudioSource.Play();
                m_CanPlayAudio = false;
            }
        }
        else
        {
            Physics2D.IgnoreLayerCollision(9, 11, false);
        }
        if (!m_GameOverAccess.IsGameOver)
        {
            if (m_AlienHP > 0)
            {
                transform.position += Vector3.left * m_Speed * Time.deltaTime;        
            }
            else
            {
                transform.position += Vector3.down * m_DefeatedSpeed * Time.deltaTime;
            }
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            --m_AlienHP;
        }

        if (other.gameObject.CompareTag("Player"))
        {
            if (!m_IgnorePlayerCollision)
            {
                DesactivateIfCollided();
                Instantiate(m_Explosion, other.transform.position, Quaternion.identity);
            }
        }

        if (other.gameObject.CompareTag("Barrier"))
        {
            DesactivateIfCollided();
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("CollisionBlocker"))
        {
            m_IgnorePlayerCollision = true;
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("GameArea"))
        {
            ++GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().CurrentScore;
            gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("CollisionBlocker"))
        {
            m_IgnorePlayerCollision = false;
        }
    }

    protected virtual void OnDisable()
    {
        m_IgnorePlayerCollision = false;
    }

    void DesactivateIfCollided()
    {
        --GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().CurrentScore; //garante que um ponto não seja contabilizado ao colidir com a Barreira e o Player
        gameObject.SetActive(false);
    }
}