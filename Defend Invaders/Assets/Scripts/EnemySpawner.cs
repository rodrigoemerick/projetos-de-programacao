﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    private GameOver m_GameOverAccess;

    [Header("Rookie Alien")]
    [SerializeField]
    private float[] m_RookiesSpawnRates;
    [SerializeField]
    private float[] m_SpawnLimits;
    [SerializeField]
    private int[] m_RookieRateModifierScores;

    private float m_CurrentRookieSpawnRate;

    private Player m_PlayerAccess;

    private bool m_ShouldSpawnRookie = true;

    [Header("Soldier Alien")]
    [SerializeField]
    private float[] m_SoldiersSpawnRates;
    [SerializeField]
    private int[] m_SoldierRateModifierScores;

    private float m_CurrentSoldierSpawnRate;

    private bool m_ShouldSpawnSoldier;
    private bool m_CanStartSpawnSoldier = true;

    [Header("Commander Alien")]
    [SerializeField]
    private float[] m_CommandersSpawnRates;
    [SerializeField]
    private int[] m_CommanderRateModifierScores;

    private float m_CurrentCommanderSpawnRate;

    private bool m_ShouldSpawnCommander;
    private bool m_CanStartSpawnCommander = true;

    [Header("UI Messages")]
    [SerializeField]
    private Text m_SoldierEnemyArrivalText;
    [SerializeField]
    private Text m_CommanderEnemyArrivalText;
    [SerializeField]
    private float m_MessagesTime;

    void Start()
    {
        m_PlayerAccess = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        m_GameOverAccess = GameObject.Find("GameOverController").GetComponent<GameOver>();
        m_CurrentRookieSpawnRate = m_RookiesSpawnRates[0];
        StartCoroutine(SpawnRookie());
    }
    
    void Update()
    {
        //Rookies
        if (m_PlayerAccess.CurrentScore == m_RookieRateModifierScores[0])
        {
            ChangeRookieSpawnRate(1);
        }
        if (m_PlayerAccess.CurrentScore == m_RookieRateModifierScores[1])
        {
            ChangeRookieSpawnRate(2);
        }
        if (m_PlayerAccess.CurrentScore == m_RookieRateModifierScores[2])
        {
            ChangeRookieSpawnRate(3);
        }

        //Soldiers
        if (m_PlayerAccess.CurrentScore == m_SoldierRateModifierScores[0] && m_CanStartSpawnSoldier)
        {
            m_ShouldSpawnSoldier = true;
            ShowSomeUIMessage(m_SoldierEnemyArrivalText.gameObject, true);
            StartCoroutine(DisableNewEnemyMessage(m_SoldierEnemyArrivalText.gameObject));
            StartCoroutine(SpawnSoldier());
            //ChangeSoldierSpawnRate(1);
            m_CanStartSpawnSoldier = false;
        }
        if (m_PlayerAccess.CurrentScore == m_SoldierRateModifierScores[0])
        {
            ChangeSoldierSpawnRate(1);
        }
        if (m_PlayerAccess.CurrentScore == m_SoldierRateModifierScores[1])
        {
            ChangeSoldierSpawnRate(2);
        }

        //Commanders
        if (m_PlayerAccess.CurrentScore == m_CommanderRateModifierScores[0] && m_CanStartSpawnCommander)
        {
            m_ShouldSpawnCommander = true;
            ShowSomeUIMessage(m_CommanderEnemyArrivalText.gameObject, true);
            StartCoroutine(DisableNewEnemyMessage(m_CommanderEnemyArrivalText.gameObject));
            StartCoroutine(SpawnCommander());
            //ChangeCommanderSpawnRate(1);
            m_CanStartSpawnCommander = false;
        }
        if (m_PlayerAccess.CurrentScore == m_SoldierRateModifierScores[0])
        {
            ChangeCommanderSpawnRate(1);
        }
    }

    void ChangeRookieSpawnRate(int newRateIndex)
    {
        m_CurrentRookieSpawnRate = m_RookiesSpawnRates[newRateIndex];
    }

    void ChangeSoldierSpawnRate(int newRateIndex)
    {
        m_CurrentSoldierSpawnRate = m_SoldiersSpawnRates[newRateIndex];
    }

    void ChangeCommanderSpawnRate(int newRateIndex)
    {
        m_CurrentCommanderSpawnRate = m_CommandersSpawnRates[newRateIndex];
    }

    IEnumerator SpawnRookie()
    {
        yield return new WaitForSeconds(m_CurrentRookieSpawnRate);
        GameObject rookie = ObjectPool.SharedInstance.GetPooledObject("RookieEnemy");
        if (rookie != null)
        {
            rookie.transform.position = new Vector3(m_SpawnLimits[0], Random.Range(m_SpawnLimits[1], m_SpawnLimits[2]), 0.0f);
            rookie.transform.rotation = Quaternion.identity;
            rookie.SetActive(true);
        }

        if (m_ShouldSpawnRookie && !m_GameOverAccess.IsGameOver)
        {
            StartCoroutine(SpawnRookie());
        }
    }

    IEnumerator SpawnSoldier()
    {
        yield return new WaitForSeconds(m_CurrentSoldierSpawnRate);
        GameObject soldier = ObjectPool.SharedInstance.GetPooledObject("SoldierEnemy");
        if (soldier != null)
        {
            soldier.transform.position = new Vector3(m_SpawnLimits[0], Random.Range(m_SpawnLimits[1], m_SpawnLimits[2]), 0.0f);
            soldier.transform.rotation = Quaternion.identity;
            soldier.SetActive(true);
        }

        if (m_ShouldSpawnSoldier && !m_GameOverAccess.IsGameOver)
        {
            StartCoroutine(SpawnSoldier());
        }
    }

    IEnumerator SpawnCommander()
    {
        yield return new WaitForSeconds(m_CurrentCommanderSpawnRate);
        GameObject commander = ObjectPool.SharedInstance.GetPooledObject("CommanderEnemy");
        if (commander != null)
        {
            commander.transform.position = new Vector3(m_SpawnLimits[0], Random.Range(m_SpawnLimits[1], m_SpawnLimits[2]), 0.0f);
            commander.transform.rotation = Quaternion.identity;
            commander.SetActive(true);
        }

        if (m_ShouldSpawnCommander && !m_GameOverAccess.IsGameOver)
        {
            StartCoroutine(SpawnCommander());
        }
    }

    IEnumerator DisableNewEnemyMessage(GameObject message)
    {
        yield return new WaitForSeconds(m_MessagesTime);
        ShowSomeUIMessage(message, false);
    }

    public void ShowSomeUIMessage(GameObject obj, bool isActivated)
    {
        obj.SetActive(isActivated);
    }
}