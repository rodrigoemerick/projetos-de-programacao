﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommanderAlien : EnemyAlien
{
    [SerializeField]
    private float[] m_NewSpeeds;
    [SerializeField]
    private int[] m_SpeedModifiersScores;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void Update()
    {
        base.Update();

        if (m_PlayerAccess.CurrentScore > m_SpeedModifiersScores[0])
        {
            ChangeSpeed(0);
        }
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        base.OnTriggerExit2D(other);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    void ChangeSpeed(int index)
    {
        m_Speed = m_NewSpeeds[index];
    }
}
