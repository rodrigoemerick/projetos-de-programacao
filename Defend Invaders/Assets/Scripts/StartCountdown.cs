﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartCountdown : MonoBehaviour
{
    [SerializeField]
    private float m_Countdown;
    [SerializeField]
    private float m_CountdownRate;
    [SerializeField]
    private Text m_CountdownText;
    [SerializeField]
    private string m_GoText;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        m_Countdown -= m_CountdownRate * Time.deltaTime;

        if (m_Countdown > 0.0f)
        {
            m_CountdownText.text = Mathf.CeilToInt(m_Countdown).ToString();
        }
        else
        {
            m_CountdownText.text = m_GoText;
        }
    }

    public void DesactivateObject()
    {
        gameObject.SetActive(false);
    }
}
