﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class GameOver : MonoBehaviour
{
    [SerializeField]
    private Image m_GameOverScreen;
    [SerializeField]
    private Text m_MatchScore;
    [SerializeField]
    private Text m_HighScore;

    private Player m_PlayerAccess;
    private Barrier m_BarrierAccess;

    private string m_AdID = "3485427";
    private bool m_TestMode = true;
    private bool m_ShowedAd;

    private bool m_IsGameOver;
    public bool IsGameOver { get { return m_IsGameOver; } }

    private int m_RandomNum = -1;

    void Start()
    {
        m_IsGameOver = false;
        m_MatchScore.color = Color.white;
        m_HighScore.color = Color.yellow;
        m_PlayerAccess = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        m_BarrierAccess = GameObject.FindGameObjectWithTag("Barrier").GetComponent<Barrier>();
        m_HighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        m_ShowedAd = false;
        Advertisement.Initialize(m_AdID, m_TestMode);
    }
    
    void Update()
    {
        if (m_PlayerAccess.CurrentScore > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", m_PlayerAccess.CurrentScore);
            m_HighScore.color = Color.green;
        }
        else if (m_PlayerAccess.CurrentScore < PlayerPrefs.GetInt("HighScore", 0))
        {
            m_HighScore.color = Color.yellow;
        }

        m_MatchScore.text = "Pontos : " + m_PlayerAccess.CurrentScore.ToString();
        m_HighScore.text = "Melhor : " + PlayerPrefs.GetInt("HighScore").ToString();

        if (m_PlayerAccess.DamageTaken == 3 || m_BarrierAccess.BarrierHP <= 0)
        {
            m_IsGameOver = true;
        }

        if (!m_IsGameOver)
        {
            m_GameOverScreen.gameObject.SetActive(false);
        }
        else
        {
            if (!m_ShowedAd)
            {
                Advertisement.Show();
                m_ShowedAd = true;
            }
            m_GameOverScreen.gameObject.SetActive(true);
        }
    }
}
