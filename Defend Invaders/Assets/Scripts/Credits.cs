﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    [SerializeField]
    private Image m_Credits;

    public void ShowCredits()
    {
        if (!m_Credits.gameObject.activeSelf)
        {
            m_Credits.gameObject.SetActive(true);
        }
    }

    public void HideCredits()
    {
        if (m_Credits.gameObject.activeSelf)
        {
            m_Credits.gameObject.SetActive(false);
        }
    }
}
