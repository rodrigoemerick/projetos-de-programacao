﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem
{
    [SerializeField]
    private GameObject m_ObjectToPool;
    public GameObject ObjectToPool
    {
        get { return m_ObjectToPool; }
    }
    [SerializeField]
    private int m_AmountOfItens;
    public int AmountOfItens
    {
        get { return m_AmountOfItens; }
    }
    [SerializeField]
    private bool m_ShouldExpand;
    public bool ShouldExpand
    {
        get { return m_ShouldExpand; }
    }
}

public class ObjectPool : MonoBehaviour
{
    private static ObjectPool m_SharedInstance;
    public static ObjectPool SharedInstance
    {
        get { return m_SharedInstance; }
    }

    [SerializeField]
    private List<ObjectPoolItem> m_ItemsToPool;

    private List<GameObject> m_PooledObjects;

    void Awake()
    {
        m_SharedInstance = this;
    }

    void Start()
    {
        m_PooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in m_ItemsToPool)
        {
            for (int i = 0; i < item.AmountOfItens; ++i)
            {
                GameObject obj = (GameObject)Instantiate(item.ObjectToPool);
                obj.SetActive(false);
                m_PooledObjects.Add(obj);
            }
        }
    }

    public GameObject GetPooledObject(string gameObjectTag)
    {
        for (int i = 0; i < m_PooledObjects.Count; ++i)
        {
            if (!m_PooledObjects[i].activeInHierarchy && m_PooledObjects[i].CompareTag(gameObjectTag))
            {
                return m_PooledObjects[i];
            }
        }
        foreach (ObjectPoolItem item in m_ItemsToPool)
        {
            if (item.ObjectToPool.CompareTag(gameObjectTag))
            {
                if (item.ShouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.ObjectToPool);
                    obj.SetActive(false);
                    m_PooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }
}
