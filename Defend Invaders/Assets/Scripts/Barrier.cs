﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Barrier : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    private Text m_BarrierText;
    [SerializeField]
    private int m_BarrierHP;
    public int BarrierHP { get { return m_BarrierHP; } }
    [SerializeField]
    private int[] m_ColorParamsValues;
    [SerializeField]
    private GameObject m_Explosion;

    void Start()
    {
        
    }

    void Update()
    {
        m_BarrierText.text = "Barreira: " + m_BarrierHP.ToString() + "%";

        if (m_BarrierHP > m_ColorParamsValues[0])
        {
            m_BarrierText.color = Color.green;
        }
        if (m_BarrierHP > m_ColorParamsValues[1] && m_BarrierHP <= m_ColorParamsValues[0])
        {
            m_BarrierText.color = Color.yellow;
        }
        if (m_BarrierHP <= m_ColorParamsValues[1])
        {
            m_BarrierText.color = Color.red;
        }

        if (m_BarrierHP < 0)
        {
            m_BarrierHP = 0;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("RookieEnemy"))
        {
            m_BarrierHP -= other.gameObject.GetComponent<RookieAlien>().DamageToBarrier;
            Instantiate(m_Explosion, other.transform.position, Quaternion.identity);
        }
        if (other.gameObject.CompareTag("SoldierEnemy"))
        {
            m_BarrierHP -= other.gameObject.GetComponent<SoldierAlien>().DamageToBarrier;
            Instantiate(m_Explosion, other.transform.position, Quaternion.identity);
        }
        if (other.gameObject.CompareTag("CommanderEnemy"))
        {
            m_BarrierHP -= other.gameObject.GetComponent<CommanderAlien>().DamageToBarrier;
            Instantiate(m_Explosion, other.transform.position, Quaternion.identity);
        }
    }
}
