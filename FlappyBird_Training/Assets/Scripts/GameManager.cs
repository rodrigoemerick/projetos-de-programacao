﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Manager m_MSO;
    [SerializeField] private GameObject m_ObstacleGenerator;
    [SerializeField] private GameObject[] m_MenuUIElements;
    [SerializeField] private GameObject[] m_InGameUIElements;

    void Awake()
    {
        m_MSO.currentState = 1;
        m_ObstacleGenerator.SetActive(false);
    }

    void Update()
    {
        if (m_MSO.currentState == 1 && Input.GetKeyDown(KeyCode.Space))
        {
            m_MSO.currentState = 2;
        }

        switch (m_MSO.currentState)
        {
            case 1:
                for (int i = 0; i < m_InGameUIElements.Length; ++i)
                {
                    m_InGameUIElements[i].SetActive(false);
                }
                break;
            case 2:
                if (!m_ObstacleGenerator.activeSelf)
                {
                    m_ObstacleGenerator.SetActive(true);
                 
                    for (int i = 0; i < m_MenuUIElements.Length; ++i)
                    {
                        m_MenuUIElements[i].SetActive(false);
                    }
                }

                for (int i = 0; i < m_InGameUIElements.Length; ++i)
                {
                    m_InGameUIElements[i].SetActive(true);
                }
                break;
            case 3:
                m_ObstacleGenerator.SetActive(false);

                for (int i = 0; i < m_InGameUIElements.Length; ++i)
                {
                    m_InGameUIElements[i].SetActive(false);
                }
                break;
        }
    }
}