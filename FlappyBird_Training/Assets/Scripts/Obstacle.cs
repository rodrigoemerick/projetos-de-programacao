﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private float m_Speed;
    [SerializeField] private Manager m_MSO;

    void Update()
    {
        if (m_MSO.currentState == 2)
        {
            transform.position += Vector3.left * m_Speed * Time.deltaTime;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("GameArea"))
        {
            gameObject.SetActive(false);
        }
    }
}