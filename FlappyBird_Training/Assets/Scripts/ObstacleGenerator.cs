﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    [SerializeField] private float m_Rate;
    [SerializeField] private float[] m_YLimits;
    [SerializeField] private Transform m_Origin;
    [SerializeField] private Manager m_MSO;

    void OnEnable()
    {
        StartCoroutine(SpawnObstacle());
    }

    IEnumerator SpawnObstacle()
    {
        yield return new WaitForSeconds(m_Rate);
        GameObject obstacle = ObjectPooler.SharedInstance.GetPooledObject("ObstacleSafe");
        if (obstacle != null)
        {
            obstacle.transform.position = new Vector3(m_Origin.position.x, Random.Range(m_YLimits[0], m_YLimits[1]), 0.0f);
            obstacle.transform.rotation = Quaternion.identity;
            obstacle.SetActive(true);
        }

        if (m_MSO.currentState == 2)
        {
            StartCoroutine(SpawnObstacle());
        }
    }
}