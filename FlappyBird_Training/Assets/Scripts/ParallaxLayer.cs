﻿using UnityEngine;

public class ParallaxLayer : MonoBehaviour
{
    private Renderer m_Renderer;
    private float m_Velocity;

    [SerializeField] private BackgroundsParallax m_BPSO;
    [SerializeField] private Manager m_MSO;
    [SerializeField] private int m_Background;
    [SerializeField] private int m_Layer;

    void Start()
    {
        m_Renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        if (m_MSO.currentState != 3)
        {
            switch (m_Background)
            {
                case 1:
                    m_Velocity = m_BPSO.background1LayersSpeeds[m_Layer - 1];
                    break;
                case 2:
                    m_Velocity = m_BPSO.background2LayersSpeeds[m_Layer - 1];
                    break;
                case 3:
                    m_Velocity = m_BPSO.background3LayersSpeeds[m_Layer - 1];
                    break;
                case 4:
                    m_Velocity = m_BPSO.background4LayersSpeeds[m_Layer - 1];
                    break;
            }

            Vector2 offset = new Vector2(m_Velocity * Time.deltaTime, 0.0f);
            m_Renderer.material.mainTextureOffset += offset;
        }
    }
}