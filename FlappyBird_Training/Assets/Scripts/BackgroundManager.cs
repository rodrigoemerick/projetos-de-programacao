﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundManager : MonoBehaviour
{
    [SerializeField] private BackgroundsParallax m_BPSO;
    [SerializeField] private GameObject[] m_Backgrounds;
    [SerializeField] private GameObject m_IsInCurrentScenarioText;
    [SerializeField] private Button[] m_BackgroundButtons;
    [SerializeField] private float m_NotifyTime;

    public void ChangeBackground(int background)
    {
        if (background == m_BPSO.currentBackground)
        {
            StartCoroutine(NotifyIsInCurrentScenario());
        }

        for (int i = 0; i < m_Backgrounds.Length; ++i)
        {
            if (i == background)
            {
                m_Backgrounds[i].SetActive(true);
            }
            else
            {
                m_Backgrounds[i].SetActive(false);
            }
        }

        m_BPSO.currentBackground = background;
    }

    void SetNotifyMessage(bool isShowing)
    {
        m_IsInCurrentScenarioText.gameObject.SetActive(isShowing);
        for (int i = 0; i < m_BackgroundButtons.Length; ++i)
        {
            m_BackgroundButtons[i].interactable = !isShowing;
        }
    }

    IEnumerator NotifyIsInCurrentScenario()
    {
        SetNotifyMessage(true);
        yield return new WaitForSeconds(m_NotifyTime);
        SetNotifyMessage(false);
    }
}