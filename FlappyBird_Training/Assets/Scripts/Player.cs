﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private Manager m_MSO;
    [SerializeField] private float m_Thrust;
    [SerializeField] private float m_JumpTime;
    [SerializeField] private Text m_ScoreText;

    private int m_Score;
    Rigidbody2D m_Rb;

    void Start()
    {
        m_Rb = GetComponent<Rigidbody2D>();
        StartCoroutine(Jump(true));
        m_Score = 0;
    }

    void Update()
    {
        switch (m_MSO.currentState)
        {
            case 1:

                break;
            case 2:
                m_ScoreText.text = $"Score: {m_Score.ToString()}";
                StopAllCoroutines();

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    GoUp();
                }
                break;
            case 3:

                break;
        }
    }

    void GoUp()
    {
        m_Rb.velocity = new Vector2(m_Rb.velocity.x, m_Thrust);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Obstacle") || other.gameObject.CompareTag("Bottom"))
        {
            m_MSO.currentState = 3;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("ObstacleSafe"))
        {
            ++m_Score;
        }
    }

    IEnumerator Jump(bool firstTime)
    {
        if (firstTime)
        {
            GoUp();
        }

        yield return new WaitForSeconds(m_JumpTime);
        GoUp();

        if (m_MSO.currentState == 1)
        {
            StartCoroutine(Jump(false));
        }
    }
}