﻿using UnityEngine;

[CreateAssetMenu]
public class BackgroundsParallax : ScriptableObject
{
    public int currentBackground;
    public float[] background1LayersSpeeds;
    public float[] background2LayersSpeeds;
    public float[] background3LayersSpeeds;
    public float[] background4LayersSpeeds;
}