using UnityEngine;

[CreateAssetMenu(fileName = "Racer", menuName = "New Racer")]
public class Racer : ScriptableObject
{
    public string racerName;
    public string racerCharacter;
    public Sprite racerCharacterPhoto;
    public int racerPoints;
    public int[] racerHistoryPositions = new int[18];
    public int racerBestLapPoints;
    public int untieWeight;
}
