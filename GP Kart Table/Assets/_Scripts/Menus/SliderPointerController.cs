using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderPointerController : MonoBehaviour, IPointerUpHandler
{
    Slider slider;
    float oldValue;

    [SerializeField] private MenusManager menusManager;
    [SerializeField] private string sliderType;

    void Start()
    {
        
    }

    public void Init()
    {
        slider = GetComponent<Slider>();
        oldValue = slider.value;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (slider.value != oldValue)
        {
            switch (sliderType)
            {
                case "Music":
                    menusManager.SetMusicVolume(slider.value);
                    break;
                case "SFX":
                    menusManager.SetSFXVolume(slider.value);
                    break;
            }
            oldValue = slider.value;
        }
    }
}
