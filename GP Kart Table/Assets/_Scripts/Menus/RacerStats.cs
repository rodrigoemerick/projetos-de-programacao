using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RacerStats : MonoBehaviour
{
    public TMP_Text racerPositionText;
    public Image racerPhoto;
    public TMP_Text racerNameText;
    public TMP_Text racerPointsText;
}
