using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScaleButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private float scaleX;
    [SerializeField] private float scaleY;
    [SerializeField] private Sprite scaledButtonSprite;
    [SerializeField] private Sprite normalButtonSprite;
    [SerializeField] private Sprite mainBackgroundSprite;
    [SerializeField] private Image mainBackground;
    [SerializeField] private bool shouldChangeBackground;
    [SerializeField] private AudioSource pointerEnterAudio;

    public bool canInteract;

    Button button;

    void Start()
    {
        button = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (canInteract)
        {
            transform.localScale = new Vector2(scaleX, scaleY);
            button.image.sprite = scaledButtonSprite;
            pointerEnterAudio.Play();

            if (shouldChangeBackground)
            {
                mainBackground.sprite = mainBackgroundSprite;
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = new Vector2(1.0f, 1.0f);
        button.image.sprite = normalButtonSprite;
    }
}
