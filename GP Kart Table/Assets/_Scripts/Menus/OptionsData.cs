using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OptionsData
{
    public float musicSliderValue;
    public float sfxSliderValue;
}
