using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RacerData
{
    public string racerName;
    public string racerCharacter;
    public int racerPoints;
    public int[] racerHistoryPositions = new int[18];
    public int racerBestLapPoints;
}
