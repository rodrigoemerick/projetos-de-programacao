using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : MonoBehaviour
{
    public static Action OnFinishedSorting;

    [SerializeField] private MenusManager menusManager;
    [SerializeField] private Racer[] racers;
    public Racer[] Racers { get => racers; }
    [SerializeField] private RacerStats[] stats;
    public RacerStats[] Stats { get => stats; set => stats = value; }
    [SerializeField] private AudioSource cameraClickAudio;

    private string savePathJson;
    private string savePathScreenshots;

    private Racer[] currRaceClassification;
    public Racer[] CurrRaceClassification { get => currRaceClassification; }

    private bool[] runnersAbsence;
    public bool[] RunnersAbsence { get => runnersAbsence; set => runnersAbsence = value; }

    void OnEnable()
    {
        MenusManager.OnReceiveAllRacerPositions += UpdateRacerPoints;
        MenusManager.OnStartResultsInsertion += InitCurrRaceArray;
        MenusManager.OnReceiveBestLapRacer += UpdateBestLapRacerPoints;
    }

    void OnDisable()
    {
        MenusManager.OnReceiveAllRacerPositions -= UpdateRacerPoints;
        MenusManager.OnStartResultsInsertion -= InitCurrRaceArray;
        MenusManager.OnReceiveBestLapRacer -= UpdateBestLapRacerPoints;
    }

    void Start()
    {
        LoadFromJson();
        UpdateStatsTable();
        savePathScreenshots = Application.persistentDataPath + "/screenshot " + System.DateTime.Now.ToString("MM-dd-yy (HH-mm-ss)") + ".png";
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.E))
        {
            TakeTableScreenshot();
        }

#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.A))
        {
            ResetAllRacerPoints();
        }
#endif
    }

    void InitCurrRaceArray()
    {
        currRaceClassification = new Racer[18];
        runnersAbsence = new bool[18];
    }

    void UpdateRacerPoints(Dictionary<int, int> racerPositions)
    {
        foreach (KeyValuePair<int, int> pair in racerPositions)
        {
            ++racers[pair.Key].racerHistoryPositions[pair.Value - 1];

            racers[pair.Key].racerPoints = 0; //ZERAR TODA VEZ PARA HAVER A COMPUTA��O CERTA BASEADA NO HIST�RICO DE POSI��ES

            for (int i = 0; i < 11; i++)
            {
                if (racers[pair.Key].racerHistoryPositions[i] == 0 || runnersAbsence[pair.Key])
                {
                    continue;
                }

                switch (i)
                {
                    case 0:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 15;
                        break;
                    case 1:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 12;
                        break;
                    case 2:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 10;
                        break;
                    case 3:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 8;
                        break;
                    case 4:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 7;
                        break;
                    case 5:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 6;
                        break;
                    case 6:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 5;
                        break;
                    case 7:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 4;
                        break;
                    case 8:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 3;
                        break;
                    case 9:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 2;
                        break;
                    case 10:
                        racers[pair.Key].racerPoints += racers[pair.Key].racerHistoryPositions[i] * 1;
                        break;
                }
            }
        }
    }

    void UpdateBestLapRacerPoints(int racerPos)
    {
        if (racerPos >= 1 && racerPos <= 3)
        {
            currRaceClassification[racerPos - 1].racerBestLapPoints += 1;
        }
        else if (racerPos >= 4 && racerPos <= 7)
        {
            currRaceClassification[racerPos - 1].racerBestLapPoints += 3;
        }
        else if (racerPos >= 8 && racerPos <= 15)
        {
            currRaceClassification[racerPos - 1].racerBestLapPoints += 5;
        }
    }

    void UpdateStatsTable()
    {
        for (int i = 0; i < stats.Length; i++)
        {
            stats[i].racerPositionText.text = (i + 1).ToString();
            stats[i].racerPhoto.sprite = racers[i].racerCharacterPhoto;
            stats[i].racerNameText.text = racers[i].racerName;
            stats[i].racerPointsText.text = racers[i].racerPoints.ToString();
        }
    }

    void ApplyBestLapPoints()
    {
        for (int i = 0; i < racers.Length; i++)
        {
            if (racers[i].racerBestLapPoints != 0)
            {
                racers[i].racerPoints += racers[i].racerBestLapPoints;
            }
        }
    }

    void TakeTableScreenshot()
    {
        if (menusManager.CurrentMenu == 1)
        {
            ScreenCapture.CaptureScreenshot(savePathScreenshots);
            cameraClickAudio.Play();
        }
    }

    void SaveToJson()
    {
        for (int i = 0; i < racers.Length; i++)
        {
            RacerData data = new RacerData();
            data.racerName = racers[i].racerName;
            data.racerCharacter = racers[i].racerCharacter;
            data.racerHistoryPositions = racers[i].racerHistoryPositions;
            data.racerPoints = racers[i].racerPoints;
            data.racerBestLapPoints = racers[i].racerBestLapPoints;

            string persistentDataPath = Application.persistentDataPath + "/SaveData" + racers[i].racerCharacter + ".json";
            using StreamWriter writer = new StreamWriter(persistentDataPath);
            string json = JsonUtility.ToJson(data);
            writer.Write(json);
            writer.Close();
        }
    }

    void LoadFromJson()
    {
        for (int i = 0; i < racers.Length; i++)
        {
            if (File.Exists(Application.persistentDataPath + "/SaveData" + racers[i].racerCharacter + ".json"))
            {
                string persistentDataPath = Application.persistentDataPath + "/SaveData" + racers[i].racerCharacter + ".json";
                using StreamReader reader = new StreamReader(persistentDataPath);

                string json = reader.ReadToEnd();
                RacerData loadedRacer = JsonUtility.FromJson<RacerData>(json);

                racers[i].racerName = loadedRacer.racerName;
                racers[i].racerCharacter = loadedRacer.racerCharacter;
                racers[i].racerHistoryPositions = loadedRacer.racerHistoryPositions;
                racers[i].racerPoints = loadedRacer.racerPoints;
                racers[i].racerBestLapPoints = loadedRacer.racerBestLapPoints;

                reader.Close();
            }
            else
            {
                return;
            }
        }

        ApplyBestLapPoints();
        QuickSortPoints(racers, 0, racers.Length - 1);
        UntieRacers();
        UpdateStatsTable();
    }

    void ResetAllRacerPoints()
    {
        for (int i = 0; i < racers.Length; i++)
        {
            racers[i].racerPoints = 0;
            racers[i].racerBestLapPoints = 0;
            for (int j = 0; j < racers[i].racerHistoryPositions.Length; j++)
            {
                racers[i].racerHistoryPositions[j] = 0;
            }
        }
    }

    public void SortRacerByPoints()
    {
        SaveToJson();
        ApplyBestLapPoints();
        QuickSortPoints(racers, 0, racers.Length - 1);
        UntieRacers();
        UpdateStatsTable();
    }

    void UntieRacers()
    {
        for (int i = 0; i < racers.Length; i++)
        {
            racers[i].untieWeight = 0;

            for (int j = 0, w = (int)Math.Pow(2, 18); j < racers[i].racerHistoryPositions.Length; j++, w /= 2)
            {
                racers[i].untieWeight += w * racers[i].racerHistoryPositions[j];
            }
        }

        for (int i = 0; i < racers.Length - 1; i++)
        {
            if (racers[i].racerPoints == racers[i + 1].racerPoints)
            {
                int comparison = racers[i].untieWeight.CompareTo(racers[i + 1].untieWeight);
                if (comparison == -1)
                {
                    Racer temp = racers[i + 1];
                    racers[i + 1] = racers[i];
                    racers[i] = temp;
                }
            }
        }

        OnFinishedSorting?.Invoke();
    }

    public int FindLastAvailablePosition()
    {
        for (int i = currRaceClassification.Length - 1; i >= 0; i--)
        {
            if (currRaceClassification[i] == null)
            {
                return i;
            }
        }

        return -1;
    }

    void QuickSortPoints(Racer[] arr, int left, int right)
    {
        if (left < right)
        {
            int pivot = PartitionPoints(arr, left, right);

            QuickSortPoints(arr, left, pivot - 1);
            QuickSortPoints(arr, pivot + 1, right);
        }
    }

    private int PartitionPoints(Racer[] arr, int left, int right)
    {
        int pivot = arr[right].racerPoints;
        int i = left - 1;

        for (int j = left; j < right; j++)
        {
            if (arr[j].racerPoints >= pivot)
            {
                i++;
                Racer temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        Racer temp1 = arr[i + 1];
        arr[i + 1] = arr[right];
        arr[right] = temp1;

        return i + 1;
    }
}
