using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trophy : MonoBehaviour
{
    [SerializeField] private float rotationSpeed;

    Vector3 rotation = new Vector3(0.0f, 10.0f, 0.0f);

    void Update()
    {
        transform.Rotate(rotation * rotationSpeed * Time.deltaTime);        
    }
}
