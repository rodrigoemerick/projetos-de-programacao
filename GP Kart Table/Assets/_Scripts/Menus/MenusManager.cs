using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class MenusManager : MonoBehaviour
{
    public static Action<Dictionary<int, int>> OnReceiveAllRacerPositions;
    public static Action OnStartResultsInsertion;
    public static Action<int> OnReceiveBestLapRacer;

    private Dictionary<int, int> currRaceInsertionsDict;

    [SerializeField] TableManager tableManager;

    [Header("Coroutines times")]
    [SerializeField] private float logoScreenDuration;
    [SerializeField] private float afterPressedSpaceDuration;
    [SerializeField] private float waitToDisplayMainButtonsDuration;
    [SerializeField] private float waitToMainMenuOutDuration;
    [SerializeField] private float waitToDisplayReturnButtonDuration;
    [SerializeField] private float invalidPosTextDisplayDuration;
    [SerializeField] private float goToOtherMenuAudioFadeOutTime;
    [SerializeField] private float goToOtherMenuAudioFadeInTime;

    [Header("Audios")]
    [SerializeField] private AudioSource logoScreenAudio;
    [SerializeField] private AudioClip[] characterVoices;
    [SerializeField] private AudioSource characterVoiceAudio;
    [SerializeField] private AudioClip[] mainMenuMusics;
    [SerializeField] private AudioSource mainMenuAudio;
    [SerializeField] private AudioSource pressSpaceAudio;
    [SerializeField] private AudioClip tableMenuClip;
    [SerializeField] private AudioClip insertMenuClip;
    [SerializeField] private AudioClip optionsMenuClip;

    [Header("Animators")]
    [SerializeField] private Animator gpLogoAnimator;
    [SerializeField] private Animator whiteFadeAnimator;
    [SerializeField] private Animator pressSpaceAnimator;
    [SerializeField] private Animator mainMenuGPLogoAnimator;
    [SerializeField] private Animator mainButtonsBackgroundAnimator;
    [SerializeField] private Animator mainButtonsAnimator;
    [SerializeField] private Animator trophyAnimator;
    [SerializeField] private Animator returnButtonAnimator;
    [SerializeField] private Animator resultsMenuAnimator;
    [SerializeField] private Animator insertResultsMenuAnimator;
    [SerializeField] private Animator optionsMenuAnimator;

    [Header("UI")]
    [SerializeField] private Image mainBackgroundImage;
    [SerializeField] private Sprite mainMenuBackground;
    [SerializeField] private Sprite[] tableMenuBackgrounds;
    [SerializeField] private Sprite insertResultsMenuBackground;
    [SerializeField] private Sprite optionsMenuBackground;
    [SerializeField] private GameObject quitMenu;

    [Header("Insert Results Objects")]
    [SerializeField] private GameObject areYouSureMenu;
    [SerializeField] private GameObject insertEditor;
    [SerializeField] private GameObject finishInsertButton;
    [SerializeField] private TMP_InputField racerPosInputField;
    [SerializeField] private TMP_Text invalidPosText;
    [SerializeField] private TMP_Text whichPositionText;
    [SerializeField] private Button goToNextButton;
    [SerializeField] private Button cancelInsertionButton;
    [SerializeField] private Toggle runnerAbsenceToggle;
    private int currEditPos;

    [Header("Options Menu")]
    [SerializeField] private TMP_Text copiedToClipboardText;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider SFXSlider;
    [SerializeField] private AudioMixer mixer;
    private System.Random random;
    private bool isInFirstMenu;

    private int currentMenu;
    public int CurrentMenu { get => currentMenu; }

    private Coroutine logoScreenCoroutine;

    void OnEnable()
    {
        TableManager.OnFinishedSorting += EnableFinishResultsButton;
    }

    void OnDisable()
    {
        TableManager.OnFinishedSorting -= EnableFinishResultsButton;
    }

    void Start()
    {
        currentMenu = -1;
        random = new System.Random();
        logoScreenCoroutine = StartCoroutine(LogoScreen());
        LoadOptionsFromJson();
        musicSlider.GetComponent<SliderPointerController>().Init();
        SFXSlider.GetComponent<SliderPointerController>().Init();
        currRaceInsertionsDict = new Dictionary<int, int>();
    }

    void Update()
    {
        if (isInFirstMenu && Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(PressedSpaceOnMainMenu());
            isInFirstMenu = false;
        }

        if (insertEditor.activeInHierarchy)
        {
            goToNextButton.interactable = racerPosInputField.text != "" || runnerAbsenceToggle.isOn;
        }

        if (currentMenu == 0 && Input.GetKeyDown(KeyCode.Escape))
        {
            quitMenu.SetActive(true);
        } 

        if (currentMenu == -1 && Input.GetKeyDown(KeyCode.Space))
        {
            StopCoroutine(logoScreenCoroutine);
            currentMenu = 0;
            logoScreenAudio.Stop();
            StartCoroutine(PlayCharacterVoiceAndGoToFirstMenu());
            isInFirstMenu = true;
        }
    }

    public void GoFromMainMenuToTableMenu()
    {
        currentMenu = 1;
        StartCoroutine(MenuToTable());
    }

    public void GoFromMainMenuToInsertResultsMenu()
    {
        currentMenu = 2;
        StartCoroutine(MenuToInsertResults());
    }

    public void GoFromMainMenuToOptionsMenu()
    {
        currentMenu = 3;
        StartCoroutine(MenuToOptionsResults());
    }

    public void GoToMainMenu()
    {
        switch (currentMenu)
        {
            case 1:
                trophyAnimator.SetTrigger("trophy_out");
                resultsMenuAnimator.SetTrigger("results_menu_out");
                returnButtonAnimator.SetTrigger("return_button_out");
                break;
            case 2:
                insertResultsMenuAnimator.SetTrigger("insert_results_out");
                break;
            case 3:
                optionsMenuAnimator.SetTrigger("options_menu_out");
                returnButtonAnimator.SetTrigger("return_button_out");
                break;
        }

        currentMenu = 0;
        StartCoroutine(ToMainMenu());
    }

    public void EnableInsertResultsEditor()
    {
        currRaceInsertionsDict.Clear();
        areYouSureMenu.SetActive(false);
        insertEditor.SetActive(true);
        goToNextButton.gameObject.SetActive(true);
        cancelInsertionButton.gameObject.SetActive(true);
        runnerAbsenceToggle.gameObject.SetActive(true);
        runnerAbsenceToggle.isOn = false;
        racerPosInputField.gameObject.SetActive(true);
        racerPosInputField.text = "";
        runnerAbsenceToggle.isOn = false;
        currEditPos = 0;
        OnStartResultsInsertion?.Invoke();
        whichPositionText.text = $"Em qual posi��o terminou o/a corredor/a {tableManager.Racers[currEditPos].racerName} ({tableManager.Racers[currEditPos].racerCharacter})";
    }

    public void GoToNextEdition()
    {
        if (currEditPos < 18)
        {
            string text = racerPosInputField.text.Trim((char)8203);

            if (runnerAbsenceToggle.isOn)
            {
                int pos = tableManager.FindLastAvailablePosition();

                if (pos != -1)
                {
                    currRaceInsertionsDict.Add(currEditPos, pos + 1);
                    tableManager.CurrRaceClassification[pos] = tableManager.Racers[currEditPos];
                    tableManager.RunnersAbsence[pos] = true;
                    currEditPos++;
                }

                if (currEditPos <= 17)
                {
                    whichPositionText.text = $"Em qual posi��o terminou o/a corredor/a {tableManager.Racers[currEditPos].racerName} ({tableManager.Racers[currEditPos].racerCharacter})";
                }

                if (currEditPos == 18)
                {
                    runnerAbsenceToggle.gameObject.SetActive(false);
                    whichPositionText.text = $"Por favor, insira a POSI��O que finalizou o/a corredor/a que deu a melhor volta";
                }
            }
            else
            {
                if (int.TryParse(text, out int racerPos))
                {
                    if (racerPos <= 0 || racerPos > 18)
                    {
                        invalidPosText.text = "Posi��o inv�lida, tente novamente.";
                        StartCoroutine(InvalidPositionValue());
                    }
                    else
                    {
                        if (tableManager.CurrRaceClassification[racerPos - 1] == null)
                        {
                            currRaceInsertionsDict.Add(currEditPos, racerPos);
                            tableManager.CurrRaceClassification[racerPos - 1] = tableManager.Racers[currEditPos]; 
                            invalidPosText.gameObject.SetActive(false);
                            currEditPos++;
                        }
                        else
                        {
                            //POSI��O J� USADA
                            invalidPosText.text = "Posi��o j� inserida, tente novamente.";
                            StartCoroutine(InvalidPositionValue());
                        }

                        if (currEditPos <= 17)
                        {
                            whichPositionText.text = $"Em qual posi��o terminou o/a corredor/a {tableManager.Racers[currEditPos].racerName} ({tableManager.Racers[currEditPos].racerCharacter})";
                        }

                        if (currEditPos == 18)
                        {
                            runnerAbsenceToggle.gameObject.SetActive(false);
                            whichPositionText.text = $"Por favor, insira a POSI��O que finalizou o/a corredor/a que deu a melhor volta";
                        }
                    }
                }
                else
                {
                    invalidPosText.text = "Posi��o inv�lida, tente novamente.";
                    StartCoroutine(InvalidPositionValue());
                }
            }
        }
        else //QUEM DEU A MELHOR VOLTA?
        {
            string text = racerPosInputField.text.Trim((char)8203);

            if (int.TryParse(text, out int bestLapRacerPos))
            {
                if (bestLapRacerPos <= 0 || bestLapRacerPos > 18)
                {
                    invalidPosText.text = "Posi��o inv�lida, tente novamente.";
                    StartCoroutine(InvalidPositionValue());
                }
                else
                {
                    OnReceiveAllRacerPositions?.Invoke(currRaceInsertionsDict);
                    OnReceiveBestLapRacer?.Invoke(bestLapRacerPos);
                    invalidPosText.gameObject.SetActive(false);
                    tableManager.SortRacerByPoints();
                }
            }
            else
            {
                invalidPosText.text = "Posi��o inv�lida, tente novamente.";
                StartCoroutine(InvalidPositionValue());
            }
        }

        racerPosInputField.text = "";
        runnerAbsenceToggle.isOn = false;
    }

    public void FinishResultsInsertion()
    {
        GoToMainMenu();
    }

    public void CancelInsertion()
    {
        GoToMainMenu();
    }

    public void CopyJSONAdressToClipboard()
    {
        GUIUtility.systemCopyBuffer = Application.persistentDataPath;
        StartCoroutine(CopiedToClipboardDisplay());
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void SetMusicVolume(float value)
    {
        float set = 0.0f - (value * 80.0f);
        mixer.SetFloat("musicVolume", set);
        SaveOptionsToJson();
    }

    public void SetSFXVolume(float value)
    {
        float set = 0.0f - (value * 80.0f);
        mixer.SetFloat("sfxVolume", set);
        SaveOptionsToJson();
    }

    public void SetMusicVolumeWithoutSaving(float value)
    {
        float set = 0.0f - (value * 80.0f);
        mixer.SetFloat("musicVolume", set);
    }

    public void SetSFXVolumeWithoutSaving(float value)
    {
        float set = 0.0f - (value * 80.0f);
        mixer.SetFloat("sfxVolume", set);
    }

    public void ManageToggleState()
    {
        if (runnerAbsenceToggle.isOn)
        {
            racerPosInputField.text = "";
            racerPosInputField.interactable = false;
        }
        else
        {
            racerPosInputField.interactable = true;
        }
    }

    void EnableFinishResultsButton()
    {
        cancelInsertionButton.gameObject.SetActive(false);
        finishInsertButton.SetActive(true);
        whichPositionText.text = "Inser��o de resultados finalizada com sucesso!";
        racerPosInputField.gameObject.SetActive(false);
        goToNextButton.gameObject.SetActive(false);
    }

    IEnumerator LogoScreen()
    {
        logoScreenAudio.Play();
        yield return new WaitForSeconds(logoScreenDuration);
        currentMenu = 0;
        logoScreenAudio.Stop();
        StartCoroutine(PlayCharacterVoiceAndGoToFirstMenu());
        isInFirstMenu = true;
    }

    IEnumerator PlayCharacterVoiceAndGoToFirstMenu()
    {
        int voiceClip = random.Next(0, characterVoices.Length);
        characterVoiceAudio.clip = characterVoices[voiceClip];
        characterVoiceAudio.Play();
        gpLogoAnimator.SetTrigger("logo_out");
        whiteFadeAnimator.SetTrigger("white_fade_logo_out");
        yield return new WaitForSeconds(characterVoiceAudio.clip.length);
        mainMenuGPLogoAnimator.SetTrigger("menu_logo_in");
        pressSpaceAnimator.SetTrigger("press_space_in");
        mainMenuAudio.Play();
    }

    IEnumerator PressedSpaceOnMainMenu()
    {
        pressSpaceAudio.Play();
        pressSpaceAnimator.SetTrigger("press_space_shine");
        yield return new WaitForSeconds(afterPressedSpaceDuration);
        pressSpaceAnimator.SetTrigger("press_space_out");
        mainMenuGPLogoAnimator.SetTrigger("menu_logo_out");
        StartCoroutine(DisplayMainMenuButtons());
    }

    IEnumerator DisplayMainMenuButtons()
    {
        yield return new WaitForSeconds(waitToDisplayMainButtonsDuration);
        mainButtonsBackgroundAnimator.SetTrigger("main_buttons_background_in");
        mainButtonsAnimator.SetTrigger("main_buttons_in");
    }

    IEnumerator MenuToTable()
    {
        mainButtonsBackgroundAnimator.SetTrigger("main_buttons_background_out");
        mainButtonsAnimator.SetTrigger("main_buttons_out");
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeOutTime, 0.0f));
        yield return new WaitForSeconds(waitToMainMenuOutDuration);
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeInTime, 1.0f));
        mainMenuAudio.Stop();
        mainMenuAudio.clip = tableMenuClip;
        mainMenuAudio.Play();
        int tableBackground = random.Next(0, tableMenuBackgrounds.Length);
        mainBackgroundImage.sprite = tableMenuBackgrounds[tableBackground];
        trophyAnimator.SetTrigger("trophy_in");
        resultsMenuAnimator.SetTrigger("results_menu_in");
        yield return new WaitForSeconds(waitToDisplayReturnButtonDuration);
        returnButtonAnimator.SetTrigger("return_button_in");
    }

    IEnumerator MenuToInsertResults()
    {
        mainButtonsBackgroundAnimator.SetTrigger("main_buttons_background_out");
        mainButtonsAnimator.SetTrigger("main_buttons_out");
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeOutTime, 0.0f));
        yield return new WaitForSeconds(waitToMainMenuOutDuration);
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeInTime, 1.0f));
        mainMenuAudio.Stop();
        mainMenuAudio.clip = insertMenuClip;
        mainMenuAudio.Play();
        mainBackgroundImage.sprite = insertResultsMenuBackground;
        areYouSureMenu.SetActive(true);
        insertEditor.SetActive(false);
        finishInsertButton.SetActive(false);
        insertResultsMenuAnimator.SetTrigger("insert_results_in");
    }

    IEnumerator MenuToOptionsResults()
    {
        mainButtonsBackgroundAnimator.SetTrigger("main_buttons_background_out");
        mainButtonsAnimator.SetTrigger("main_buttons_out");
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeOutTime, 0.0f));
        yield return new WaitForSeconds(waitToMainMenuOutDuration);
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeInTime, 1.0f));
        mainMenuAudio.Stop();
        mainMenuAudio.clip = optionsMenuClip;
        mainMenuAudio.Play();
        mainBackgroundImage.sprite = optionsMenuBackground;
        optionsMenuAnimator.SetTrigger("options_menu_in");
        yield return new WaitForSeconds(waitToDisplayReturnButtonDuration);
        returnButtonAnimator.SetTrigger("return_button_in");
    }

    IEnumerator ToMainMenu()
    {
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeOutTime, 0.0f));
        yield return new WaitForSeconds(waitToDisplayMainButtonsDuration);
        int menuMusicClip = random.Next(0, mainMenuMusics.Length);
        mainMenuAudio.clip = mainMenuMusics[menuMusicClip];
        mainMenuAudio.Play();
        StartCoroutine(FadeAudioSource.StartFade(mainMenuAudio, goToOtherMenuAudioFadeInTime, 1.0f));
        mainBackgroundImage.sprite = mainMenuBackground;
        mainButtonsBackgroundAnimator.SetTrigger("main_buttons_background_in");
        mainButtonsAnimator.SetTrigger("main_buttons_in");
    }

    IEnumerator InvalidPositionValue()
    {
        invalidPosText.gameObject.SetActive(true);
        yield return new WaitForSeconds(invalidPosTextDisplayDuration);
        invalidPosText.gameObject.SetActive(false);
    }

    IEnumerator CopiedToClipboardDisplay()
    {
        copiedToClipboardText.gameObject.SetActive(true);
        yield return new WaitForSeconds(invalidPosTextDisplayDuration);
        copiedToClipboardText.gameObject.SetActive(false);
    }

    void SaveOptionsToJson()
    {
        OptionsData data = new OptionsData();
        data.musicSliderValue = musicSlider.value;
        data.sfxSliderValue = SFXSlider.value;

        string persistentDataPath = Application.persistentDataPath + "/SaveDataOptions.json";
        using StreamWriter writer = new StreamWriter(persistentDataPath);
        string json = JsonUtility.ToJson(data);
        writer.Write(json);
        writer.Close();
    }

    void LoadOptionsFromJson()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveDataOptions.json"))
        {
            string persistentDataPath = Application.persistentDataPath + "/SaveDataOptions.json";
            using StreamReader reader = new StreamReader(persistentDataPath);

            string json = reader.ReadToEnd();
            OptionsData loadedData = JsonUtility.FromJson<OptionsData>(json);

            musicSlider.value = loadedData.musicSliderValue;
            SFXSlider.value = loadedData.sfxSliderValue;

            SetMusicVolumeWithoutSaving(musicSlider.value);
            SetSFXVolumeWithoutSaving(SFXSlider.value);
        }
    }
}
