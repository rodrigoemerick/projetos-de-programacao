﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class CarController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private float m_SpeedRotation;
    [SerializeField]
    private float m_Acceleration;
    [SerializeField]
    private float m_StopTime;
    [SerializeField]
    private int m_CurrentNumberOfLaps;
    public int CurrentNumberOfLaps
    {
        get { return m_CurrentNumberOfLaps; }
        set { m_CurrentNumberOfLaps = value; }
    }
    [SerializeField]
    private bool m_CanStartNewLap;
    [SerializeField]
    private bool m_PassedThroughMiddleLap;

    [SerializeField]
    private PhotonView m_PV;

    [SerializeField]
    private Material[] m_PlayerMaterials;
    public Material[] PlayerMaterials
    {
        get { return m_PlayerMaterials; }
        set { m_PlayerMaterials = value; }
    }

    private NetworkController m_NC;

    private Vector3 m_InputValues;

    void Start()
    {
        m_InputValues = Vector3.zero;
        m_CanStartNewLap = true;
        m_NC = GameObject.Find("NetworkManager").GetComponent<NetworkController>();
    }

    void Update()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.ActorNumber);

        if (m_NC.CanStartMatch)
        {
            m_Acceleration = 0.05f;
        }
        else
        {
            m_Acceleration = 0.0f;
        }

        if (m_NC.SomeoneWon)
        {
            m_Acceleration = 0.0f;
        }

        if (PhotonNetwork.IsMasterClient)
        {
            //SetColor(0);
            m_PV.RPC("SetColor", RpcTarget.AllBuffered, 0);
        }
        else
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
            {
                //SetColor(1);
                m_PV.RPC("SetColor", RpcTarget.AllBuffered, 1);
            }
            else if (PhotonNetwork.LocalPlayer.ActorNumber == 3)
            {
                //SetColor(2);
                m_PV.RPC("SetColor", RpcTarget.AllBuffered, 2);
            }
            else if (PhotonNetwork.LocalPlayer.ActorNumber == 4)
            {
                //SetColor(3);
                m_PV.RPC("SetColor", RpcTarget.AllBuffered, 3);
            }

            //switch (PhotonNetwork.CurrentRoom.PlayerCount)
            //{
            //    case 2:
            //        gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[1];
            //        break;
            //    case 3:
            //        gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[2];
            //        break;
            //    case 4:
            //        gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[3];
            //        break;
            //}
        }

        if (m_PV.IsMine)
        {
            if (Input.GetAxis("Vertical") > 0.0f)
            {
                m_Speed += m_Acceleration;

                if (m_Speed > 10.0f)
                {
                    m_Speed = 10.0f;
                }
            }
            else if (Input.GetAxis("Vertical") == 0.0f)
            {
                m_Speed = 0.0f;
            }
            else
            {
                m_Speed -= m_Acceleration;

                if (m_Speed < 0.0f)
                {
                    m_Speed = 0.0f;
                }
            }

            m_InputValues.x = Input.GetAxis("Horizontal") * m_SpeedRotation * Time.deltaTime;
            m_InputValues.z = Input.GetAxis("Vertical") * m_Speed * Time.deltaTime;

            transform.Translate(0.0f, 0.0f, m_InputValues.z);
            transform.Rotate(0.0f, m_InputValues.x, 0.0f);
        }
    }

    [PunRPC]
    public void SetColor(int whichColor)
    {
        switch (whichColor)
        {
            case 0:
                gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[0];
                break;
            case 1:
                gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[1];
                break;
            case 2:
                gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[2];
                break;
            case 3:
                gameObject.GetComponent<Renderer>().material = m_PlayerMaterials[3];
                break;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(gameObject.GetComponent<Renderer>().material);
        }
        else
        {
            gameObject.GetComponent<Renderer>().material = (Material)stream.ReceiveNext();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            m_Acceleration = 0.0f;
            StartCoroutine(StopOtherCar());
        }

        if (other.gameObject.CompareTag("RaceStart") && m_CanStartNewLap)
        {
            ++m_CurrentNumberOfLaps;
            m_CanStartNewLap = false;
        }

        if (other.gameObject.CompareTag("LapController"))
        {
            m_CanStartNewLap = true;
        }
    }

    private IEnumerator StopOtherCar()
    {
        yield return new WaitForSeconds(m_StopTime);
        m_Acceleration = 0.05f;
    }
}
