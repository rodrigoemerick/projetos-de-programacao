﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    private Button m_ConnectButton;
    [SerializeField]
    private Button m_RoomButton;
    [SerializeField]
    private Button m_DisconnectButton;
    [SerializeField]
    private Button m_IsReadyButton;
    [SerializeField]
    private GameObject m_NetworkManager;
    [SerializeField]
    private float m_TimeToConnect;

    void Awake()
    {
        SetButtons(true, false, false, false);
        DontDestroyOnLoad(m_NetworkManager);
    }
    
    public void OnConnectButtonClicked()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            StartCoroutine(Connecting());
        }
    }

    public void OnRoomButtonClicked()
    {
        if (PhotonNetwork.IsConnected)
        {
            
            SetButtons(false, false, false, false);
        }
    }

    public void OnDisconnectButtonClicked()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
            SetButtons(false, false, false, false);
        }
    }

    public void OnReadyButtonClicked()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinLobby();
            SetButtons(false, true, true, false);
        }
    }

    private void SetButtons(bool connect, bool room, bool disconnect, bool isReady)
    {
        m_ConnectButton.interactable = connect;
        m_RoomButton.interactable = room;
        m_DisconnectButton.interactable = disconnect;
        m_IsReadyButton.interactable = isReady;
    }

    private IEnumerator Connecting()
    {
        yield return new WaitForSeconds(m_TimeToConnect);
        SetButtons(false, false, true, true);
    }
}
