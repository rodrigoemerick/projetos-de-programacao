﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class NetworkController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Text m_Status;
    [SerializeField]
    private Button m_ConnectButton;
    [SerializeField]
    private Button m_RoomButton;
    [SerializeField]
    private Button m_DisconnectButton;
    [SerializeField]
    private Button m_IsReadyButton;
    [SerializeField]
    private Canvas m_MenuCanvas;
    [SerializeField]
    private GameObject m_Player;
    [SerializeField]
    private Image m_Logo;
    [SerializeField]
    private Image m_Background;
    [SerializeField]
    private Text m_CurrentScore;

    List<GameObject> m_PlayersList;

    private bool m_RoomExists;
    private bool m_IsReady;
    private bool m_CanStartMatch;
    public bool CanStartMatch
    {
        get { return m_CanStartMatch; }
        set { m_CanStartMatch = value; }
    }
    private bool m_SomeoneWon;
    public bool SomeoneWon
    {
        get { return m_SomeoneWon; }
        set { m_SomeoneWon = value; }
    }

    void Start()
    {
        m_PlayersList = new List<GameObject>();
        //PhotonNetwork.CurrentRoom.MaxPlayers = 4;
    }

    void Update()
    {
        if (m_RoomExists)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount >= 2 && PhotonNetwork.CurrentRoom.PlayerCount <= 4 && !m_IsReady && m_CanStartMatch)
            {
                m_IsReady = true;
                m_ConnectButton.gameObject.SetActive(false);
                m_RoomButton.gameObject.SetActive(false);
                m_DisconnectButton.gameObject.SetActive(false);
                m_Status.text = "";
                m_Status.gameObject.SetActive(false);
                m_Logo.gameObject.SetActive(false);
                m_Background.gameObject.SetActive(false);
                m_CurrentScore.gameObject.SetActive(true);
                StartCoroutine(CheckIfSomeoneWon());
                UpdateText(PhotonNetwork.LocalPlayer.ActorNumber - 1);
            }
        }
    }

    public override void OnConnected()
    {

    }

    public override void OnConnectedToMaster()
    {
        m_Status.text = "Status: Connected, join a room!";
    }

    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom("Room" + Random.Range(10, 1000));
    }

    public override void OnJoinedRoom()
    {
        m_RoomExists = true;
        Vector3 playerPosition = Vector3.zero;
        Quaternion playerRotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
        SceneManager.LoadScene("Game", LoadSceneMode.Additive);

        switch (PhotonNetwork.LocalPlayer.ActorNumber)
        {
            case 1:
                m_Status.text = "Status: Waiting for more oponents...";
                playerPosition = new Vector3(-14.42f, 0.55f, 17.94f);
                //m_Player.gameObject.GetComponent<Renderer>().material = m_Player.GetComponent<CarController>().PlayerMaterials[0];
                break;
            case 2:
                m_Status.text = "Status: You can wait for 2 more oponents or start the match...";
                playerPosition = new Vector3(-14.42f, 0.55f, 19.68f);
                //m_Player.gameObject.GetComponent<Renderer>().material = m_Player.GetComponent<CarController>().PlayerMaterials[1];
                break;
            case 3:
                m_Status.text = "Status: You can wait for 1 more oponent or start the match...";
                playerPosition = new Vector3(-14.42f, 0.55f, 21.36f);
                //m_Player.gameObject.GetComponent<Renderer>().material = m_Player.GetComponent<CarController>().PlayerMaterials[2];
                break;
            case 4:
                m_Status.text = "Status: Ready!";
                playerPosition = new Vector3(-14.42f, 0.55f, 23.11f);
                //m_Player.gameObject.GetComponent<Renderer>().material = m_Player.GetComponent<CarController>().PlayerMaterials[3];
                break;
        }
        m_PlayersList.Add(PhotonNetwork.Instantiate(m_Player.name, playerPosition, playerRotation, 0));
    }

    public override void OnDisconnected(DisconnectCause cause)
    {

    }

    public void MatchCanStart()
    {
        m_CanStartMatch = true;
        m_IsReadyButton.gameObject.SetActive(false);
    }

    private IEnumerator CheckIfSomeoneWon()
    {
        CarController temp;

        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < m_PlayersList.Count; ++i)
        {
            temp = m_PlayersList[i].GetComponent<CarController>();

            if (temp.CurrentNumberOfLaps == 6)
            {
                m_SomeoneWon = true;
                Debug.Log(temp.name + " venceu!!!");
            }
        }

        if (!m_SomeoneWon)
        {
            StartCoroutine(CheckIfSomeoneWon());
        }
    }

    private void UpdateText(int whichPlayer)
    {
        CarController temp;

        switch (PhotonNetwork.LocalPlayer.ActorNumber)
        {
            case 1:
                m_CurrentScore.gameObject.transform.position = new Vector3(0, 0, 0);
                break;
            case 2:
                m_CurrentScore.gameObject.transform.position = new Vector3(5, 0, 0);
                break;
            case 3:
                m_CurrentScore.gameObject.transform.position = new Vector3(10, 0, 0);
                break;
            case 4:
                m_CurrentScore.gameObject.transform.position = new Vector3(15, 0, 0);
                break;
        }

        if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
        {
            temp = m_PlayersList[whichPlayer].GetComponent<CarController>();
        }
        else
        {
            temp = m_PlayersList[whichPlayer - 1].GetComponent<CarController>();
        }

        m_CurrentScore.text = temp.CurrentNumberOfLaps + "/5";
    }
}