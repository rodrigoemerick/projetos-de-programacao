﻿using System;
using System.IO;
using System.Globalization;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace _31850786_Ex7_IrisFlowerML
{
    class Program
    {
        static readonly string _trainPath = Path.Combine(Environment.CurrentDirectory, "Data", "iris.train.txt");
        static readonly string _testPath = Path.Combine(Environment.CurrentDirectory, "Data", "iris.test.txt");
        static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "Data", "IrisClusteringModel.zip");

        static void Main(string[] args)
        {
            NumberFormatInfo number = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            number.NumberDecimalSeparator = ".";

            var mlContextTrain = new MLContext(seed: 0);

            IDataView dataView = mlContextTrain.Data.LoadFromTextFile<IrisData>(_trainPath, hasHeader: false, separatorChar: ',');

            string featuresColumnName = "Features";
            var pipeline = mlContextTrain.Transforms
                .Concatenate(featuresColumnName, "SepalLength", "SepalWidth", "PetalLength", "PetalWidth")
                .Append(mlContextTrain.Clustering.Trainers.KMeans(featuresColumnName, numberOfClusters: 3));

            var model = pipeline.Fit(dataView);

            using (var fileStream = new FileStream(_modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                mlContextTrain.Model.Save(model, dataView.Schema, fileStream);
            }

            var predictor = mlContextTrain.Model.CreatePredictionEngine<IrisData, ClusterPrediction>(model);

            string[] testLines = File.ReadAllLines(_testPath);

            for (int i = 0; i < testLines.Length; ++i)
            {
                string[] numbers = testLines[i].Split(",");
                IrisData iris = new IrisData(float.Parse(numbers[0], number), float.Parse(numbers[1], number), float.Parse(numbers[2], number), float.Parse(numbers[3], number));
                var prediction = predictor.Predict(iris);
                Console.WriteLine($"Linha do arquivo de validação: {i}, com ID obtido: {prediction.PredictedClusterId} e flor: {prediction.PredictedClusterId.IdToIrisName()}");
            }
        }
    }

    static class Extensions
    {
        public static string IdToIrisName(this uint id)
        {
            string flower = "";

            switch (id)
            {
                case 1:
                    flower = "Iris-setosa";
                    break;
                case 2:
                    flower = "Iris-versicolor";
                    break;
                case 3:
                    flower = "Iris-virginica";
                    break;
            }

            return flower;
        }
    }
}