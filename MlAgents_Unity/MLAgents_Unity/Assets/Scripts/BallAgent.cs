﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class BallAgent : Agent
{
    Rigidbody m_Rigidbody;

    [SerializeField] private Transform m_TargetCube;
    [SerializeField] private Transform m_Bridge;
    [SerializeField] private float m_Speed;
    [SerializeField] private float m_TimeOut;
    [SerializeField] private float m_DistToTarget;
    [SerializeField] private float m_RestartHeight;
    [SerializeField] private float[] m_BridgeLimits;
    [SerializeField] private float[] m_TargetLimitsX;
    [SerializeField] private float[] m_TargetLimitsZ;
    [SerializeField] private float m_MaxReward;
    [SerializeField] private float m_MinReward;

    bool m_PassedBridge;
    bool m_ReturnedToFloorA;
    bool m_ReceivedBridgeReward;

    const float m_TargetCubeY = 0.5f;
    const float m_BridgeY = 0.0f;
    const float m_BridgeZ = 6.5f;

    Vector3 m_InitialPos;

    Coroutine m_Coroutine;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_InitialPos = transform.localPosition;
    }

    public override void OnEpisodeBegin()
    {
        m_Coroutine = StartCoroutine(RestartIfTimeOut());

        if (transform.localPosition.y < m_RestartHeight)
        {
            m_Rigidbody.angularVelocity = Vector3.zero;
            m_Rigidbody.velocity = Vector3.zero;
        }

        transform.localPosition = m_InitialPos;
        m_TargetCube.localPosition = new Vector3(Random.Range(m_TargetLimitsX[0], m_TargetLimitsX[1]), m_TargetCubeY, Random.Range(m_TargetLimitsZ[0], m_TargetLimitsZ[1]));
        m_Bridge.localPosition = new Vector3(Random.Range(m_BridgeLimits[0], m_BridgeLimits[1]), m_BridgeY, m_BridgeZ);
        m_PassedBridge = false;
        m_ReturnedToFloorA = false;
        m_ReceivedBridgeReward = false;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(m_TargetCube.localPosition);
        sensor.AddObservation(m_Bridge.localPosition);
        sensor.AddObservation(transform.localPosition);

        sensor.AddObservation(m_Rigidbody.velocity.x);
        sensor.AddObservation(m_Rigidbody.velocity.z);
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = vectorAction[0];
        controlSignal.z = vectorAction[1];
        m_Rigidbody.AddForce(controlSignal * m_Speed);

        float distanceToTarget = Vector3.Distance(transform.localPosition, m_TargetCube.transform.localPosition);

        if (distanceToTarget < m_DistToTarget)
        {
            SetReward(m_MaxReward);
            EndProcess();
            Debug.Log("Ganhei 1 de recompensa");
        }

        if (m_PassedBridge && !m_ReceivedBridgeReward)
        {
            SetReward(m_MinReward);
            m_ReceivedBridgeReward = true;
            Debug.Log("Ganhei 0.1 de recompensa");
        }

        if (transform.localPosition.y < m_RestartHeight || m_ReturnedToFloorA)
        {
            Debug.Log("Recomecei");
            EndProcess();
        }
    }
    
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = -Input.GetAxis("Vertical");
        actionsOut[1] = Input.GetAxis("Horizontal");
    }

    void OnTriggerEnter(Collider other)
    {
        if (!m_PassedBridge)
        {
            if (other.gameObject.CompareTag("Bridge"))
            {
                m_PassedBridge = true;
            }
        }
        else
        {
            m_ReturnedToFloorA = true;
        }
    }

    void EndProcess()
    {
        StopCoroutine(m_Coroutine);
        EndEpisode();
    }

    IEnumerator RestartIfTimeOut()
    {
        Debug.Log("Corrotina");
        yield return new WaitForSeconds(m_TimeOut);
        EndProcess();
    }
}