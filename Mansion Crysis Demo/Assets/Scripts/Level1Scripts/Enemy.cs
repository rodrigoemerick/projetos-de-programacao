﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    //navMesh setting
    private GameObject m_TargetPlayer;
    [SerializeField]
    private float m_EnemyRange;
    [SerializeField]
    private float m_Hp;
    public float Hp
    {
        get { return m_Hp; }
        set { m_Hp = value; }
    }
    [SerializeField]
    private float m_PushBackValue;

    [SerializeField]
    private bool m_EnemyDefeated;
    public bool EnemyDefeated
    {
        get { return m_EnemyDefeated; }
        set { m_EnemyDefeated = value; }
    }

    NavMeshAgent m_Enemy;

    Bullet m_Bullet;

    Scene m_CurrentScene;
    [SerializeField]
    private string m_SceneName;

    Aula.Player.PlayerController m_PlayerLevel1;
    Aula.Player.PlayerController2 m_PlayerLevel2;

    void Start()
    {
        m_CurrentScene = SceneManager.GetActiveScene();
        m_SceneName = m_CurrentScene.name;

        m_TargetPlayer = GameObject.Find("PlayerController");
        if (m_SceneName == "Nivel1")
        {
            m_PlayerLevel1 = FindObjectOfType<Aula.Player.PlayerController>();
        }
        else if (m_SceneName == "Nivel2")
        {
            m_PlayerLevel2 = FindObjectOfType<Aula.Player.PlayerController2>();
        }
        m_Enemy = GetComponent<NavMeshAgent>();
    }
    
    void Update()
    {
        if (Vector3.Distance(m_Enemy.transform.position, m_TargetPlayer.transform.position) < m_EnemyRange)
        {
            m_Enemy.destination = m_TargetPlayer.transform.position;
        }

        OnEnemyDefeated();
    }

    void OnEnemyDefeated()
    {
        if (m_SceneName == "Nivel1")
        {
            if (m_Hp <= 0.0f)
            {
                Destroy(gameObject);
                m_PlayerLevel1.ResetSpeed();
            }
        }
        else if (m_SceneName == "Nivel2")
        {
            if (m_Hp <= 0.0f)
            {
                Destroy(gameObject);
                m_PlayerLevel2.ResetSpeed();
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Bullet")
        {
            m_Bullet = other.gameObject.GetComponent<Bullet>();
            m_Hp -= m_Bullet.BulletDamage;
            Vector3 m_PushPosition = transform.position - other.transform.position;
            m_PushPosition.Normalize();
            gameObject.GetComponent<Rigidbody>().AddForce(m_PushBackValue * m_PushPosition);
        }
    }

    public void SetNewParent(Transform parent)
    {
        transform.SetParent(parent);
    }
}
