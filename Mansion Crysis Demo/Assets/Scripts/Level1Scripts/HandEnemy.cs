﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HandEnemy : Enemy
{
    [SerializeField]
    private float m_Damage;
    public float Damage
    {
        get { return m_Damage; }
        set { m_Damage = value; }
    }
}