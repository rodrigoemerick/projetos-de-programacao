﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float m_Speed;

    public float m_BulletDamage;
    public float BulletDamage
    {
        get { return m_BulletDamage; }
        set { m_BulletDamage = value; }
    }

    public GameObject m_Origin;

    void Update()
    {
        float step = m_Speed * Time.deltaTime;
        transform.localPosition += transform.forward * step;
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "HandEnemy" || other.gameObject.tag == "EyeEnemy")
        {
            gameObject.SetActive(false);
        }    
    } 
}
