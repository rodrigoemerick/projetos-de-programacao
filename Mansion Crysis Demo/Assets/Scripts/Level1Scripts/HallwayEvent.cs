﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayEvent : MonoBehaviour
{
    [SerializeField]
    private AudioSource m_HandEnemyFX;
    [SerializeField]
    private AudioSource m_EyeEnemyFX;
    [SerializeField]
    private bool m_EventHappened;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && !m_EventHappened)
        {
            m_HandEnemyFX.Play();
            m_EyeEnemyFX.Play();
            m_EventHappened = true;
        }    
    }
}
