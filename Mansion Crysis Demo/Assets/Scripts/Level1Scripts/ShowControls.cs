﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowControls : MonoBehaviour
{
    [SerializeField]
    private Image m_ControlsScreen;
    [SerializeField]
    private Text m_TextControls;
    [SerializeField]
    private bool m_IsShowing;
    [SerializeField]
    private Text m_PressEscape;
    [SerializeField]
    private EspAmmoStatus m_CanShowInScreen;

    void Awake()
    {
        m_CanShowInScreen = GameObject.Find("EspecialAmmoFill").GetComponent<EspAmmoStatus>();
        m_ControlsScreen.enabled = false;
        m_TextControls.text = "W ou Seta Cima: Mover para frente" + "\n" + "S ou Seta Baixo: Mover para trás" + "\n" + "Arrastar Mouse: Girar Câmera" + "\n" + "Botão Esquerdo do Mouse: Disparo" + "\n" + "Botâo Direito do Mouse: ???";
        m_TextControls.enabled = false;
        m_PressEscape.enabled = false;
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T) && !m_IsShowing)
        {
            m_IsShowing = true;
            Show();
        }
        
        if (Input.GetKeyDown(KeyCode.Escape) && m_IsShowing)
        {
            m_IsShowing = false;
            Hide();
        }

        if (m_CanShowInScreen.CanShowInTutorialScreen)
        {
            m_TextControls.text = "W ou Seta Cima: Mover para frente" + "\n" + "S ou Seta Baixo: Mover para trás" + "\n" + "Arrastar Mouse: Girar Câmera" + "\n" + "Botão Esquerdo do Mouse: Disparo" + "\n" + "Botâo Direito do Mouse: Disparo Especial";
        }
    }

    void Show()
    {
        m_ControlsScreen.enabled = true;
        m_TextControls.enabled = true;
        m_PressEscape.enabled = true;
    }

    void Hide()
    {
        m_ControlsScreen.enabled = false;
        m_TextControls.enabled = false;
        m_PressEscape.enabled = false;
    }
}
