﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EspAmmoStatus : MonoBehaviour
{
    private Image m_Status;

    [SerializeField]
    private Image m_CanFireEspecialUI;

    [SerializeField]
    private float m_MaxValue;

    private float m_StatusFill;
    public bool m_CanShowInTutorialScreen;
    public bool CanShowInTutorialScreen
    {
        get { return m_CanShowInTutorialScreen; }
        set { m_CanShowInTutorialScreen = value; }
    }
    public float m_MaxAmmoValue { get; set; }
    private float m_CurrentAmmoStatus;
    public float CurrentAmmoStatus
    {
        get { return m_CurrentAmmoStatus; }
        set { m_CurrentAmmoStatus = value;
            if (value < 0)
            {
                m_CurrentAmmoStatus = 0;
                m_CanFireEspecialUI.enabled = false;
            }
            else if (value >= m_MaxAmmoValue)
            {
                m_CurrentAmmoStatus = m_MaxAmmoValue;
                m_CanFireEspecialUI.enabled = true;
                m_CanShowInTutorialScreen = true;
            }
            else
            {
                m_CurrentAmmoStatus = value;
                m_CanFireEspecialUI.enabled = false;
            }
            m_StatusFill = m_CurrentAmmoStatus / m_MaxAmmoValue;
        }
    }

    void Start()
    {
        m_Status = GetComponent<Image>();
        m_CanFireEspecialUI.enabled = false;
        m_MaxAmmoValue = m_MaxValue;
    }

    void Update()
    {
        m_Status.fillAmount = m_StatusFill;
    }

    public void Init(float currentStatus, float maxValue)
    {
        m_CurrentAmmoStatus = currentStatus;
        m_MaxAmmoValue = maxValue;
    }
}