﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTutorial : MonoBehaviour
{
    [SerializeField]
    private Image m_TutorialBackground;
    [SerializeField]
    private Text m_TutorialMessage;
    [SerializeField]
    private Text m_PressEscape;
    [SerializeField]
    private bool m_IsShowing;

    void Awake()
    {
        m_TutorialBackground.enabled = false;
        m_TutorialMessage.enabled = false;
        m_PressEscape.enabled = false;
    }

    void Update()
    {
        if (m_IsShowing && Input.GetKeyDown(KeyCode.Escape))
        {
            m_IsShowing = false;
            Hide();
        }
    }

    public void Show()
    {
        Time.timeScale = 0;
        m_TutorialBackground.enabled = true;
        m_TutorialMessage.enabled = true;
        m_PressEscape.enabled = true;
    }

    public void Hide()
    {
        Time.timeScale = 1;
        m_TutorialBackground.enabled = false;
        m_TutorialMessage.enabled = false;
        m_PressEscape.enabled = false;
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Show();
            m_IsShowing = true;
        }    
    }
}
