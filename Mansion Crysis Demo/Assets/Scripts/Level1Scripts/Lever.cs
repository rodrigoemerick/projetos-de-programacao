﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class Lever : MonoBehaviour
//{
//    public Animator m_LeverAnimator;

//    public bool m_PlayerIsColliding;
//    public bool m_LeverPulled;

//    [SerializeField]
//    private Text m_Interaction;

//    [SerializeField]
//    private AudioSource m_PullFX;

//    void Start()
//    {
//        m_Interaction.enabled = false;
//    }

//    void Update()
//    {
//        if(m_PlayerIsColliding && !m_LeverPulled && Input.GetKeyDown(KeyCode.E))
//        {
//            m_LeverAnimator.SetTrigger("Pull");
//            m_PullFX.Play();
//            m_LeverPulled = true;
//            m_Interaction.enabled = false;
//        }        
//    }

//    void OnTriggerEnter(Collider other)
//    {
//        if (other.gameObject.tag == "Player")
//        {
//            m_PlayerIsColliding = true;
//            if (!m_LeverPulled)
//            {
//                m_Interaction.enabled = true;
//            }
//        }
//    }

//    void OnTriggerExit(Collider other)
//    {
//        if (other.gameObject.tag == "Player")
//        {
//            m_PlayerIsColliding = false;
//            m_Interaction.enabled = false;
//        }
//    }
//}
