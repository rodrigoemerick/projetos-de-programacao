﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspecialAmmo : MonoBehaviour
{
    [SerializeField]
    private float m_TimeToRespawn;

    public GameObject m_EspecialAmmo;
    [SerializeField]
    private EspAmmoStatus m_EspAmmoStatus;
    [SerializeField]
    private float m_InitialEspAmmo;
    [SerializeField]
    private float m_MaxEspAmmo;

    void Start()
    {
        m_EspAmmoStatus.Init(m_InitialEspAmmo, m_MaxEspAmmo);
        m_EspAmmoStatus.CurrentAmmoStatus = m_InitialEspAmmo;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && gameObject.activeSelf)
        {
            if (m_EspAmmoStatus.CurrentAmmoStatus < m_MaxEspAmmo)
            {
                m_EspecialAmmo.SetActive(false);
                StartCoroutine(RespawnAmmo());
            }
        }
    }

    private IEnumerator RespawnAmmo()
    {
        yield return new WaitForSeconds(m_TimeToRespawn);
        m_EspecialAmmo.SetActive(true);
    }
}
