﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Locker : MonoBehaviour
{
    [SerializeField]
    private Animator m_LockerDoorAnimator;

    [SerializeField]
    private Text m_Interaction;

    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_IsOpen;
    public bool IsOpen
    {
        get { return m_IsOpen; }
        set { m_IsOpen = value; }
    }

    [SerializeField]
    private AudioSource m_LockerOpenFX;

    LevelManager m_LevelManagerAccess;

    void Start()
    {
        m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        m_Interaction.enabled = false;
    }

    void Update()
    {
        if(m_PlayerIsColliding && !m_IsOpen && m_LevelManagerAccess.LeverStatus && Input.GetKeyDown(KeyCode.E))
        {
            m_LockerDoorAnimator.SetTrigger("Open");
            m_LockerOpenFX.Play();
            m_IsOpen = true;
            m_Interaction.enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if (!m_IsOpen && m_LevelManagerAccess.LeverStatus)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }
    }
}
