﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerValues : MonoBehaviour
{
    Aula.Player.PlayerController m_PlayerValuesAccess;
    [SerializeField]
    private FearBarStatus m_PlayerFear;

    Scene m_CurrentScene;
    [SerializeField]
    private string m_SceneName;

    [SerializeField]
    private float m_OnLoadHorizontalSpeed;
    public float OnLoadHorizontalSpeed
    {
        get { return m_OnLoadHorizontalSpeed; }
        set { m_OnLoadHorizontalSpeed = value; }
    }
    [SerializeField]
    private float m_OnLoadVerticalSpeed;
    public float OnLoadVerticalSpeed
    {
        get { return m_OnLoadVerticalSpeed; }
        set { m_OnLoadVerticalSpeed = value; }
    }

    [SerializeField]
    private float m_OnLoadFearValue;
    public float OnLoadFearValue
    {
        get { return m_OnLoadFearValue; }
        set { m_OnLoadFearValue = value; }
    }

    void Start()
    {
        m_CurrentScene = SceneManager.GetActiveScene();
        m_SceneName = m_CurrentScene.name;

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        
        if (m_SceneName == "Nivel1")
        {
            m_PlayerValuesAccess = GameObject.Find("PlayerController").GetComponent<Aula.Player.PlayerController>();
        }
    }
    
    void Update()
    {
        if (m_SceneName == "Nivel1")
        {
            m_OnLoadHorizontalSpeed = m_PlayerValuesAccess.TempHorizontalSpeed;
            m_OnLoadVerticalSpeed = m_PlayerValuesAccess.TempVerticalSpeed;
            m_OnLoadFearValue = m_PlayerFear.CurrentFearStatus;
        }
    }

    void OnSceneLoaded(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode mode)
    {
        if(scene.name == "FinalAlarmClock")
        {
            Destroy(gameObject);
        }
    }
}
