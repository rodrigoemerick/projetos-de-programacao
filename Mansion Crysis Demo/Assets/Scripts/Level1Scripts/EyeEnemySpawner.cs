﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeEnemySpawner : EnemySpawner
{
    protected override void Update()
    {
        if (HasSpawned && GameObject.FindGameObjectsWithTag("EyeEnemy").Length == 0)
        {
            HordeIsDefeated = true;
        }
    }
}
