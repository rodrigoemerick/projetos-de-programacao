﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FearBarStatus : MonoBehaviour
{
    private Image m_Status;

    [SerializeField]
    private float m_MaxValue;

    private float m_StatusFill;
    public float m_MaxFearValue { get; set; }
    private float m_CurrentFearStatus;
    public float CurrentFearStatus
    {
        get { return m_CurrentFearStatus; }
        set { m_CurrentFearStatus = value;
            if(value < 0)
            {
                m_CurrentFearStatus = 0;
            }
            else if(value > m_MaxFearValue)
            {
                m_CurrentFearStatus = m_MaxFearValue;
            }
            else
            {
                m_CurrentFearStatus = value;
            }
            m_StatusFill = m_CurrentFearStatus / m_MaxFearValue;
        }
    }

    void Start()
    {
        m_Status = GetComponent<Image>();
        m_MaxFearValue = m_MaxValue;
    }

    void Update()
    {
        m_Status.fillAmount = m_StatusFill;
    }

    public void Init(float currentStatus, float maxValue)
    {
        m_CurrentFearStatus = currentStatus;
        m_MaxFearValue = maxValue;
    }
}
