﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspecialBullet : MonoBehaviour
{   
    public float m_Speed;
    public float m_TimeToExplode;
    public float m_RadiusRange;
    public float m_ExplosionForce;
    public float m_Damage;
    private bool m_HasExploded;
    public bool HasExploded
    {
        get { return m_HasExploded; }
        set { m_HasExploded = value; }
    }

    public GameObject m_Origin;

    [SerializeField]
    private ExplosionFog m_ExplosionFog;

    [SerializeField]
    private AudioSource m_EspecialBulletFX;
    [SerializeField]
    private AudioSource m_Explosion;

    void Start()
    {
        m_ExplosionFog = GameObject.Find("ExplosionFogManager").GetComponent<ExplosionFog>();
    }

    void Update()
    {
        StartCoroutine(ExplodeAfterTime());
        
        float step = m_Speed * Time.deltaTime;
        transform.localPosition += transform.forward * step;

        if (!m_EspecialBulletFX.isPlaying)
        {
            m_EspecialBulletFX.Play();
        }
    }

    public IEnumerator ExplodeAfterTime()
    {
        yield return new WaitForSeconds(m_TimeToExplode);
        Explode();
        gameObject.SetActive(false);
    }

    void Explode()
    {
        if (!m_Explosion.isPlaying)
        {
            m_Explosion.Play();
        }
        m_EspecialBulletFX.Pause();

        m_ExplosionFog.CallExplosion();

        Collider[] detectedColliders = Physics.OverlapSphere(transform.position, m_RadiusRange);

        foreach(Collider enemiesInRange in detectedColliders)
        {
            Rigidbody m_Rigidbody = enemiesInRange.GetComponent<Rigidbody>();
            if(m_Rigidbody != null)
            {
                m_Rigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_RadiusRange);
            }

            HandEnemy m_HandEnemy = enemiesInRange.GetComponent<HandEnemy>();
            if(m_HandEnemy != null)
            {
                m_HandEnemy.Hp -= m_Damage;
            }

            EyeEnemy m_EyeEnemy = enemiesInRange.GetComponent<EyeEnemy>();
            if(m_EyeEnemy != null)
            {
                m_EyeEnemy.Hp -= m_Damage;
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "HandEnemy" || other.gameObject.tag == "EyeEnemy")
        {
            Explode();
            gameObject.SetActive(false);
        }
    }
}
