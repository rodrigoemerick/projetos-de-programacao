﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Painting : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_MovedPainting;
    public bool MovedPainting
    {
        get { return m_MovedPainting; }
        set { m_MovedPainting = value; }
    }

    [SerializeField]
    private Text m_Interaction;

    LevelManager m_LevelManagerAccess;

    [SerializeField]
    private AudioSource m_MovingFX;

    void Start()
    {
        m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        m_Interaction.enabled = false;
    }

    void Update()
    {
        if (m_PlayerIsColliding && !m_MovedPainting && m_LevelManagerAccess.KeyStatus && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Reveal");
            m_MovingFX.Play();
            m_MovedPainting = true;
            m_Interaction.enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if(m_PlayerIsColliding && !m_MovedPainting && m_LevelManagerAccess.KeyStatus)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }
    }
}
