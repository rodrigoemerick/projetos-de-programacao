﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandEnemySpawner : EnemySpawner
{
    protected override void Update()
    {
        if (HasSpawned && transform.childCount == 0)
        {
            HordeIsDefeated = true;
        }
    }
}