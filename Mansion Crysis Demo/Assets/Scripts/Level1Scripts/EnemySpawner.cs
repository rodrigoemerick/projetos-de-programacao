﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Enemy;

    [Header("Positions")]
    private float m_HordePosX;
    private float m_HordePosZ;
    [SerializeField]
    private float m_HordePosXMin;
    [SerializeField]
    private float m_HordePosXMax;
    [SerializeField]
    private float m_HordePosZMin;
    [SerializeField]
    private float m_HordePosZMax;
    [SerializeField]
    private float m_PosY;

    private int m_Counter;

    [SerializeField]
    private int m_HordeEnemyCounter;
    [SerializeField]
    private float m_SpawnGap;

    [Header("Bools")]
    private bool m_HasSpawned;
    public bool HasSpawned
    {
        get { return m_HasSpawned; }
        set { m_HasSpawned = value; }
    }

    [SerializeField]
    private bool m_HordeIsDefeated;
    public bool HordeIsDefeated
    {
        get { return m_HordeIsDefeated; }
        set { m_HordeIsDefeated = value; }
    }
    [SerializeField]
    private bool m_DependsOnLeverPulled;
    [SerializeField]
    private bool m_DependsOnKey;
    [SerializeField]
    private bool m_DependsOnFinalPuzzleSolved;
    
    LevelManager m_LevelManagerAccess;
    LevelManager2 m_LevelManager2Access;

    Scene m_CurrentScene;
    [SerializeField]
    private string m_SceneName;

    void Start()
    {
        m_CurrentScene = SceneManager.GetActiveScene();
        m_SceneName = m_CurrentScene.name;

        if (m_SceneName == "Nivel1")
        {
            m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        }
        else if(m_SceneName == "Nivel2")
        {
            m_LevelManager2Access = GameObject.Find("LevelManager").GetComponent<LevelManager2>();
        }
    }
    
    protected virtual void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && !m_HasSpawned)
        {
            StartCoroutine(EnemyHordeSpawner());
        }    
    }

    private IEnumerator EnemyHordeSpawner()
    {
        if(!m_DependsOnKey && !m_DependsOnLeverPulled && !m_DependsOnFinalPuzzleSolved)
        {
            while (m_Counter < m_HordeEnemyCounter)
            {
                yield return new WaitForSeconds(m_SpawnGap);
                InstantiateHorde();
            }
        }
        else if (m_SceneName == "Nivel1" && m_DependsOnLeverPulled && m_LevelManagerAccess.LeverStatus)
        {
            while (m_Counter < m_HordeEnemyCounter)
            {
                yield return new WaitForSeconds(m_SpawnGap);
                InstantiateHorde();
            }
        }
        else if (m_SceneName == "Nivel1" && m_DependsOnKey && m_LevelManagerAccess.KeyStatus)
        {
            while (m_Counter < m_HordeEnemyCounter)
            {
                yield return new WaitForSeconds(m_SpawnGap);
                InstantiateHorde();
            }
        }
        else if(m_SceneName == "Nivel2" && m_DependsOnFinalPuzzleSolved && m_LevelManager2Access.FinalPuzzleStatus)
        {
            while (m_Counter < m_HordeEnemyCounter)
            {
                yield return new WaitForSeconds(m_SpawnGap);
                InstantiateHorde();
            }
        }
    }

    void InstantiateHorde()
    {
        m_HordePosX = Random.Range(m_HordePosXMin, m_HordePosXMax);
        m_HordePosZ = Random.Range(m_HordePosZMin, m_HordePosZMax);
        GameObject temp = Instantiate(m_Enemy, new Vector3(m_HordePosX, m_PosY, m_HordePosZ), Quaternion.Euler(new Vector3(0.0f, -90.0f, 0.0f)));
        if (temp.GetComponent<HandEnemy>() != null) 
        {
            temp.GetComponent<HandEnemy>().SetNewParent(this.transform);
        }
        else
        {
            temp.GetComponent<EyeEnemy>().SetNewParent(this.transform);
        }
        ++m_Counter;
        m_HasSpawned = true;
    }
}
