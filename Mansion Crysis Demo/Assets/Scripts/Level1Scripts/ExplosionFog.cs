﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFog : MonoBehaviour
{
    [SerializeField]
    private float m_IncreaseFogIntensity;
    [SerializeField]
    private float m_FogStart;
    [SerializeField]
    private float m_FogEnd;
    [SerializeField]
    private float m_CoroutineIncTime;

    public void CallExplosion()
    {
        RenderSettings.fogColor = Color.white;
        RenderSettings.fog = true;
        RenderSettings.fogDensity = 1.0f;
        RenderSettings.fogMode = FogMode.Linear;
        RenderSettings.fogStartDistance = m_FogStart;
        RenderSettings.fogEndDistance = m_FogStart;
        StartCoroutine(DecreaseFogIntensity());
    }

    private IEnumerator DecreaseFogIntensity()
    {
        while (RenderSettings.fogEndDistance < m_FogEnd)
        {
            RenderSettings.fogEndDistance += m_IncreaseFogIntensity;
            yield return new WaitForSeconds(m_CoroutineIncTime);
        }
        RenderSettings.fog = false;
    }
}
