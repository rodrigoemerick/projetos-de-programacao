﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    StateManager m_LeverCheck;
    [SerializeField]
    private bool m_LeverStatus;
    public bool LeverStatus
    {
        get { return m_LeverStatus; }
        set { m_LeverStatus = value; }
    }

    Locker m_LockerCheck;
    [SerializeField]
    private bool m_LockerStatus;
    public bool LockerStatus
    {
        get { return m_LockerStatus; }
        set { m_LockerStatus = value; }
    }

    Key m_KeyCheck;
    [SerializeField]
    private bool m_KeyStatus;
    public bool KeyStatus
    {
        get { return m_KeyStatus; }
        set { m_KeyStatus = value; }
    }

    Painting m_PaintingCheck;
    [SerializeField]
    private bool m_PaintingStatus;
    public bool PaintingStatus
    {
        get { return m_PaintingStatus; }
        set { m_PaintingStatus = value; }
    }

    KeysPuzzle m_PuzzleCheck;
    [SerializeField]
    private bool m_PuzzleStatus;
    public bool PuzzleStatus
    {
        get { return m_PuzzleStatus; }
        set { m_PuzzleStatus = value; }
    }

    EnemySpawner m_HordeCheck;
    [SerializeField]
    private bool m_HordeIsDefeated;
    public bool HordeIsDefeated
    {
        get { return m_HordeIsDefeated; }
        set { m_HordeIsDefeated = value; }
    }

    void Start()
    {
        m_LeverCheck = GameObject.Find("lever").GetComponent<StateManager>();
        m_LockerCheck = GameObject.Find("lockerDoor").GetComponent<Locker>();
        m_KeyCheck = GameObject.Find("key").GetComponent<Key>();
        m_PaintingCheck = GameObject.Find("puzzlePainting").GetComponent<Painting>();
        m_PuzzleCheck = GameObject.Find("Puzzle").GetComponent<KeysPuzzle>();
        m_HordeCheck = GameObject.Find("1stHordeHandEnemy").GetComponent<HandEnemySpawner>();
    }

    void Update()
    {
        m_LeverStatus = m_LeverCheck.LeverPulled;
        m_LockerStatus = m_LockerCheck.IsOpen;
        m_KeyStatus = m_KeyCheck.HasKey;
        m_PaintingStatus = m_PaintingCheck.MovedPainting;
        m_PuzzleStatus = m_PuzzleCheck.PuzzleIsSolved;
        m_HordeIsDefeated = m_HordeCheck.HordeIsDefeated;
    }
}
