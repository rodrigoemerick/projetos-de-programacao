﻿public abstract class State<T>
{
    public abstract void Enter();
    public abstract void Exit();
    public abstract void Update();

    protected T m_Parent;

    public State(T parent)
    {
        m_Parent = parent;
    }
}
