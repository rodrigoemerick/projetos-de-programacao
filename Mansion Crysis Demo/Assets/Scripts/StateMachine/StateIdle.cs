﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateIdle : State<StateManager>
{
    public StateIdle(StateManager parent) : base(parent)
    {

    }

    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public override void Update()
    {
        if (m_Parent.PlayerIsColliding && !m_Parent.LeverPulled && Input.GetKeyDown(KeyCode.E))
        {
            m_Parent.LeverAnimator.SetTrigger("Pull");
            m_Parent.PullFX.Play();
            m_Parent.LeverPulled = true;
            m_Parent.Interaction.enabled = false;
            m_Parent.ChangeState("lever_pulled");
        }
    }
}
