﻿using UnityEngine;
using UnityEngine.UI;

public class StateManager : MonoBehaviour
{
    private StateMachine<StateManager> m_FSM;

    [SerializeField]
    private Animator m_LeverAnimator;
    public Animator LeverAnimator
    {
        get { return m_LeverAnimator; }
        set { m_LeverAnimator = value; }
    }

    [SerializeField]
    private bool m_PlayerIsColliding;
    public bool PlayerIsColliding
    {
        get { return m_PlayerIsColliding; }
        set { m_PlayerIsColliding = value; }
    }
    [SerializeField]
    private bool m_LeverPulled;
    public bool LeverPulled
    {
        get { return m_LeverPulled; }
        set { m_LeverPulled = value; }
    }

    [SerializeField]
    private Text m_Interaction;
    public Text Interaction
    {
        get { return m_Interaction; }
        set { m_Interaction = value; }
    }

    [SerializeField]
    private AudioSource m_PullFX;
    public AudioSource PullFX
    {
        get { return m_PullFX; }
        set { m_PullFX = value; }
    }

    void Awake()
    {
        m_FSM = new StateMachine<StateManager>(this);
        m_FSM.AddState("lever_idle", new StateIdle(this));
        m_FSM.AddState("lever_pulled", new StatePulled(this));
        ChangeState("lever_idle");    
    }

    void Update()
    {
        m_FSM.Update();
    }

    public void ChangeState(string stateName)
    {
        m_FSM.ChangeState(stateName);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if (!m_LeverPulled)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }
    }
}
