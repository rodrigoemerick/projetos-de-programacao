﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePulled : State<StateManager>
{
    public StatePulled(StateManager parent) : base(parent)
    {

    }

    public override void Enter()
    {

    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}
