﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<T>
{
    protected T m_Parent;

    private Dictionary<string, State<T>> m_StateDictionary;

    protected State<T> m_CurrentState;
    public State<T> CurrentState 
    { 
        get { return m_CurrentState; } 
    }

    public StateMachine(T parent)
    {
        m_Parent = parent;

        m_StateDictionary = new Dictionary<string, State<T>> ();

        m_CurrentState = null;
    }

    public void Update()
    {
        if(m_CurrentState != null)
        {
            m_CurrentState.Update();
        }
    }

    public bool IsInState(string stateName)
    {
        if (m_CurrentState == null) return false;

        return m_CurrentState == m_StateDictionary[stateName];
    }

    public void ChangeState(string stateName)
    {
        if (!m_StateDictionary.ContainsKey(stateName))
        {
            return;
        }

        if(m_StateDictionary[stateName] == m_CurrentState)
        {
            return;
        }

        if (m_CurrentState != null) m_CurrentState.Exit();

        m_CurrentState = m_StateDictionary[stateName];
        m_CurrentState.Enter();
    }

    public void ClearCurrentState()
    {
        if (m_CurrentState != null) m_CurrentState.Exit();

        m_CurrentState = null;
    }

    public bool AddState(string stateName, State<T> state, bool overwriteIfExists = true)
    {
        if (state == null)
        {
            return false;
        }

        if (overwriteIfExists)
        {
            m_StateDictionary[stateName] = state;
        }
        else
        {
            try
            {
                m_StateDictionary.Add(stateName, state);
            }
            catch(Exception e)
            {
                return false;
            }
        }

        return true;
    }

    public bool RemoveState(string stateName, bool forceInUse = false)
    {
        if (m_StateDictionary.ContainsKey(stateName))
        {
            if (m_CurrentState == m_StateDictionary[stateName])
            {
                if (forceInUse)
                {
                    m_CurrentState.Exit();
                    m_CurrentState = null;

                    return m_StateDictionary.Remove(stateName);
                }
                else
                {
                    return false;
                }
            }
        }

        return false;
    }
}
