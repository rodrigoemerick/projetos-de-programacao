﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door2 : MonoBehaviour
{
    public Animator m_Animator;

    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_DoorIsOpen;
    [SerializeField]
    private bool m_DependsOnLeverPulled;
    [SerializeField]
    private bool m_DependsOnKeyFound;
    [SerializeField]
    private bool m_DependsOnPuzzleSolved;

    [SerializeField]
    private Text m_Interaction;
    [SerializeField]
    private Text m_IsLocked;

    LevelManager2 m_LevelManagerAccess;

    [Header("Audios")]
    [SerializeField]
    private AudioSource m_DoorFX;
    [SerializeField]
    private AudioClip m_DoorOpenFX;
    [SerializeField]
    private AudioClip m_DoorCloseFX;

    void Start()
    {
        m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager2>();
        m_Interaction.enabled = false;
        m_IsLocked.enabled = false;
    }

    void Update()
    {
        if (m_PlayerIsColliding && !m_DoorIsOpen && Input.GetKeyDown(KeyCode.E))
        {
            if (m_DependsOnLeverPulled && m_LevelManagerAccess.LeverStatus)
            {
                m_Animator.SetTrigger("Open");
                m_DoorFX.clip = m_DoorOpenFX;
                m_DoorFX.Play();
                m_DoorIsOpen = true;
            }
            else if (m_DependsOnKeyFound && m_LevelManagerAccess.KeyStatus)
            {
                m_Animator.SetTrigger("Open");
                m_DoorFX.clip = m_DoorOpenFX;
                m_DoorFX.Play();
                m_DoorIsOpen = true;
            }
            else if (m_DependsOnPuzzleSolved && m_LevelManagerAccess.FinalPuzzleStatus)
            {
                m_Animator.SetTrigger("Open");
                m_DoorFX.clip = m_DoorOpenFX;
                m_DoorFX.Play();
                m_DoorIsOpen = true;
            }
            else if(!m_DependsOnLeverPulled && !m_DependsOnKeyFound && !m_DependsOnPuzzleSolved)
            {
                m_Animator.SetTrigger("Open");
                m_DoorFX.clip = m_DoorOpenFX;
                m_DoorFX.Play();
                m_DoorIsOpen = true;
            }
        }
        else if (m_PlayerIsColliding && m_DoorIsOpen && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Close");
            m_DoorFX.clip = m_DoorCloseFX;
            m_DoorFX.Play();
            m_DoorIsOpen = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if(m_DependsOnLeverPulled && !m_LevelManagerAccess.LeverStatus && !m_DoorIsOpen)
            {
                m_IsLocked.enabled = true;
            }
            else if (m_DependsOnLeverPulled && m_LevelManagerAccess.LeverStatus && !m_DoorIsOpen)
            {
                m_IsLocked.enabled = false;
                m_Interaction.enabled = true;
            }
            else if (m_DependsOnKeyFound && !m_LevelManagerAccess.KeyStatus && !m_DoorIsOpen)
            {
                m_IsLocked.enabled = true;
            }
            else if (m_DependsOnKeyFound && m_LevelManagerAccess.KeyStatus && !m_DoorIsOpen)
            {
                m_IsLocked.enabled = false;
                m_Interaction.enabled = true;
            }
            else if (m_DependsOnPuzzleSolved && !m_LevelManagerAccess.FinalPuzzleStatus && !m_DoorIsOpen)
            {
                m_IsLocked.enabled = true;
            }
            else if (m_DependsOnPuzzleSolved && m_LevelManagerAccess.FinalPuzzleStatus && !m_DoorIsOpen)
            {
                m_IsLocked.enabled = false;
                m_Interaction.enabled = true;
            }
            else if (!m_DependsOnKeyFound && !m_DependsOnLeverPulled && !m_DependsOnPuzzleSolved)
            {
                m_Interaction.enabled = true;
            }
            else if (m_DoorIsOpen)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
            m_IsLocked.enabled = false;
        }
    }
}