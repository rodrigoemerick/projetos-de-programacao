﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cupboard : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_CupboardIsOpen;
    public bool CupboardIsOpen
    {
        get { return m_CupboardIsOpen; }
        set { m_CupboardIsOpen = value; }
    }

    [SerializeField]
    private Text m_Interaction;

    [SerializeField]
    private AudioSource m_OpenFX;

    void Start()
    {
        m_Interaction.enabled = false;
    }

    
    void Update()
    {
        if(m_PlayerIsColliding && !m_CupboardIsOpen && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Open");
            m_OpenFX.Play();
            m_CupboardIsOpen = true;
            m_Interaction.enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if (!m_CupboardIsOpen)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }
    }
}
