﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalAlarm : MonoBehaviour
{
    [SerializeField]
    private AudioSource m_FinalAlarmClockFX;

    [SerializeField]
    private float m_TimeToPlayAlarm;
    [SerializeField]
    private float m_TimeToGoToMenu;

    void Start()
    {
        StartCoroutine(PlayAlarmAudio());
        StartCoroutine(GoToMenu());
    }

    private IEnumerator PlayAlarmAudio()
    {
        yield return new WaitForSeconds(m_TimeToPlayAlarm);
        if (!m_FinalAlarmClockFX.isPlaying)
        {
            m_FinalAlarmClockFX.Play();
        }
    }

    private IEnumerator GoToMenu()
    {
        yield return new WaitForSeconds(m_TimeToGoToMenu);
        SceneManager.LoadScene("Menu");
    }
}
