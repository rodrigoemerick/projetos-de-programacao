﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColectableItem : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private bool m_HasItem;
    public bool HasItem
    {
        get { return m_HasItem; }
        set { m_HasItem = value; }
    }

    [SerializeField]
    private Transform m_PuzzleNewParent;
    [SerializeField]
    private Vector3 m_NewTransformPosition;
    [SerializeField]
    private Quaternion m_NewTransformRotation;
    [SerializeField]
    private Vector3 m_NewTransformScale;

    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_CanCollectItem;

    [SerializeField]
    private Text m_Interaction;

    [SerializeField]
    private float m_AnimationTime;

    LevelManager2 m_LevelManagerAccess;

    [SerializeField]
    private AudioSource m_ItemCollectFX;

    void Start()
    {
        m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager2>();
        m_Interaction.enabled = false;
    }
    
    void Update()
    {
        if (m_PlayerIsColliding && !m_HasItem && m_LevelManagerAccess.BarrierStatus && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Collect");
            m_ItemCollectFX.Play();
            StartCoroutine(CollectAnimationTime());
            m_Interaction.enabled = false;
        }
    }

    private IEnumerator CollectAnimationTime()
    {
        yield return new WaitForSeconds(m_AnimationTime);
        gameObject.SetActive(false);
        m_HasItem = true;
        gameObject.transform.SetParent(m_PuzzleNewParent);
        transform.localPosition = m_NewTransformPosition;
        transform.localRotation = m_NewTransformRotation;
        transform.localScale = m_NewTransformScale;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if (!m_HasItem && m_LevelManagerAccess.BarrierStatus)
            {
                m_Interaction.enabled = true;
            }
        }    
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }    
    }
}
