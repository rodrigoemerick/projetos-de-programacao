﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key2 : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private bool m_HasKey;
    public bool HasKey
    {
        get { return m_HasKey; }
        set { m_HasKey = value; }
    }

    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_CanCatchKey;

    [SerializeField]
    private Text m_Interaction;

    [SerializeField]
    private float m_TimeToCatchKey;
    [SerializeField]
    private float m_AnimationTime;

    LevelManager2 m_LevelManagerAccess;
    
    [SerializeField]
    private AudioSource m_KeyCollectFX;

    void Start()
    {
        m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager2>();
        m_Interaction.enabled = false;
    }

    void Update()
    {
        if (m_LevelManagerAccess.CupboardStatus)
        {
            StartCoroutine(DelayToCatchKey());
        }
        if (m_PlayerIsColliding && !m_HasKey && m_CanCatchKey && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Collect");
            m_KeyCollectFX.Play();
            StartCoroutine(CollectAnimationTime());
            m_Interaction.enabled = false;
        }
    }

    private IEnumerator DelayToCatchKey()
    {
        yield return new WaitForSeconds(m_TimeToCatchKey);
        m_CanCatchKey = true;
    }

    private IEnumerator CollectAnimationTime()
    {
        yield return new WaitForSeconds(m_AnimationTime);
        gameObject.SetActive(false);
        m_HasKey = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if (!m_HasKey && m_CanCatchKey)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }
    }
}
