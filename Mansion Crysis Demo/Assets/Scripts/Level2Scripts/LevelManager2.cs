﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager2 : MonoBehaviour
{
    StateManager m_LeverCheck;
    [SerializeField]
    private bool m_LeverStatus;
    public bool LeverStatus
    {
        get { return m_LeverStatus; }
        set { m_LeverStatus = value; }
    }

    Cupboard m_CupboardCheck;
    [SerializeField]
    private bool m_CupboardStatus;
    public bool CupboardStatus
    {
        get { return m_CupboardStatus; }
        set { m_CupboardStatus = value; }
    }

    Key2 m_KeyCheck;
    [SerializeField]
    private bool m_KeyStatus;
    public bool KeyStatus
    {
        get { return m_KeyStatus; }
        set { m_KeyStatus = value; }
    }

    BarrierController m_BarrierCheck;
    [SerializeField]
    private bool m_BarrierStatus;
    public bool BarrierStatus
    {
        get { return m_BarrierStatus; }
        set { m_BarrierStatus = value; }
    }

    ColectableItem m_AshtrayCheck;
    [SerializeField]
    private bool m_AshtrayStatus;
    public bool AshtrayStatus
    {
        get { return m_AshtrayStatus; }
        set { m_AshtrayStatus = value; }
    }

    ColectableItem m_PortraitCheck;
    [SerializeField]
    private bool m_PortraitStatus;
    public bool PortraitStatus
    {
        get { return m_PortraitStatus; }
        set { m_PortraitStatus = value; }
    }

    ColectableItem m_SaucerCheck;
    [SerializeField]
    private bool m_SaucerStatus;
    public bool SaucerStatus
    {
        get { return m_SaucerStatus; }
        set { m_SaucerStatus = value; }
    }

    FinalPuzzle m_FinalPuzzleCheck;
    [SerializeField]
    private bool m_FinalPuzzleStatus;
    public bool FinalPuzzleStatus
    {
        get { return m_FinalPuzzleStatus; }
        set { m_FinalPuzzleStatus = value; }
    }

    void Start()
    {
        m_LeverCheck = GameObject.Find("lever").GetComponent<StateManager>();
        m_CupboardCheck = GameObject.Find("cupboard").GetComponent<Cupboard>();
        m_KeyCheck = GameObject.Find("key").GetComponent<Key2>();
        m_BarrierCheck = GameObject.Find("BarrierController").GetComponent<BarrierController>();
        m_AshtrayCheck = GameObject.Find("ashtray").GetComponent<ColectableItem>();
        m_PortraitCheck = GameObject.Find("portrait").GetComponent<ColectableItem>();
        m_SaucerCheck = GameObject.Find("saucerToCollect").GetComponent<ColectableItem>();
        m_FinalPuzzleCheck = GameObject.Find("finalPuzzlePanel").GetComponent<FinalPuzzle>();
    }

    void Update()
    {
        m_LeverStatus = m_LeverCheck.LeverPulled;
        m_CupboardStatus = m_CupboardCheck.CupboardIsOpen;
        m_KeyStatus = m_KeyCheck.HasKey;
        m_BarrierStatus = m_BarrierCheck.BarrierDisabled;
        m_AshtrayStatus = m_AshtrayCheck.HasItem;
        m_PortraitStatus = m_PortraitCheck.HasItem;
        m_SaucerStatus = m_SaucerCheck.HasItem;
        m_FinalPuzzleStatus = m_FinalPuzzleCheck.PuzzleIsSolved;
    }
}
