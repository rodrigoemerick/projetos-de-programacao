﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Barrier;

    [SerializeField]
    private bool m_BarrierDisabled;
    public bool BarrierDisabled
    {
        get { return m_BarrierDisabled; }
        set { m_BarrierDisabled = value; }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_Barrier.SetActive(false);
            m_BarrierDisabled = true;
        }    
    }
}
