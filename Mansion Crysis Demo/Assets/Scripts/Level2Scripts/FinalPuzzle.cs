﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalPuzzle : MonoBehaviour
{
    [SerializeField]
    private bool m_PlayerIsColliding;
    [SerializeField]
    private bool m_PuzzleIsSolved;
    public bool PuzzleIsSolved
    {
        get { return m_PuzzleIsSolved; }
        set { m_PuzzleIsSolved = value; }
    }

    LevelManager2 m_LevelManagerAccess;

    [SerializeField]
    private Text m_Interaction;

    [SerializeField]
    private GameObject m_Ashtray;
    [SerializeField]
    private GameObject m_Portrait;
    [SerializeField]
    private GameObject m_Saucer;

    [SerializeField]
    private AudioSource m_ObjectsOnPanelFX;

    void Start()
    {
        m_LevelManagerAccess = GameObject.Find("LevelManager").GetComponent<LevelManager2>();
        m_Interaction.enabled = false;
    }

    void Update()
    {
        if (m_PlayerIsColliding && m_LevelManagerAccess.AshtrayStatus && m_LevelManagerAccess.PortraitStatus && m_LevelManagerAccess.SaucerStatus && Input.GetKeyDown(KeyCode.E))
        {
            m_Ashtray.SetActive(true);
            m_Portrait.SetActive(true);
            m_Saucer.SetActive(true);
            m_Interaction.enabled = false;
            m_ObjectsOnPanelFX.Play();
            m_PuzzleIsSolved = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            if (m_PlayerIsColliding && m_LevelManagerAccess.AshtrayStatus && m_LevelManagerAccess.PortraitStatus && m_LevelManagerAccess.SaucerStatus)
            {
                m_Interaction.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_Interaction.enabled = false;
        }
    }
}
