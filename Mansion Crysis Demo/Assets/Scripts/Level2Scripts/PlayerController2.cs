﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aula.Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController2 : MonoBehaviour
    {
        [Header("Player speed")]
        [SerializeField]
        private float m_HorizontalSpeed;
        [SerializeField]
        private float m_VerticalSpeed;
        [SerializeField]
        private float m_RotationSpeed;
        private float m_TempHorizontalSpeed;
        private float m_TempVerticalSpeed;
        [SerializeField]
        private float m_EyeEnemyEffectSpeed;
        [SerializeField]
        private float m_IncSpeedValue;

        [Header("Player control")]
        [SerializeField]
        private bool m_IsTankMovement = true;
        [SerializeField]
        private bool m_IsRotationWithMouse = true;
        [SerializeField]
        private bool m_IsWalking = false;

        private Rigidbody m_Rigidbody;
        private Vector3 m_InputValues;
        private float m_HorizontalValue;

        [Header("Animations and Animators")]
        [SerializeField]
        private Animator m_CameraShakeAnimator;
        [SerializeField]
        private Animation m_CameraShakeAnimation;

        [Header("FearBar")]
        [SerializeField]
        private FearBarStatus m_PlayerFear;
        [SerializeField]
        private float m_InitialFear;
        [SerializeField]
        private float m_MaxFear;

        [Header("EspecialAmmoBar")]
        [SerializeField]
        private EspAmmoStatus m_EspAmmoStatus;
        [SerializeField]
        private float m_InitialEspAmmo;
        [SerializeField]
        private float m_MaxEspAmmo;

        [Header("Audio Sources and Audio Clips")]
        [SerializeField]
        private AudioSource m_HeartBeat;
        [SerializeField]
        private AudioSource m_TiredBreath;
        [SerializeField]
        private AudioSource m_Footsteps;
        [SerializeField]
        private AudioSource m_NormalBulletFX;
        [SerializeField]
        private AudioSource m_PlayerScream;
        [SerializeField]
        private AudioSource m_FinalAlarmClock;
        [SerializeField]
        private AudioSource m_Soundtrack;

        [SerializeField]
        private Vector3 m_ResetGamePosition;

        HandEnemy m_HandEnemy;

        [Header("Shooter System")]
        [SerializeField]
        private GameObject m_Bullet;
        [SerializeField]
        private GameObject m_EspecialBullet;
        [SerializeField]
        private GameObject m_BulletOrigin;
        [SerializeField]
        private bool m_CanFire = true;
        [SerializeField]
        private bool m_CanFireEspecial = false;
        [SerializeField]
        private GameObject m_ReloadController;

        [Header("HordeCheckers")]
        [SerializeField]
        private HandEnemySpawner m_HandFirstHorde;
        [SerializeField]
        private HandEnemySpawner m_HandSecondHorde;
        [SerializeField]
        private HandEnemySpawner m_HandThirdHorde;
        [SerializeField]
        private HandEnemySpawner m_HandFourthHorde;
        [SerializeField]
        private EyeEnemySpawner m_EyeFirstHorde;
        [SerializeField]
        private EyeEnemySpawner m_EyeSecondHorde;
        [SerializeField]
        private EyeEnemySpawner m_EyeThirdHorde;
        [SerializeField]
        private EyeEnemySpawner m_EyeFourthHorde;

        [Header("Check Bools for Inc Player Speed")]
        [SerializeField]
        private bool m_CanIncSpeedFirstHorde;
        [SerializeField]
        private bool m_CanIncSpeedSecondHorde;
        [SerializeField]
        private bool m_CanIncSpeedThirdHorde;
        [SerializeField]
        private bool m_CanIncSpeedFourthHorde;

        [Header("Audio Pitch Values")]
        [SerializeField]
        private float m_NormalPitchValue;
        [SerializeField]
        private float m_HeartAcceleratedPitch;
        [SerializeField]
        private float m_BreathAcceleratedPitch;
        [SerializeField]
        private float m_FearFillValueToAccelerate;

        [Header("Coroutine Time Values")]
        [SerializeField]
        private float m_ResetBulletPositionTime;
        [SerializeField]
        private float m_ResetEspecialBulletPositionTime;
        [SerializeField]
        private float m_TimeToReload;
        [SerializeField]
        private float m_InvencibilityTime;
        [SerializeField]
        private float m_TimeToPlayAlarm;

        [SerializeField]
        private PlayFadeAnimation m_Fade;

        GameManagerValues m_GameManagerAccess;
        
        private bool m_FirstResetGameCall = true;

        public bool IsTankMovement
        {
            get { return m_IsTankMovement; }
            set { m_IsTankMovement = value; }
        }

        public bool IsRotationWithMouse
        {
            get { return m_IsRotationWithMouse; }
            set { m_IsRotationWithMouse = value; }
        }

        [SerializeField]
        private Image m_EyeEnemyEffect;

        void Awake()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
            m_HandFirstHorde = GameObject.Find("1stHordeHandEnemy").GetComponent<HandEnemySpawner>();
            m_HandSecondHorde = GameObject.Find("2ndHordeHandEnemy").GetComponent<HandEnemySpawner>();
            m_HandThirdHorde = GameObject.Find("3rdHordeHandEnemy").GetComponent<HandEnemySpawner>();
            m_HandFourthHorde = GameObject.Find("4thHordeHandEnemy").GetComponent<HandEnemySpawner>();
            m_EyeFirstHorde = GameObject.Find("1stHordeEyeEnemy").GetComponent<EyeEnemySpawner>();
            m_EyeSecondHorde = GameObject.Find("2ndHordeEyeEnemy").GetComponent<EyeEnemySpawner>();
            m_EyeThirdHorde = GameObject.Find("3rdHordeEyeEnemy").GetComponent<EyeEnemySpawner>();
            m_EyeFourthHorde = GameObject.Find("4thHordeEyeEnemy").GetComponent<EyeEnemySpawner>();
            m_Fade = GameObject.Find("PlayFadeAnimation").GetComponent<PlayFadeAnimation>();
            m_GameManagerAccess = GameObject.Find("GameManager").GetComponent<GameManagerValues>();
            transform.tag = "Player";
            m_Bullet.SetActive(false);
            m_EspecialBullet.SetActive(false);
            m_ResetGamePosition = transform.position;
            m_PlayerFear.Init(m_InitialFear, m_MaxFear);
            m_EspAmmoStatus.Init(m_InitialEspAmmo, m_MaxEspAmmo);
            m_ReloadController.SetActive(false);
            m_EyeEnemyEffect.enabled = false;
            ResetGame();
        }

        private void ResetGame()
        {
            transform.position = m_ResetGamePosition;
            if (m_FirstResetGameCall)
            {
                m_PlayerFear.CurrentFearStatus = m_GameManagerAccess.OnLoadFearValue;
            }
            else
            {
                m_PlayerFear.CurrentFearStatus = m_InitialFear;
            }
            m_FirstResetGameCall = false;
            m_EspAmmoStatus.CurrentAmmoStatus = m_InitialEspAmmo;
            m_HeartBeat.Play();
            m_TiredBreath.Play();
        }

        void Start()
        {
            m_InputValues = Vector3.zero;
            m_TempHorizontalSpeed = m_GameManagerAccess.OnLoadHorizontalSpeed;
            m_TempVerticalSpeed = m_GameManagerAccess.OnLoadVerticalSpeed;
            m_HorizontalSpeed = m_TempHorizontalSpeed;
            m_VerticalSpeed = m_TempVerticalSpeed;
        }

        void Update()
        {
            m_InputValues.z = Input.GetAxis("Vertical") * m_VerticalSpeed * Time.deltaTime;

            if (m_IsTankMovement)
            {
                m_HorizontalValue = m_IsRotationWithMouse ? Input.GetAxis("Mouse X") : Input.GetAxis("Horizontal");
                m_InputValues.x = m_HorizontalValue * m_RotationSpeed;
            }
            else
            {
                m_HorizontalValue = Input.GetAxis("Horizontal");
                m_InputValues.x = m_HorizontalValue * m_HorizontalSpeed * Time.deltaTime;
            }

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                m_IsWalking = true;
            }
            else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                m_IsWalking = false;
            }

            if (m_IsWalking)
            {
                m_CameraShakeAnimator.SetBool("Walking", true);
                if (m_InputValues.z != 0.0f && !m_Footsteps.isPlaying)
                {
                    m_Footsteps.Play();
                }
            }
            else
            {
                m_CameraShakeAnimator.SetBool("Walking", false);
                m_Footsteps.Pause();
            }

            if (m_PlayerFear.CurrentFearStatus >= m_MaxFear)
            {
                ResetGame();
            }

            if (m_PlayerFear.CurrentFearStatus >= m_FearFillValueToAccelerate)
            {
                m_HeartBeat.pitch = m_HeartAcceleratedPitch;
                m_TiredBreath.pitch = m_BreathAcceleratedPitch;
            }
            else
            {
                m_HeartBeat.pitch = m_NormalPitchValue;
                m_TiredBreath.pitch = m_NormalPitchValue;
            }

            if (m_EspAmmoStatus.CurrentAmmoStatus >= m_MaxEspAmmo)
            {
                m_EspAmmoStatus.CurrentAmmoStatus = m_MaxEspAmmo;
                m_CanFireEspecial = true;
            }

            if (Input.GetMouseButtonDown(0))
            {
                FireBullet();
            }

            if (m_CanFireEspecial && Input.GetMouseButtonDown(1))
            {
                FireEspecialBullet();
            }
            
            //verificadores das hordas
            if (m_HandFirstHorde.HordeIsDefeated && m_EyeFirstHorde.HordeIsDefeated && !m_CanIncSpeedFirstHorde)
            {
                m_CanIncSpeedFirstHorde = true;
                IncreaseSpeed();
            }
            if (m_HandSecondHorde.HordeIsDefeated && m_EyeSecondHorde.HordeIsDefeated && !m_CanIncSpeedSecondHorde)
            {
                m_CanIncSpeedSecondHorde = true;
                IncreaseSpeed();
            }
            if (m_HandThirdHorde.HordeIsDefeated && m_EyeThirdHorde.HordeIsDefeated && !m_CanIncSpeedThirdHorde)
            {
                m_CanIncSpeedThirdHorde = true;
                IncreaseSpeed();
            }
            if (m_HandFourthHorde.HordeIsDefeated && m_EyeFourthHorde.HordeIsDefeated && !m_CanIncSpeedFourthHorde)
            {
                m_CanIncSpeedFourthHorde = true;
                IncreaseSpeed();
            }

            if (!m_Bullet.activeSelf)
            {
                m_Bullet.transform.position = m_BulletOrigin.transform.position;
                m_Bullet.transform.rotation = transform.rotation;
            }

            if (!m_EspecialBullet.activeSelf)
            {
                m_EspecialBullet.transform.position = m_BulletOrigin.transform.position;
                m_EspecialBullet.transform.rotation = transform.rotation;
            }
        }

        void FixedUpdate()
        {
            if (m_IsTankMovement)
            {
                m_Rigidbody.MovePosition(transform.position + transform.forward * m_InputValues.z);
                m_Rigidbody.MoveRotation(m_Rigidbody.rotation * Quaternion.Euler(0.0f, m_InputValues.x, 0.0f));
            }
            else
            {
                m_Rigidbody.MovePosition(transform.position + m_InputValues);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            
            if (other.gameObject.tag == "Finish")
            {
                m_Fade.PlayFadeToScene("Alarm");
            }

            if (other.gameObject.tag == "EyeEnemy")
            {
                m_HorizontalSpeed = m_EyeEnemyEffectSpeed;
                m_VerticalSpeed = m_EyeEnemyEffectSpeed;
                m_EyeEnemyEffect.enabled = true;
            }

            if (other.gameObject.tag == "EspecialAmmo")
            {
                m_EspAmmoStatus.CurrentAmmoStatus += Random.Range(1, 4);
            }
        }

        public void ResetSpeed()
        {
            m_HorizontalSpeed = m_TempHorizontalSpeed;
            m_VerticalSpeed = m_TempVerticalSpeed;
            m_EyeEnemyEffect.enabled = false;
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "EyeEnemy")
            {
                m_HorizontalSpeed = m_TempHorizontalSpeed;
                m_VerticalSpeed = m_TempVerticalSpeed;
                m_EyeEnemyEffect.enabled = false;
            }
        }

        void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == "HandEnemy")
            {
                m_HandEnemy = other.gameObject.GetComponent<HandEnemy>();
                m_PlayerFear.CurrentFearStatus += m_HandEnemy.Damage;
                m_PlayerScream.Play();
                Physics.IgnoreLayerCollision(8, 9, true);
                StartCoroutine(InvencibilityAfterDamage());
            }
        }

        void IncreaseSpeed()
        {
            m_TempHorizontalSpeed += m_IncSpeedValue;
            m_TempVerticalSpeed += m_IncSpeedValue;
            m_HorizontalSpeed = m_TempHorizontalSpeed;
            m_VerticalSpeed = m_TempVerticalSpeed;
        }

        void FireBullet()
        {
            if (m_CanFire)
            {
                m_Bullet.SetActive(true);
                m_CanFire = false;
                StartCoroutine(ResetBulletPosition());
                StartCoroutine(ReloadTime());
                m_ReloadController.SetActive(true);
                m_NormalBulletFX.Play();
            }
        }

        void FireEspecialBullet()
        {
            m_EspecialBullet.SetActive(true);
            m_EspAmmoStatus.CurrentAmmoStatus = 0;
            m_CanFire = false; //travar o tiro comum enquanto o tiro especial estiver acontecendo
            m_CanFireEspecial = false;
            StartCoroutine(ResetEspecialBulletPosition());
        }

        private IEnumerator ResetBulletPosition()
        {
            yield return new WaitForSeconds(m_ResetBulletPositionTime);
            m_Bullet.SetActive(false);
            m_Bullet.transform.position = m_BulletOrigin.transform.position;
        }

        private IEnumerator ResetEspecialBulletPosition()
        {
            yield return new WaitForSeconds(m_ResetEspecialBulletPositionTime);
            m_EspecialBullet.transform.position = m_BulletOrigin.transform.position;
            m_CanFire = true;
        }

        private IEnumerator ReloadTime()
        {
            yield return new WaitForSeconds(m_TimeToReload);
            m_ReloadController.SetActive(false);
            m_CanFire = true;
        }

        private IEnumerator InvencibilityAfterDamage()
        {
            yield return new WaitForSeconds(m_InvencibilityTime);
            Physics.IgnoreLayerCollision(8, 9, false);
        }
    }
}