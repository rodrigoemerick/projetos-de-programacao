﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowCredits : MonoBehaviour
{
    [SerializeField]
    private Image m_CreditsBackground;
    [SerializeField]
    private Text m_CreditsMessage;
    [SerializeField]
    private Text m_CreditsTextWhiteBack;
    [SerializeField]
    private Button m_Return;
    [SerializeField]
    private Text m_ReturnText;
    [SerializeField]
    private Text m_ReturnTextWhiteBack;

    void Start()
    {
        m_CreditsBackground.enabled = false;
        m_CreditsMessage.enabled = false;
        m_CreditsTextWhiteBack.enabled = false;
        m_Return.enabled = false;
        m_ReturnText.enabled = false;
        m_ReturnTextWhiteBack.enabled = false;
    }

    public void Show()
    {
        m_CreditsBackground.enabled = true;
        m_CreditsMessage.enabled = true;
        m_CreditsTextWhiteBack.enabled = true;
        m_Return.enabled = true;
        m_ReturnText.enabled = true;
        m_ReturnTextWhiteBack.enabled = true;
    }

    public void Hide()
    {
        m_CreditsBackground.enabled = false;
        m_CreditsMessage.enabled = false;
        m_CreditsTextWhiteBack.enabled = false;
        m_Return.enabled = false;
        m_ReturnText.enabled = false;
        m_ReturnTextWhiteBack.enabled = false;
    }
}
