﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayFadeAnimation : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    public void PlayFadeToScene(string parameterScene)
    {
        m_Animator.SetTrigger(parameterScene);
    }
}
