﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonRotation : MonoBehaviour
{

    public Transform m_ReferenceToRotate;
    public Transform m_SecondReference;

    public float m_LocalRotation;
    public float m_RotationAroundReference;
    public float m_RotationAroundSecondReference;

    void LateUpdate()
    {
        transform.Rotate(Vector3.up * m_LocalRotation * Time.deltaTime);
        transform.RotateAround(m_ReferenceToRotate.transform.position, Vector3.up, m_RotationAroundReference * Time.deltaTime);
        transform.RotateAround(m_SecondReference.transform.position, Vector3.up, m_RotationAroundSecondReference * Time.deltaTime);
    }
}
