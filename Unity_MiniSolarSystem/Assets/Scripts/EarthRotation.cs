﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthRotation : MonoBehaviour
{
    public Transform m_ReferenceToRotate;

    public float m_LocalRotation;
    public float m_RotationAroundReference;

    void LateUpdate()
    {
        transform.Rotate(Vector3.up * m_LocalRotation * Time.deltaTime);
        transform.RotateAround(m_ReferenceToRotate.transform.position, Vector3.up, m_RotationAroundReference * Time.deltaTime);
    }
}
