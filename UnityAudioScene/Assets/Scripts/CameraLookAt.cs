﻿using UnityEngine;

namespace Aula.Camera
{
    public class CameraLookAt : MonoBehaviour
    {
        [Header("Target (Object to look at)")]
        [SerializeField]
        private Transform m_LookAt;
        public Transform LookAt
        {
            set { m_LookAt = value; }
        }

        void LateUpdate()
        {
            if (m_LookAt != null)
            {
                transform.LookAt(m_LookAt.transform);
            }
        }

    }
}
