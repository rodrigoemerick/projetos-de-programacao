﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource m_AudioKey;
    public AudioSource m_CorridorLaugh;

    public bool m_TriggerIsKey = false;

    void Update()
    {
        if (m_TriggerIsKey && Input.GetKeyDown(KeyCode.E))
        {
            m_AudioKey.Play();
            m_TriggerIsKey = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Key")
        {
            m_TriggerIsKey = true;
        }
        else if(other.gameObject.tag == "Corridor")
        {
            m_CorridorLaugh.Play();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Key")
        {
            m_TriggerIsKey = false;
        }
    }
}
