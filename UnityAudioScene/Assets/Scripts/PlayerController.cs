﻿using UnityEngine;

namespace Aula.Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour
    {
        [Header("Player speed")]
        [SerializeField] private float m_HorizontalSpeed = 3.0f;
        [SerializeField] private float m_VerticalSpeed = 3.0f;
        [SerializeField] private float m_RotationSpeed = 5.0f;

        [Header("Player control")]
        [SerializeField] private bool m_IsTankMovement = true;
        [SerializeField] private bool m_IsRotationWithMouse = true;

        private Rigidbody m_Rigidbody;
        private Vector3 m_InputValues;
        private float m_HorizontalValue;

        public bool IsTankMovement
        {
            get { return m_IsTankMovement; }
            set { m_IsTankMovement = value; }
        }

        public bool IsRotationWithMouse
        {
            get { return m_IsRotationWithMouse; }
            set { m_IsRotationWithMouse = value; }
        }

        void Awake()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
        }

        void Start()
        {
            m_InputValues = Vector3.zero;
        }

        void Update()
        {
            m_InputValues.z = Input.GetAxis("Vertical") * m_VerticalSpeed * Time.deltaTime;

            if (m_IsTankMovement)
            {
                m_HorizontalValue = m_IsRotationWithMouse ? Input.GetAxis("Mouse X") : Input.GetAxis("Horizontal");
                m_InputValues.x = m_HorizontalValue * m_RotationSpeed;
            }
            else
            {
                m_HorizontalValue = Input.GetAxis("Horizontal");
                m_InputValues.x = m_HorizontalValue * m_HorizontalSpeed * Time.deltaTime;
            }
        }

        void FixedUpdate()
        {
            if (m_IsTankMovement)
            {
                m_Rigidbody.MovePosition(transform.position + transform.forward * m_InputValues.z);
                m_Rigidbody.MoveRotation(m_Rigidbody.rotation * Quaternion.Euler(0.0f, m_InputValues.x, 0.0f));
            }
            else
            {
                m_Rigidbody.MovePosition(transform.position + m_InputValues);
            }
        }
    }
}