﻿using UnityEngine;

namespace Aula.Camera
{
    [RequireComponent(typeof(BoxCollider))]
    public class CameraTrigger : MonoBehaviour
    {
        private BoxCollider m_BoxCollider;

        private UnityEngine.Camera m_Camera;
        public UnityEngine.Camera Camera
        {
            get { return m_Camera; }
        }

        private CameraManager m_Parent;
        public CameraManager Parent
        {
            set { m_Parent = value; }
        }

        void Start()
        {
            m_BoxCollider = GetComponent<BoxCollider>();
            m_BoxCollider.isTrigger = true;

            m_Camera = transform.GetChild(0).GetComponent<UnityEngine.Camera>();
        }

        public void EnableCamera()
        {
            if (m_Camera != null)
            {
                m_Camera.GetComponent<AudioListener>().enabled = true;
                m_Camera.enabled = true;
            }
        }

        public void DisableCamera()
        {
            if (m_Camera != null)
            {
                m_Camera.GetComponent<AudioListener>().enabled = false;
                m_Camera.enabled = false;
            }
        }
		
        void OnTriggerEnter(Collider other)
        {
            if (m_Parent != null)
            {
                m_Parent.SetCurrentCamera(this);
            }
        }
    }
}
