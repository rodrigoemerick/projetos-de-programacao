﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key : MonoBehaviour
{
    public bool m_HasKey = false;
    public bool m_PlayerIsColliding = false;

    public Image m_ButtonInteraction;
    public Image m_KeyStatus;

    public Light m_KeySpotLight;

    void Start()
    {
        m_ButtonInteraction = GameObject.Find("ButtonInteraction").GetComponent<Image>();
        m_ButtonInteraction.enabled = false;
        m_KeyStatus = GameObject.Find("KeyStatus").GetComponent<Image>();
        m_KeyStatus.enabled = false;
    }

    void Update()
    {
        if(m_PlayerIsColliding && !m_HasKey && Input.GetKeyDown(KeyCode.E))
        {
            gameObject.SetActive(false);
            m_HasKey = true;
            m_KeySpotLight.enabled = false;
            m_KeyStatus.enabled = true;
            m_ButtonInteraction.enabled = false;
        }    
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            m_ButtonInteraction.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_ButtonInteraction.enabled = false;
        }
    }
}
