﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Aula.Camera
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private CameraTrigger m_CurrentCamera;
        private List<CameraTrigger> m_CameraTriggerList;

        void Start()
        {
            m_CameraTriggerList = new List<CameraTrigger>();

            CameraTrigger cameraTrigger;
            foreach (Transform child in transform)
            {
                cameraTrigger = child.GetComponent<CameraTrigger>();
                if (cameraTrigger != null)
                {
                    Debug.Log("cam: " + child);
                    cameraTrigger.Parent = this;
                    cameraTrigger.DisableCamera();
                    m_CameraTriggerList.Add(cameraTrigger);
                }
            }

            m_CurrentCamera.EnableCamera();
        }

        private void DisableAllCameras()
        {
            for (int i = 0; i < m_CameraTriggerList.Count; ++i)
            {
                m_CameraTriggerList[i].DisableCamera();
            }
        }

        private void EnableAllCameras()
        {
            for (int i = 0; i < m_CameraTriggerList.Count; ++i)
            {
                m_CameraTriggerList[i].EnableCamera();
            }
        }

        public void SetCurrentCamera(CameraTrigger currentCamera)
        {
            if (m_CurrentCamera != currentCamera)
            {
                DisableAllCameras();

                m_CurrentCamera = currentCamera;
                m_CurrentCamera.EnableCamera();
            }
        }
    }
}
