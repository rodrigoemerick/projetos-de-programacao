﻿using UnityEngine;

namespace Aula.Camera
{
    public class CameraFollow : MonoBehaviour
    {
        [Header("Target (Object to follow)")]
        [SerializeField]
        private Transform m_Target;
        public Transform Target
        {
            set { m_Target = value; }
        }

        [SerializeField]
        private Vector3 m_TargetOffset;
        public Vector3 TargetOffset
        {
            get { return m_TargetOffset; }
            set { m_TargetOffset = value; }
        }

        [Header("Target (Object to look at)")]
        [SerializeField]
        private Transform m_LookAt;
        public Transform LookAt
        {
            set { m_LookAt = value; }
        }

        [SerializeField]
        private bool m_CanSwitchSides;
        public bool CanSwitchSides
        {
            get { return m_CanSwitchSides; }
            set { m_CanSwitchSides = value; }
        }

        private Quaternion m_TargetRotation;

        [Header("Camera")]
        [Tooltip("Set 0.0 for instant position")]
        [SerializeField]
        private float m_Speed = 0.0f;
        public float Speed
        {
            get { return m_Speed; }
            set { m_Speed = value; }
        }

        [SerializeField]
        private bool m_InvertYAxis = false;
        public bool InvertYAxis
        {
            get { return m_InvertYAxis; }
            set
            {
                m_InvertYAxis = value;
                m_YAxis = m_InvertYAxis ? -1.0f : 1.0f;
            }
        }

        [SerializeField]
        private float m_RotationSpeed = 5.0f;

        
        private float m_YRotation;
        private float m_YAxis;

        void Start()
        {
            m_YRotation = 0.0f;
            m_YAxis = m_InvertYAxis ? -1.0f : 1.0f;

            m_TargetOffset = transform.position - m_Target.position;
        }

        void Update()
        {
            if (m_CanSwitchSides && Input.GetButtonDown("Fire2"))
            {
                m_TargetOffset.x = -m_TargetOffset.x;
            }
        }

        void LateUpdate()
        {
            if (m_Target != null)
            {
                m_YRotation += Input.GetAxis("Mouse Y") * m_RotationSpeed * m_YAxis;
                m_TargetRotation = Quaternion.Euler(m_Target.transform.eulerAngles.x + m_YRotation, m_Target.transform.eulerAngles.y, m_Target.transform.eulerAngles.z);

                if (m_YRotation > 60.0f) m_YRotation = 60.0f;
                if (m_YRotation < -60.0f) m_YRotation = -60.0f;

                if (m_Speed == 0.0f)
                {
                    transform.position = m_Target.transform.position + (m_TargetRotation * m_TargetOffset);
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position,
                        m_Target.transform.position + (m_TargetRotation * m_TargetOffset),
                        m_Speed * Time.deltaTime);
                }
            }

            if (m_LookAt != null)
            {
                transform.LookAt(m_LookAt.transform);
            }
        }

    }
}
