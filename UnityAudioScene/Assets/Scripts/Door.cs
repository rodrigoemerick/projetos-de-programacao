﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    public Animator m_Animator;

    public Image m_ButtonInteraction;

    public bool m_DoorIsOpen = false;
    public bool m_PlayerIsColliding;
    public Key m_Key;

    public AudioSource m_DoorOpen;
    public AudioSource m_DoorClose;

    void Start()
    {
        m_Key = GameObject.Find("key").GetComponent<Key>();
        m_ButtonInteraction = GameObject.Find("ButtonInteraction").GetComponent<Image>();
        m_ButtonInteraction.enabled = false;
    }

    void Update()
    {
        if(m_PlayerIsColliding && !m_DoorIsOpen && m_Key.m_HasKey && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Open");
            m_DoorIsOpen = true;
            m_DoorOpen.Play();
        }

        else if (m_PlayerIsColliding && m_DoorIsOpen && Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetTrigger("Close");
            m_DoorIsOpen = false;
            m_DoorClose.Play();
        }
    }

    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = true;
            m_ButtonInteraction.enabled = true;
        }

        if (!m_Key.m_HasKey)
        {
            m_ButtonInteraction.enabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_PlayerIsColliding = false;
            m_ButtonInteraction.enabled = false;
        }
    }
}
