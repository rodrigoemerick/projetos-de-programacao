﻿using System.Collections;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] int m_TimeInSeconds;
    [SerializeField] int m_TargetTimeInSeconds;
    [SerializeField] bool m_IsDescending = true;

    TMPro.TMP_Text m_Text;

    int m_Seconds, m_Minutes, m_Hours, m_Days;
    const int m_HoursInADay = 24, m_TimeRate = 1, m_SecondsInADay = 86400, m_SecondsInAnHour = 3600, m_SecondsInAMinute = 60;

    void Start()
    {
        m_Text = GetComponent<TMPro.TMP_Text>();
        StartCoroutine(ChangeTime());
    }

    void Update()
    {
        m_Days = m_TimeInSeconds / m_SecondsInADay;
        m_Hours = (m_TimeInSeconds / m_SecondsInAnHour) % m_HoursInADay;
        m_Minutes = (m_TimeInSeconds / m_SecondsInAMinute) % m_SecondsInAMinute;
        m_Seconds = m_TimeInSeconds % m_SecondsInAMinute;

        m_Text.text = $"{m_Days}d:{m_Hours}h:{m_Minutes}m:{m_Seconds}s";    
    }

    void CoroutineLoop(bool isDescending)
    {
        m_TimeInSeconds += isDescending ? -m_TimeRate : m_TimeRate;

        StartCoroutine(ChangeTime());
    }

    IEnumerator ChangeTime()
    {
        yield return new WaitForSeconds(m_TimeRate);

        if (m_TimeInSeconds > m_TargetTimeInSeconds)
        {
            CoroutineLoop(true);
        }
        else if (m_TimeInSeconds < m_TargetTimeInSeconds)
        {
            CoroutineLoop(false);
        }
    }
}

//1 minute = 60 seconds
//1 hour = 60 minutes = 3600 seconds
//1 day = 24 hours = 1440 minutes = 86400 seconds