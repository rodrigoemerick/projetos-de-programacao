﻿public abstract class State<T>
{
    public abstract void Enter();
    public abstract void Exit();
    public abstract void Update();

    protected T parent;

    public State(T parent)
    {
        this.parent = parent;
    }
}
