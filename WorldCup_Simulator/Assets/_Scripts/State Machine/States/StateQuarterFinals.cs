using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateQuarterFinals : State<StateManager>
{
    public StateQuarterFinals(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        parent.QuarterFinalStage.CreateMatches();
    }

    public override void Exit()
    {
        
    }

    public override void Update()
    {

    }
}
