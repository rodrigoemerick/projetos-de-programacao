using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMenus : State<StateManager>
{
    public static Action<string> OnSetTriggerAnimation;

    public StateMenus(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        OnSetTriggerAnimation?.Invoke("main_menu_enter");
    }

    public override void Exit()
    {
        OnSetTriggerAnimation?.Invoke("main_menu_out_up");
        OnSetTriggerAnimation?.Invoke("simulation_screen_enter");
    }

    public override void Update()
    {

    }
}
