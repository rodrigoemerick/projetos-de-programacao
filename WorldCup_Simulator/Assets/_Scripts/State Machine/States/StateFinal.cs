using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFinal : State<StateManager>
{
    public static Action<string> OnSetTriggerAnimation;
    public static Action OnSave;
    
    public StateFinal(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        parent.FinalStage.CreateMatches();
    }

    public override void Exit()
    {
        OnSave.Invoke();
        OnSetTriggerAnimation?.Invoke("simulation_screen_out_down");
        OnSetTriggerAnimation?.Invoke("main_menu_enter");
}

    public override void Update()
    {

    }
}
