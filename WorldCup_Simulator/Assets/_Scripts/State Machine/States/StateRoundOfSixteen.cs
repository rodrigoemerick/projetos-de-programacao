using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateRoundOfSixteen : State<StateManager>
{
    public static Action OnEnterRoundOf16;

    public StateRoundOfSixteen(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        OnEnterRoundOf16?.Invoke();
        parent.RoundOfSixteensStage.CreateMatches();
    }

    public override void Exit()
    {
        
    }

    public override void Update()
    {
        
    }
}
