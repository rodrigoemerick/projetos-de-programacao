using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateGroupStage : State<StateManager>
{
    public StateGroupStage(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        parent.GroupStage.CallGroupCreation();
    }

    public override void Exit()
    {
        parent.GroupStage.SetClassifiedCountries();
    }

    public override void Update()
    {
        
    }
}
