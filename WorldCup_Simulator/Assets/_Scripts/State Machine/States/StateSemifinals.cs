using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateSemifinals : State<StateManager>
{
    public StateSemifinals(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        parent.SemifinalStage.CreateMatches();
    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}
