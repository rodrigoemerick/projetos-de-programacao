using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateThirdPlace : State<StateManager>
{
    public StateThirdPlace(StateManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        parent.ThirdPlaceStage.CreateMatches();
    }

    public override void Exit()
    {

    }

    public override void Update()
    {

    }
}
