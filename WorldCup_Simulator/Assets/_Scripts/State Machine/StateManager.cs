﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class StateManager : MonoBehaviour
{
    private StateMachine<StateManager> fsm;
    public StateMachine<StateManager> FSM { get => fsm; }

    public static StateManager instance;

    #region Serialized Fields for Stages
    [Header("World Cup Stages")]
    [SerializeField] GroupStage groupStage;
    public GroupStage GroupStage { get => groupStage; }

    [SerializeField] RoundOfSixteenStage roundOfSixteenStage;
    public RoundOfSixteenStage RoundOfSixteensStage { get => roundOfSixteenStage; }

    [SerializeField] QuarterFinalStage quarterFinalStage;
    public QuarterFinalStage QuarterFinalStage { get => quarterFinalStage; }

    [SerializeField] SemifinalStage semifinalStage;
    public SemifinalStage SemifinalStage { get => semifinalStage; }

    [SerializeField] ThirdPlaceStage thirdPlaceStage;
    public ThirdPlaceStage ThirdPlaceStage { get => thirdPlaceStage; }

    [SerializeField] FinalStage finalStage;
    public FinalStage FinalStage { get => finalStage; }
    #endregion

    [Header("Other settings")]
    [SerializeField] float startTimeCheckCurrentState;
    [SerializeField] float rateTimeCheckCurrentState;

    #region Actions
    public static Action<string> OnChangeCurrentStageText;
    #endregion

    void OnEnable()
    {
        UIManager.OnChangeState += ChangeState;
    }

    void OnDisable()
    {
        UIManager.OnChangeState -= ChangeState;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        fsm = new StateMachine<StateManager>(this);
        fsm.AddState("main_menu", new StateMenus(this));
        fsm.AddState("group_stage", new StateGroupStage(this));
        fsm.AddState("round_of_sixteen_stage", new StateRoundOfSixteen(this));
        fsm.AddState("quarter_finals_stage", new StateQuarterFinals(this));
        fsm.AddState("semifinals_stage", new StateSemifinals(this));
        fsm.AddState("third_place_stage", new StateThirdPlace(this));
        fsm.AddState("final_stage", new StateFinal(this));
    }

    void Start()
    {
        ChangeState("main_menu");
        InvokeRepeating("CheckCurrentState", startTimeCheckCurrentState, rateTimeCheckCurrentState);
    }

    void Update()
    {
        fsm.Update();
    }

    protected void ChangeState(string stateName)
    {
        fsm.ChangeState(stateName);
    }

    void CheckCurrentState()
    {
        if (fsm.IsInState("group_stage"))
        {
            if (MatchesManager.instance.CurrentMatch <= 16) 
            {
                OnChangeCurrentStageText?.Invoke("Group Stage - Round 1");
            }
            else if (MatchesManager.instance.CurrentMatch > 16 && MatchesManager.instance.CurrentMatch <= 32)
            {
                OnChangeCurrentStageText?.Invoke("Group Stage - Round 2");
            }
            else
            {
                OnChangeCurrentStageText?.Invoke("Group Stage - Round 3");
            }
        }
        if (fsm.IsInState("round_of_sixteen_stage"))
        {
            OnChangeCurrentStageText?.Invoke("Round of 16");
        }
        if (fsm.IsInState("quarter_finals_stage"))
        {
            OnChangeCurrentStageText?.Invoke("Quarter Finals");
        }
        if (fsm.IsInState("semifinals_stage"))
        {
            OnChangeCurrentStageText?.Invoke("Semifinals");
        }
        if (fsm.IsInState("third_place_stage"))
        {
            OnChangeCurrentStageText?.Invoke("Third Place");
        }
        if (fsm.IsInState("final_stage"))
        {
            OnChangeCurrentStageText?.Invoke("Final");
        }
    }
}
