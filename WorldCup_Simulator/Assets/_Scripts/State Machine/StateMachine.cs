﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<T>
{
    protected T parent;

    private Dictionary<string, State<T>> stateDict;

    protected State<T> currentState;
    public State<T> CurrentState 
    { 
        get { return currentState; } 
    }

    public StateMachine(T parent)
    {
        this.parent = parent;

        stateDict = new Dictionary<string, State<T>> ();

        currentState = null;
    }

    public void Update()
    {
        if (currentState != null)
        {
            currentState.Update();
        }
    }

    public bool IsInState(string stateName)
    {
        if (currentState == null) return false;

        return currentState == stateDict[stateName];
    }

    public void ChangeState(string stateName)
    {
        if (!stateDict.ContainsKey(stateName)) return;

        if (stateDict[stateName] == currentState) return;

        if (currentState != null) currentState.Exit();

        currentState = stateDict[stateName];
        currentState.Enter();
    }

    public void ClearCurrentState()
    {
        if (currentState != null) currentState.Exit();

        currentState = null;
    }

    public bool AddState(string stateName, State<T> state, bool overwriteIfExists = true)
    {
        if (state == null) return false;

        if (overwriteIfExists)
        {
            stateDict[stateName] = state;
        }
        else
        {
            try
            {
                stateDict.Add(stateName, state);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        return true;
    }

    public bool RemoveState(string stateName, bool forceInUse = false)
    {
        if (stateDict.ContainsKey(stateName))
        {
            if (currentState == stateDict[stateName])
            {
                if (forceInUse)
                {
                    currentState.Exit();
                    currentState = null;

                    return stateDict.Remove(stateName);
                }
                else
                {
                    return false;
                }
            }
        }

        return false;
    }
}
