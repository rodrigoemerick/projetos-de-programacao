using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveSystem
{
    public void SaveData();
    public void LoadData();
}
