using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IKnockoutStage
{
    void SendMatchesToMatchesManager(int initialMatch, int numberOfMatches);
    void CreateMatches();
}
