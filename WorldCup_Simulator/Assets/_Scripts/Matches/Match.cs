using System;
using System.Collections;
using System.Collections.Generic;

public class Match
{
    Country team1;
    public Country Team1 { get { return team1; } }
    Country team2;
    public Country Team2 { get { return team2; } }

    int team1Score;
    public int Team1Score { get { return team1Score; } }
    int team2Score;
    public int Team2Score { get { return team2Score; } }

    #region Draw method variables
    const int MIN_GOALS_IN_A_MATCH = 0;
    const int MAX_GOALS_IN_A_MATCH = 10;
    const int VICTORY_POINTS = 3;
    const int DRAW_POINTS = 1;
    #endregion

    public static Action<int> OnKnockoutMatchDrawWinner;

    public Match (Country team1, Country team2)
    {
        this.team1 = team1;
        this.team2 = team2;
    }

    public void SimulateMatchAndUpdateResultsGroupStage()
    {
        //Simulation logic
        team1Score = UnityEngine.Random.Range(MIN_GOALS_IN_A_MATCH, MAX_GOALS_IN_A_MATCH + 1);
        team2Score = UnityEngine.Random.Range(MIN_GOALS_IN_A_MATCH, MAX_GOALS_IN_A_MATCH + 1);

        //Updating results
        team1.goalsPro += team1Score;
        team1.goalsAgainst += team2Score;
        team2.goalsPro += team2Score;
        team2.goalsAgainst += team1Score;

        if (team1Score > team2Score)
        {
            ++team1.wins;
            ++team2.losses;
            team1.points += VICTORY_POINTS;
        }
        else if (team2Score > team1Score)
        {
            ++team2.wins;
            ++team1.losses;
            team2.points += VICTORY_POINTS;
        }
        else
        {
            ++team1.draws;
            ++team2.draws;
            team1.points += DRAW_POINTS;
            team2.points += DRAW_POINTS;
        }
    }

    public Country SimulateMatchAndUpdateResultsKnockoutStages()
    {
        //Simulation logic
        team1Score = UnityEngine.Random.Range(MIN_GOALS_IN_A_MATCH, MAX_GOALS_IN_A_MATCH + 1);
        team2Score = UnityEngine.Random.Range(MIN_GOALS_IN_A_MATCH, MAX_GOALS_IN_A_MATCH + 1);

        //Updating results
        team1.goalsPro += team1Score;
        team1.goalsAgainst += team2Score;
        team2.goalsPro += team2Score;
        team2.goalsAgainst += team1Score;

        if (team1Score > team2Score)
        {
            ++team1.wins;
            team1.points += VICTORY_POINTS;
            ++team2.losses;
            return team1;
        }
        else if (team2Score > team1Score)
        {
            ++team2.wins;
            team2.points += VICTORY_POINTS;
            ++team1.losses;
            return team2;
        }
        else
        {
            int winner = UnityEngine.Random.Range(0, 2);
            OnKnockoutMatchDrawWinner?.Invoke(winner);

            ++team1.draws;
            team1.points += DRAW_POINTS;
            ++team2.draws;
            team2.points += DRAW_POINTS;

            if (winner == 0)
            {
                return team1;
            }
            else
            {
                return team2;
            }
        }
    }
}
