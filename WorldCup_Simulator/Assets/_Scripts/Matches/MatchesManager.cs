using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchesManager : MonoBehaviour
{
    public static MatchesManager instance;

    #region Matches Management Variables
    const int TOTAL_MATCHES = 64;
    const int MATCHES_PER_GROUP_ROUND = 16;
    const int MATCHES_ROUND_OF_16 = 8;
    const int MATCHES_QUARTER_FINALS = 4;
    const int MATCHES_SEMIFINALS = 2;

    const int ROUND_OF_SIXTEEN_INITIAL_MATCH = 48;
    const int QUARTER_FINALS_INITIAL_MATCH = 56;
    const int SEMIFINALS_INITIAL_MATCH = 60;
    const int THIRD_PLACE_MATCH = 62;
    const int FINAL_MATCH = 63;

    Match[] matches;
    public Match[] Matches { get => matches; set => matches = value; }

    int currentMatch;
    public int CurrentMatch { get => currentMatch; }
    #endregion

    #region Actions
    public static Action OnMatchFinished;
    public static Action OnLoad;
    public static Action OnReachFinalMatch;
    #endregion

    private void OnEnable()
    {
        UIManager.OnNextDraw += NextDraw;
        UIManager.OnStartSimulation += ResetCurrentMatchVariable;
    }

    private void OnDisable()
    {
        UIManager.OnNextDraw -= NextDraw;
        UIManager.OnStartSimulation -= ResetCurrentMatchVariable;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        matches = new Match[TOTAL_MATCHES];
    }


    void Start()
    {
        if (!JSONSaveSystem.ShouldResetStatics)
        {
            OnLoad?.Invoke();
        }
    }

    void ResetCurrentMatchVariable()
    {
        currentMatch = 0;
    }

    void NextDraw()
    {
        switch (currentMatch)
        {
            case ROUND_OF_SIXTEEN_INITIAL_MATCH:
                ChangeStateCaseNotIn("round_of_sixteen_stage");
                break;
            case QUARTER_FINALS_INITIAL_MATCH:
                ChangeStateCaseNotIn("quarter_finals_stage");
                break;
            case SEMIFINALS_INITIAL_MATCH:
                ChangeStateCaseNotIn("semifinals_stage");
                break;
            case THIRD_PLACE_MATCH:
                ChangeStateCaseNotIn("third_place_stage");
                break;
            case FINAL_MATCH:
                ChangeStateCaseNotIn("final_stage");
                break;
        }

        if (StateManager.instance.FSM.IsInState("group_stage"))
        {
            matches[currentMatch].SimulateMatchAndUpdateResultsGroupStage();
            matches[currentMatch].Team1.group.SortGroup();
        }
        else
        {
            if (StateManager.instance.FSM.IsInState("round_of_sixteen_stage"))
            {
                int index = GetNextFreePositionIndex(StateManager.instance.RoundOfSixteensStage.Winners);

                if (index != -1)
                {
                    StateManager.instance.RoundOfSixteensStage.Winners[index] = matches[currentMatch].SimulateMatchAndUpdateResultsKnockoutStages();
                }
            }
            if (StateManager.instance.FSM.IsInState("quarter_finals_stage"))
            {
                int index = GetNextFreePositionIndex(StateManager.instance.QuarterFinalStage.Winners);

                if (index != -1)
                {
                    StateManager.instance.QuarterFinalStage.Winners[index] = matches[currentMatch].SimulateMatchAndUpdateResultsKnockoutStages();
                }
            }
            if (StateManager.instance.FSM.IsInState("semifinals_stage"))
            {
                int indexWinner = GetNextFreePositionIndex(StateManager.instance.SemifinalStage.Winners);

                if (indexWinner != -1)
                {
                    StateManager.instance.SemifinalStage.Winners[indexWinner] = matches[currentMatch].SimulateMatchAndUpdateResultsKnockoutStages();
                    //This line contains a ternary operator that gets the loser of the current semifinal match to populate the losers array for third place match
                    StateManager.instance.SemifinalStage.Losers[indexWinner] = matches[currentMatch].Team1 == StateManager.instance.SemifinalStage.Winners[indexWinner] ? matches[currentMatch].Team2 : matches[currentMatch].Team1;
                }
            }
            if (StateManager.instance.FSM.IsInState("third_place_stage"))
            {
                int index = GetNextFreePositionIndex(StateManager.instance.ThirdPlaceStage.Winners);

                if (index != -1)
                {
                    StateManager.instance.ThirdPlaceStage.Winners[index] = matches[currentMatch].SimulateMatchAndUpdateResultsKnockoutStages();
                }
            }
            if (StateManager.instance.FSM.IsInState("final_stage"))
            {
                int index = GetNextFreePositionIndex(StateManager.instance.FinalStage.Winners);

                if (index != -1)
                {
                    StateManager.instance.FinalStage.Winners[index] = matches[currentMatch].SimulateMatchAndUpdateResultsKnockoutStages();
                }
            }
        }

        OnMatchFinished?.Invoke();
        
        if (currentMatch < FINAL_MATCH)
        {
            ++currentMatch;
        }
        else
        {
            OnReachFinalMatch?.Invoke();
        }
    }

    void ChangeStateCaseNotIn(string state)
    {
        if (!StateManager.instance.FSM.IsInState(state))
        {
            StateManager.instance.FSM.ChangeState(state);
        }
    }

    public int GetNextFreePositionIndex(Country[] countries)
    {
        for (int i = 0; i < countries.Length; ++i)
        {
            if (countries[i] == null)
            {
                return i;
            }
        }

        return -1;
    }
}
