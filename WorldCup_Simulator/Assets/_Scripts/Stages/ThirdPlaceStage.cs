using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPlaceStage : MonoBehaviour, IKnockoutStage
{
    const int INITIAL_MATCH = 62;
    const int NUMBER_OF_MATCHES = 1;

    Match match;

    Country[] winners;
    public Country[] Winners { get => winners; set => winners = value; }

    [SerializeField] SemifinalStage semifinalStage;

    public static Action<int, int, int> OnUpdateKnockoutStageTable;

    void OnEnable()
    {
        UIManager.OnStartSimulation += ResetWinnersArray;
    }

    void OnDisable()
    {
        UIManager.OnStartSimulation -= ResetWinnersArray;
    }

    public void CreateMatches()
    {
        match = new Match(semifinalStage.Losers[0], semifinalStage.Losers[1]);

        SendMatchesToMatchesManager(INITIAL_MATCH, NUMBER_OF_MATCHES);
    }

    public void SendMatchesToMatchesManager(int initialMatch, int numberOfMatches)
    {
        MatchesManager.instance.Matches[INITIAL_MATCH] = match;

        OnUpdateKnockoutStageTable?.Invoke(INITIAL_MATCH, INITIAL_MATCH + NUMBER_OF_MATCHES, 28);
    }

    void ResetWinnersArray()
    {
        winners = new Country[NUMBER_OF_MATCHES];
    }
}
