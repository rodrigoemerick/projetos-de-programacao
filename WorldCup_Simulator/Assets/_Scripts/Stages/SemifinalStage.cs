using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SemifinalStage : MonoBehaviour, IKnockoutStage
{
    const int INITIAL_MATCH = 60;
    const int NUMBER_OF_MATCHES = 2;

    Match[] matches;

    Country[] winners;
    public Country[] Winners { get => winners; set => winners = value; }

    protected Country[] losers;
    public Country[] Losers { get => losers; set => losers = value; }

    [SerializeField] QuarterFinalStage quarterFinalStage;

    public static Action<int, int, int> OnUpdateKnockoutStageTable;

    void OnEnable()
    {
        UIManager.OnStartSimulation += ResetWinnersArray;
        UIManager.OnStartSimulation += ResetLosersArray;
    }

    void OnDisable()
    {
        UIManager.OnStartSimulation -= ResetWinnersArray;
        UIManager.OnStartSimulation -= ResetLosersArray;
    }

    void Start()
    {
        matches = new Match[NUMBER_OF_MATCHES];
    }

    public void CreateMatches()
    {
        for (int i = 0, j = 0; i < NUMBER_OF_MATCHES; ++i, j += 2)
        {
            matches[i] = new Match(quarterFinalStage.Winners[j], quarterFinalStage.Winners[j + 1]);
        }

        SendMatchesToMatchesManager(INITIAL_MATCH, NUMBER_OF_MATCHES);
    }

    public void SendMatchesToMatchesManager(int initialMatch, int numberOfMatches)
    {
        for (int i = initialMatch, j = 0; i < initialMatch + numberOfMatches; ++i, ++j)
        {
            MatchesManager.instance.Matches[i] = matches[j];
        }

        OnUpdateKnockoutStageTable?.Invoke(INITIAL_MATCH, INITIAL_MATCH + NUMBER_OF_MATCHES, 24);
    }

    void ResetWinnersArray()
    {
        winners = new Country[NUMBER_OF_MATCHES];
    }

    void ResetLosersArray()
    {
        losers = new Country[NUMBER_OF_MATCHES];
    }
}
