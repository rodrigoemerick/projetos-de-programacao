using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class GroupStage : MonoBehaviour
{
    const int MATCHES_PER_GROUP_ROUND = 16;
    const int TOTAL_GROUP_STAGE_MATCHES = 48;

    Match[] matches;

    [SerializeField] Group[] groups;
    public Group[] Groups { get => groups; }

    Country[] classifiedCountries;
    public Country[] ClassifiedCountries { get => classifiedCountries; }

    void Start()
    {
        matches = new Match[TOTAL_GROUP_STAGE_MATCHES];
        classifiedCountries = new Country[MATCHES_PER_GROUP_ROUND];
    }

    void ResetCurrentSimulationData()
    {
        for (int i = 0; i < groups.Length; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                Country curr = groups[i].groupCountries[j];
                curr.points = 0;
                curr.goalsPro = 0;
                curr.goalsAgainst = 0;
                curr.wins = 0;
                curr.draws = 0;
                curr.losses = 0;
            }
        }
    }

    public void CallGroupCreation()
    {
        ResetCurrentSimulationData();
        CreateGroupRoundsMatches();
        SendMatchesToMatchesManager();
    }

    void CreateGroupRoundsMatches()
    {
        int size = groups.Length;
        Match match1, match2;

        for (int i = 0, j = 0; i < size; ++i, j+= 2)
        {
            match1 = new Match(groups[i].groupCountries[0], groups[i].groupCountries[1]);
            match2 = new Match(groups[i].groupCountries[2], groups[i].groupCountries[3]);
            matches[j] = match1;
            matches[j + 1] = match2;
        }

        for (int i = 0, j = MATCHES_PER_GROUP_ROUND; i < size; ++i, j += 2)
        {
            match1 = new Match(groups[i].groupCountries[0], groups[i].groupCountries[2]);
            match2 = new Match(groups[i].groupCountries[1], groups[i].groupCountries[3]);
            matches[j] = match1;
            matches[j + 1] = match2;
        }

        for (int i = 0, j = MATCHES_PER_GROUP_ROUND * 2; i < size; ++i, j += 2)
        {
            match1 = new Match(groups[i].groupCountries[0], groups[i].groupCountries[3]);
            match2 = new Match(groups[i].groupCountries[1], groups[i].groupCountries[2]);
            matches[j] = match1;
            matches[j + 1] = match2;
        }
    }

    void SendMatchesToMatchesManager()
    {
        for (int i = 0; i < TOTAL_GROUP_STAGE_MATCHES; ++i)
        {
            MatchesManager.instance.Matches[i] = matches[i];
        }
    }

    public void SetClassifiedCountries()
    {
        for (int i = 0, j = 0; i < groups.Length; ++i, j += 2)
        {
            classifiedCountries[j] = groups[i].groupCountries[0];
            classifiedCountries[j + 1] = groups[i].groupCountries[1];
        }
    }
}
