using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundOfSixteenStage : MonoBehaviour, IKnockoutStage
{
    const int INITIAL_MATCH = 48;
    const int NUMBER_OF_MATCHES = 8;

    Match[] matches;

    Country[] winners;
    public Country[] Winners { get => winners; set => winners = value; }

    [SerializeField] GroupStage groupStage;

    public static Action<int, int, int> OnUpdateKnockoutStageTable;

    void OnEnable()
    {
        UIManager.OnStartSimulation += ResetWinnersArray;
    }

    void OnDisable()
    {
        UIManager.OnStartSimulation -= ResetWinnersArray;
    }

    void Start()
    {
        matches = new Match[NUMBER_OF_MATCHES];
    }

    public void CreateMatches()
    {
        //possible refactoring here
        matches[0] = new Match(groupStage.Groups[0].groupCountries[0], groupStage.Groups[1].groupCountries[1]);
        matches[1] = new Match(groupStage.Groups[2].groupCountries[0], groupStage.Groups[3].groupCountries[1]);
        matches[2] = new Match(groupStage.Groups[4].groupCountries[0], groupStage.Groups[5].groupCountries[1]);
        matches[3] = new Match(groupStage.Groups[6].groupCountries[0], groupStage.Groups[7].groupCountries[1]);
        matches[4] = new Match(groupStage.Groups[1].groupCountries[0], groupStage.Groups[0].groupCountries[1]);
        matches[5] = new Match(groupStage.Groups[3].groupCountries[0], groupStage.Groups[2].groupCountries[1]);
        matches[6] = new Match(groupStage.Groups[5].groupCountries[0], groupStage.Groups[4].groupCountries[1]);
        matches[7] = new Match(groupStage.Groups[7].groupCountries[0], groupStage.Groups[6].groupCountries[1]);

        SendMatchesToMatchesManager(INITIAL_MATCH, NUMBER_OF_MATCHES);
    }

    public void SendMatchesToMatchesManager(int initialMatch, int numberOfMatches)
    {
        for (int i = initialMatch, j = 0; i < initialMatch + numberOfMatches; ++i, ++j)
        {
            MatchesManager.instance.Matches[i] = matches[j];
        }

        OnUpdateKnockoutStageTable?.Invoke(INITIAL_MATCH, INITIAL_MATCH + NUMBER_OF_MATCHES, 0);
    }

    void ResetWinnersArray()
    {
        winners = new Country[NUMBER_OF_MATCHES];
    }
}
