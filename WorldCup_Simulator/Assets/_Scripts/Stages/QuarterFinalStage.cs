using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuarterFinalStage : MonoBehaviour, IKnockoutStage
{
    const int INITIAL_MATCH = 56;
    const int NUMBER_OF_MATCHES = 4;

    Match[] matches;

    Country[] winners;
    public Country[] Winners { get => winners; set => winners = value; }

    [SerializeField] RoundOfSixteenStage roundOfSixteenStage;

    public static Action<int, int, int> OnUpdateKnockoutStageTable;

    void OnEnable()
    {
        UIManager.OnStartSimulation += ResetWinnersArray;
    }

    void OnDisable()
    {
        UIManager.OnStartSimulation -= ResetWinnersArray;
    }

    void Start()
    {
        matches = new Match[NUMBER_OF_MATCHES];
    }

    public void CreateMatches()
    {
        for (int i = 0, j = 0; i < NUMBER_OF_MATCHES; ++i, j += 2)
        {
            matches[i] = new Match(roundOfSixteenStage.Winners[j], roundOfSixteenStage.Winners[j + 1]);
        }

        SendMatchesToMatchesManager(INITIAL_MATCH, NUMBER_OF_MATCHES);
    }

    public void SendMatchesToMatchesManager(int initialMatch, int numberOfMatches)
    {
        for (int i = initialMatch, j = 0; i < initialMatch + numberOfMatches; ++i, ++j)
        {
            MatchesManager.instance.Matches[i] = matches[j];
        }

        OnUpdateKnockoutStageTable?.Invoke(INITIAL_MATCH, INITIAL_MATCH + NUMBER_OF_MATCHES, 16);
    }

    void ResetWinnersArray()
    {
        winners = new Country[NUMBER_OF_MATCHES];
    }
}
