using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [Header("Simulation UI")]
    [SerializeField] Image country1Image;
    [SerializeField] Image country2Image;
    [SerializeField] TMP_Text country1Name;
    [SerializeField] TMP_Text country2Name;
    [SerializeField] TMP_Text country1Score;
    [SerializeField] TMP_Text country2Score;
    [SerializeField] TMP_Text currentStageText;
    [SerializeField] Image knockoutWinnerTeam1Image;
    [SerializeField] Image knockoutWinnerTeam2Image;
    [SerializeField] Button drawButton;
    [SerializeField] Button finishSimulationButton;
    [SerializeField] Button groupStageTablesButton;
    [SerializeField] Button knockoutStageTableButton;

    [Header("Menus Animations")]
    [SerializeField] Animator[] animators;

    [Header("Main Menu UI")]
    [SerializeField] Button startSimulation;
    [SerializeField] Button statisticsMenuButton;

    [Header("Statistics Menu UI")]
    [SerializeField] Button returnMainMenuButton;
    [SerializeField] Image[] statisticsCountriesFlags;
    [SerializeField] TMP_Text[] statisticsCountriesAmounts;
    [SerializeField] TMP_Text totalSimulationsText;

    [Header("Group Stage Tables Menu UI")]
    [SerializeField] Button returnToSimulationScreenFromGroupStageTablesButton;
    [SerializeField] GroupStageTable[] groupStageTables;
    [SerializeField] GroupStage groupStage;

    [Header("Knockout Stage Tables Menu UI")]
    [SerializeField] Button returnToSimulationScreenFromKnockoutStageTablesButton;
    [SerializeField] Image[] knockoutStageTableFlags;
    [SerializeField] Sprite interrogationMark;
    [SerializeField] MatchesManager matchesManager;

    [Header("Others")]
    [SerializeField] StatisticsTracker statisticsTracker;

    #region Actions
    public static Action OnNextDraw;
    public static Action<string> OnChangeState;
    public static Action OnStartSimulation;
    #endregion

    void OnEnable()
    {
        MatchesManager.OnMatchFinished += UpdateMatchUI;
        MatchesManager.OnReachFinalMatch += MakeDrawButtonNotInteractable;
        MatchesManager.OnReachFinalMatch += ActivateFinishSimulationButton;
        StateManager.OnChangeCurrentStageText += UpdateCurrentStageText;
        StateFinal.OnSetTriggerAnimation += SetTriggerAnimation;
        StateMenus.OnSetTriggerAnimation += SetTriggerAnimation;
        Match.OnKnockoutMatchDrawWinner += ActivateTrophyKnockoutWinner;
        StateRoundOfSixteen.OnEnterRoundOf16 += UpdateGroupStageTables;
        StateRoundOfSixteen.OnEnterRoundOf16 += MakeKnockoutStageTablesButtonInteractable;
        RoundOfSixteenStage.OnUpdateKnockoutStageTable += UpdateKnockoutStageTableFlags;
        QuarterFinalStage.OnUpdateKnockoutStageTable += UpdateKnockoutStageTableFlags;
        SemifinalStage.OnUpdateKnockoutStageTable += UpdateKnockoutStageTableFlags;
        ThirdPlaceStage.OnUpdateKnockoutStageTable += UpdateKnockoutStageTableFlags;
        FinalStage.OnUpdateKnockoutStageTable += UpdateKnockoutStageTableFlags;
    }

    void OnDisable()
    {
        MatchesManager.OnMatchFinished -= UpdateMatchUI;
        MatchesManager.OnReachFinalMatch -= MakeDrawButtonNotInteractable;
        MatchesManager.OnReachFinalMatch -= ActivateFinishSimulationButton;
        StateManager.OnChangeCurrentStageText -= UpdateCurrentStageText;
        StateFinal.OnSetTriggerAnimation -= SetTriggerAnimation;
        StateMenus.OnSetTriggerAnimation -= SetTriggerAnimation;
        Match.OnKnockoutMatchDrawWinner -= ActivateTrophyKnockoutWinner;
        StateRoundOfSixteen.OnEnterRoundOf16 -= UpdateGroupStageTables;
        StateRoundOfSixteen.OnEnterRoundOf16 -= MakeKnockoutStageTablesButtonInteractable;
        RoundOfSixteenStage.OnUpdateKnockoutStageTable -= UpdateKnockoutStageTableFlags;
        QuarterFinalStage.OnUpdateKnockoutStageTable -= UpdateKnockoutStageTableFlags;
        SemifinalStage.OnUpdateKnockoutStageTable -= UpdateKnockoutStageTableFlags;
        ThirdPlaceStage.OnUpdateKnockoutStageTable -= UpdateKnockoutStageTableFlags;
        FinalStage.OnUpdateKnockoutStageTable -= UpdateKnockoutStageTableFlags;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        drawButton.onClick.AddListener(CallNextDraw);
        startSimulation.onClick.AddListener(StartSimulation);
        finishSimulationButton.onClick.AddListener(SaveSimulationAndReturnToMainMenu);
        statisticsMenuButton.onClick.AddListener(GoToStatisticsMenu);
        returnMainMenuButton.onClick.AddListener(ReturnToMainMenu);
        groupStageTablesButton.onClick.AddListener(GoToGroupStageTablesScreen);
        returnToSimulationScreenFromGroupStageTablesButton.onClick.AddListener(GoToSimulationScreenFromGroupStageTables);
        knockoutStageTableButton.onClick.AddListener(GoToKnockoutStageTablesScreen);
        returnToSimulationScreenFromKnockoutStageTablesButton.onClick.AddListener(GoToSimulationScreenFromKnockoutStageTables);

        if (StatisticsTracker.TotalSimulations == 0)
        {
            statisticsMenuButton.interactable = false;
            InvokeRepeating("MakeStatisticsButtonInteractable", 0.0f, 0.2f);
        }
    }

    void UpdateMatchUI()
    {
        if (!country1Image.gameObject.activeInHierarchy)
        {
            country1Image.gameObject.SetActive(true);
            country2Image.gameObject.SetActive(true);
            country1Name.gameObject.SetActive(true);
            country2Name.gameObject.SetActive(true);
            country1Score.gameObject.SetActive(true);
            country2Score.gameObject.SetActive(true);
        }

        if (!drawButton.interactable)
        {
            drawButton.interactable = true;
        }
        if (finishSimulationButton.gameObject.activeInHierarchy)
        {
            finishSimulationButton.gameObject.SetActive(false);
        }

        MatchesManager manager = MatchesManager.instance;

        country1Image.sprite = manager.Matches[manager.CurrentMatch].Team1.flag;
        country2Image.sprite = manager.Matches[manager.CurrentMatch].Team2.flag;

        country1Name.text = manager.Matches[manager.CurrentMatch].Team1.name;
        country2Name.text = manager.Matches[manager.CurrentMatch].Team2.name;

        country1Score.text = $"{manager.Matches[manager.CurrentMatch].Team1Score}";
        country2Score.text = $"{manager.Matches[manager.CurrentMatch].Team2Score}";
    }

    void ResetMatchUIForNextSimulation()
    {
        country1Image.gameObject.SetActive(false);
        country2Image.gameObject.SetActive(false);
        country1Name.gameObject.SetActive(false);
        country2Name.gameObject.SetActive(false);
        country1Score.gameObject.SetActive(false);
        country2Score.gameObject.SetActive(false);
        knockoutWinnerTeam1Image.gameObject.SetActive(false);
        knockoutWinnerTeam2Image.gameObject.SetActive(false);
        drawButton.interactable = true;
        finishSimulationButton.gameObject.SetActive(false);
        knockoutStageTableButton.interactable = false;

        for (int i = 0; i < knockoutStageTableFlags.Length; ++i)
        {
            knockoutStageTableFlags[i].sprite = interrogationMark;
        }
    }

    void CallNextDraw()
    {
        if (knockoutWinnerTeam1Image.gameObject.activeInHierarchy)
        {
            knockoutWinnerTeam1Image.gameObject.SetActive(false);
        }
        if (knockoutWinnerTeam2Image.gameObject.activeInHierarchy)
        {
            knockoutWinnerTeam2Image.gameObject.SetActive(false);
        }

        OnNextDraw?.Invoke();
    }

    void StartSimulation()
    {
        OnStartSimulation?.Invoke();
        OnChangeState?.Invoke("group_stage");
        ResetMatchUIForNextSimulation();
    }

    void UpdateCurrentStageText(string text)
    {
        currentStageText.text = text;
    }

    void SetTriggerAnimation(string name)
    {
        for (int i = 0; i < animators.Length; ++i)
        {
            if (animators[i].HasState(0, Animator.StringToHash(name)))
            {
                animators[i].SetTrigger(name);
                break;
            }
        }
    }

    void ActivateTrophyKnockoutWinner(int winner)
    {
        bool activateTrophyOne = winner == 0;
        knockoutWinnerTeam1Image.gameObject.SetActive(activateTrophyOne);
        knockoutWinnerTeam2Image.gameObject.SetActive(!activateTrophyOne);
    }

    void MakeDrawButtonNotInteractable()
    {
        drawButton.interactable = false;
    }

    void ActivateFinishSimulationButton()
    {
        finishSimulationButton.gameObject.SetActive(true);
    }

    void SaveSimulationAndReturnToMainMenu()
    {
        OnChangeState?.Invoke("main_menu");
    }

    void GoToStatisticsMenu()
    {
        UpdateStatistics();
        SetTriggerAnimation("main_menu_out_right");
        SetTriggerAnimation("statistics_enter");
    }

    void ReturnToMainMenu()
    {
        SetTriggerAnimation("statistics_out");
        SetTriggerAnimation("main_menu_in_left");
    }

    void MakeStatisticsButtonInteractable()
    {
        if (StatisticsTracker.TotalSimulations > 0)
        {
            statisticsMenuButton.interactable = true;
        }
    }

    void UpdateStatistics()
    {
        Country[] statisticsCountries = statisticsTracker.GetStatistics();

        for (int i = 0; i < statisticsCountries.Length; ++i)
        {
            statisticsCountriesFlags[i].sprite = statisticsCountries[i].flag;
            statisticsCountriesAmounts[i].text = statisticsTracker.Records[i].ToString();
        }

        totalSimulationsText.text = $"Total Simulations: {StatisticsTracker.TotalSimulations}";
    }

    void GoToGroupStageTablesScreen()
    {
        StartCoroutine(WaitToGoToGroupStageTablesScreen());

        if (StateManager.instance.FSM.IsInState("group_stage"))
        {
            UpdateGroupStageTables();
        }
    }

    void GoToKnockoutStageTablesScreen()
    {
        StartCoroutine(WaitToGoToKnockoutStageTablesScreen());
    }

    void GoToSimulationScreenFromGroupStageTables()
    {
        SetTriggerAnimation("group_stage_table_out_right");
        SetTriggerAnimation("simulation_screen_in_right");
    }

    void GoToSimulationScreenFromKnockoutStageTables()
    {
        SetTriggerAnimation("knockout_stage_table_out_right");
        SetTriggerAnimation("simulation_screen_in_right");
    }

    void UpdateGroupStageTables()
    {
        for (int i = 0; i < groupStageTables.Length; ++i)
        {
            for (int j = 0; j < groupStageTables[i].countries.Length; ++j)
            {
                GroupStageCountryInTable curr = groupStageTables[i].countries[j];
                Country currCountry = groupStage.Groups[i].groupCountries[j];
                curr.countryFlag.sprite = currCountry.flag;
                curr.countryName.text = currCountry.name;
                curr.countryPoints.text = currCountry.points.ToString();
                curr.countryWins.text = currCountry.wins.ToString();
                curr.countryDraws.text = currCountry.draws.ToString();
                curr.countryLosses.text = currCountry.losses.ToString();
                curr.countryGoalsPro.text = currCountry.goalsPro.ToString();
                curr.countryGoalsAgainst.text = currCountry.goalsAgainst.ToString();
                curr.countryGoalsDifference.text = currCountry.GoalDifference.ToString();
            }
        }
    }

    IEnumerator WaitToGoToGroupStageTablesScreen()
    {
        SetTriggerAnimation("simulation_screen_out_left");
        yield return new WaitForSeconds(0.5f);
        SetTriggerAnimation("group_stage_table_in_left");
        returnToSimulationScreenFromGroupStageTablesButton.interactable = false;
        yield return new WaitForSeconds(1.0f);
        returnToSimulationScreenFromGroupStageTablesButton.interactable = true;
    }

    IEnumerator WaitToGoToKnockoutStageTablesScreen()
    {
        SetTriggerAnimation("simulation_screen_out_left");
        yield return new WaitForSeconds(0.5f);
        SetTriggerAnimation("knockout_stage_table_in_left");
        returnToSimulationScreenFromKnockoutStageTablesButton.interactable = false;
        yield return new WaitForSeconds(1.0f);
        returnToSimulationScreenFromKnockoutStageTablesButton.interactable = true;
    }

    void UpdateKnockoutStageTableFlags(int managerInitialMatch, int managerFinalMatch, int initialArrayNumber)
    {
        for (int i = managerInitialMatch, j = initialArrayNumber; i < managerFinalMatch; ++i, j+= 2)
        {
            knockoutStageTableFlags[j].sprite = matchesManager.Matches[i].Team1.flag;
            knockoutStageTableFlags[j + 1].sprite = matchesManager.Matches[i].Team2.flag;
        }
    }

    void MakeKnockoutStageTablesButtonInteractable()
    {
        knockoutStageTableButton.interactable = true;
    }
}
