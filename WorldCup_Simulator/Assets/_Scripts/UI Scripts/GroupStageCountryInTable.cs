using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GroupStageCountryInTable : MonoBehaviour
{
    public Image countryFlag;
    public TMP_Text countryName;
    public TMP_Text countryPoints;
    public TMP_Text countryWins;
    public TMP_Text countryDraws;
    public TMP_Text countryLosses;
    public TMP_Text countryGoalsPro;
    public TMP_Text countryGoalsAgainst;
    public TMP_Text countryGoalsDifference;
}
