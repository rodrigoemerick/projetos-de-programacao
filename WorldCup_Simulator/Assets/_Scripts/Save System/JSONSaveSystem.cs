using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSONSaveSystem : MonoBehaviour, ISaveSystem
{
    //Testing purposes
    static bool shouldResetStatistics;
    public static bool ShouldResetStatics { get => shouldResetStatistics; }

    [Header("Stages")]
    [SerializeField] GroupStage groupStage;
    [SerializeField] FinalStage finalStage;

    private CountryData[] countries;

    void Awake()
    {
        shouldResetStatistics = false;

        if (shouldResetStatistics)
        {
            for (int i = 0; i < groupStage.Groups.Length; ++i)
            {
                int groupLength = 4;
                for (int j = 0; j < groupLength; ++j)
                {
                    Country curr = groupStage.Groups[i].groupCountries[j];
                    curr.totalPoints = 0;
                    curr.totalGoalsPro = 0;
                    curr.totalGoalsAgainst = 0;
                    curr.totalWins = 0;
                    curr.totalDraws = 0;
                    curr.totalLosses = 0;
                    curr.totalWorldCupsWon = 0;
                }
            }
        }
        for (int i = 0; i < groupStage.Groups.Length; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                Country curr = groupStage.Groups[i].groupCountries[j];
                curr.points = 0;
                curr.goalsPro = 0;
                curr.goalsAgainst = 0;
                curr.wins = 0;
                curr.draws = 0;
                curr.losses = 0;
            }
        }

        countries = new CountryData[32];
    }

    void OnEnable() 
    {
        StateFinal.OnSave += RegisterFinalWinnerToStatistics;
        MatchesManager.OnLoad += LoadData;
    }

    void OnDisable()
    {
        StateFinal.OnSave -= RegisterFinalWinnerToStatistics;
        MatchesManager.OnLoad -= LoadData;
    }

    void Start()
    {
        //countries = new CountryData[32];
    }

    void SumCurrentStatsToTotalData()
    {
        for (int i = 0; i < groupStage.Groups.Length; ++i)
        {
            int groupLength = 4;
            for (int j = 0; j < groupLength; ++j)
            {
                Country curr = groupStage.Groups[i].groupCountries[j];
                curr.totalPoints += curr.points;
                curr.totalGoalsPro += curr.goalsPro;
                curr.totalGoalsAgainst += curr.goalsAgainst;
                curr.totalWins += curr.wins;
                curr.totalDraws += curr.draws;
                curr.totalLosses += curr.losses;
            }
        }
    }

    void GetCountriesDataToSave()
    {
        SumCurrentStatsToTotalData();

        for (int i = 0; i < groupStage.Groups.Length; ++i)
        {
            int groupLength = 4;
            for (int j = 0; j < groupLength; ++j)
            {
                CountryData data = new CountryData(groupStage.Groups[i].groupCountries[j]);
                countries[(i * 4) + j] = data;
            }
        }
    }

    void GetCountriesDataToLoad()
    {
        for (int i = 0; i < groupStage.Groups.Length; ++i)
        {
            int groupLength = 4;
            for (int j = 0; j < groupLength; ++j)
            {
                CountryData data = new CountryData(groupStage.Groups[i].groupCountries[j]);
                countries[(i * 4) + j] = data;
            }
        }
    }

    public void SaveData()
    {
        GetCountriesDataToSave();

        for (int i = 0; i < countries.Length; ++i)
        {
            string persistentPath = Application.persistentDataPath + "SaveData" + countries[i].name + ".json";
            WriteJson(persistentPath, countries[i]);
        }

        GeneralStatisticsData gsd = new GeneralStatisticsData();
        string persistentStatisticsPath = Application.persistentDataPath + "SaveDataGeneralStatistics.json";
        WriteJson(persistentStatisticsPath, gsd);
    }

    public void LoadData()
    {
        GetCountriesDataToLoad();

        for (int i = 0; i < countries.Length; ++i)
        {
            if (File.Exists(Application.persistentDataPath + "SaveData" + countries[i].name + ".json"))
            {
                string persistentPath = Application.persistentDataPath + "SaveData" + countries[i].name + ".json";
                using StreamReader reader = new StreamReader(persistentPath);

                string json = reader.ReadToEnd();
                CountryData loadedCountry = JsonUtility.FromJson<CountryData>(json);

                Country currCountryToLoad = FindCountryByName(loadedCountry.name);
                currCountryToLoad.totalPoints = loadedCountry.totalPoints;
                currCountryToLoad.totalGoalsPro = loadedCountry.totalGoalsPro;
                currCountryToLoad.totalGoalsAgainst = loadedCountry.totalGoalsAgainst;
                currCountryToLoad.totalWins = loadedCountry.totalWins;
                currCountryToLoad.totalDraws = loadedCountry.totalDraws;
                currCountryToLoad.totalLosses = loadedCountry.totalLosses;
                currCountryToLoad.totalWorldCupsWon = loadedCountry.totalWorldCupsWon;

                reader.Close();
            }
        }

        LoadGeneralStatistics();
    }

    Country FindCountryByName(string name)
    {
        int groupLength = 4;
        for (int i = 0; i < groupStage.Groups.Length; ++i)
        {
            for (int j = 0; j < groupLength; ++j)
            {
                Country curr = groupStage.Groups[i].groupCountries[j];
                if (curr.name == name)
                {
                    return curr;
                }
            }
        }
        
        return null;
    }

    void RegisterFinalWinnerToStatistics()
    {
        Country winner = finalStage.Winners[0];
        ++winner.totalWorldCupsWon;

        SaveData();
    }

    void WriteJson(string persistentPath, object itemToSave)
    {
        string savePath = persistentPath;
        using StreamWriter writer = new StreamWriter(savePath);
        string json = JsonUtility.ToJson(itemToSave);
        writer.Write(json);
        writer.Close();
    }

    void LoadGeneralStatistics()
    {
        if (File.Exists(Application.persistentDataPath + "SaveDataGeneralStatistics.json"))
        {
            string persistentPath = Application.persistentDataPath + "SaveDataGeneralStatistics.json";
            using StreamReader reader = new StreamReader(persistentPath);

            string json = reader.ReadToEnd();
            GeneralStatisticsData loadedStatistics = JsonUtility.FromJson<GeneralStatisticsData>(json);
            StatisticsTracker.TotalSimulations = loadedStatistics.totalSimulations;

            reader.Close();
        }
    }
}
