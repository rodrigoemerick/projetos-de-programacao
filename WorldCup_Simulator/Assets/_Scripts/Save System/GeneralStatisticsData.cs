using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GeneralStatisticsData
{
    public int totalSimulations;

    public GeneralStatisticsData()
    {
        IncrementTotalSimulations();
    }

    void IncrementTotalSimulations()
    {
        ++StatisticsTracker.TotalSimulations;
        totalSimulations = StatisticsTracker.TotalSimulations;
    }
}
