using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CountryData
{
    public string name;
    public int totalPoints;
    public int totalGoalsPro;
    public int totalGoalsAgainst;
    public int totalWins;
    public int totalDraws;
    public int totalLosses;
    public int totalWorldCupsWon;

    public CountryData(Country country)
    {
        this.name = country.name;
        this.totalPoints = country.totalPoints;
        this.totalGoalsPro = country.totalGoalsPro;
        this.totalGoalsAgainst = country.totalGoalsAgainst;
        this.totalWins = country.totalWins;
        this.totalDraws = country.totalDraws;
        this.totalLosses = country.totalLosses;
        this.totalWorldCupsWon = country.totalWorldCupsWon;
    }
}
