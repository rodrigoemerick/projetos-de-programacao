using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Country")]
public class Country : ScriptableObject
{
    //Current simulation data
    public Group group;
    public string name;
    public Sprite flag;
    public int points;
    public int goalsPro;
    public int goalsAgainst;
    public int GoalDifference => goalsPro - goalsAgainst;
    public int wins;
    public int draws;
    public int losses;

    //Statistics simulation data
    public int totalPoints;
    public int totalGoalsPro;
    public int totalGoalsAgainst;
    public int totalWins;
    public int totalDraws;
    public int totalLosses;
    public int totalWorldCupsWon;
}
