using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Group")]
public class Group : ScriptableObject
{
    public Country[] groupCountries;
    
    //The sorting method used will be Bubble Sort as we have a small amount of items to sort
    public void SortGroup()
    {
        int size = groupCountries.Length;

        for (int i = 0; i < size - 1; ++i)
        {
            bool swapped = false;
            for (int j = 0; j < size - i - 1; ++j)
            {
                //Checking number of points
                if (groupCountries[j].points == groupCountries[j + 1].points)
                {
                    //Checking goal difference
                    if (groupCountries[j].GoalDifference == groupCountries[j + 1].GoalDifference)
                    {
                        //Draw to choose which Country will be ahead
                        if (Random.Range(0, 2) == 0)
                        {
                            SwapCountries(j, j + 1);
                            swapped = true;
                        }
                    }
                    else if (groupCountries[j].GoalDifference < groupCountries[j + 1].GoalDifference)
                    {
                        SwapCountries(j, j + 1);
                        swapped = true;
                    }
                }
                else if (groupCountries[j].points < groupCountries[j + 1].points)
                {
                    SwapCountries(j, j + 1);
                    swapped = true;
                }
            }

            if (!swapped)
            {
                break;
            }
        }
    }

    void SwapCountries(int i, int j)
    {
        Country temp = groupCountries[i];
        groupCountries[i] = groupCountries[j];
        groupCountries[j] = temp;
    }
}
