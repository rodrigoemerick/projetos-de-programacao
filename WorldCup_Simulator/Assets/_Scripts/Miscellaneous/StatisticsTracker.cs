using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsTracker : MonoBehaviour
{
    static int totalSimulations;
    public static int TotalSimulations { get => totalSimulations; set => totalSimulations = value; }

    int[] records = { 0, 0, 0, 0, 0, 0 };
    public int[] Records { get => records; }

    Country[] recordCountries;
    public Country[] RecordCountries { get => recordCountries; }

    [SerializeField] GroupStage groupStage;

    void Start()
    {
        recordCountries = new Country[6];
    }

    public Country[] GetStatistics()
    {
        int groupLength = 4;
        for (int i = 0; i < groupStage.Groups.Length; ++i)
        {
            for (int j = 0; j < groupLength; ++j)
            {
                Country curr = groupStage.Groups[i].groupCountries[j];

                if (curr.totalWorldCupsWon > records[0])
                {
                    records[0] = curr.totalWorldCupsWon;
                    recordCountries[0] = curr;
                }
                if (curr.totalWins > records[1])
                {
                    records[1] = curr.totalWins;
                    recordCountries[1] = curr;
                }
                if (curr.totalDraws > records[2])
                {
                    records[2] = curr.totalDraws;
                    recordCountries[2] = curr;
                }
                if (curr.totalLosses > records[3])
                {
                    records[3] = curr.totalLosses;
                    recordCountries[3] = curr;
                }
                if (curr.totalGoalsPro > records[4])
                {
                    records[4] = curr.totalGoalsPro;
                    recordCountries[4] = curr;
                }
                if (curr.totalGoalsAgainst > records[5])
                {
                    records[5] = curr.totalGoalsAgainst;
                    recordCountries[5] = curr;
                }
            }
        }

        return recordCountries;
    }
}
