using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioSource worldCupBeat;
    [SerializeField] AudioSource worldCupHayyaHayya;
    [SerializeField] AudioSource clickSFX;

    public static AudioManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        StartCoroutine(PlayAudioBeatAndLoopMusic());
    }

    IEnumerator PlayAudioBeatAndLoopMusic()
    {
        worldCupBeat.Play();
        yield return new WaitForSeconds(worldCupBeat.clip.length);
        worldCupHayyaHayya.Play();
    }

    public void PlayClickAudio()
    {
        if (!clickSFX.isPlaying)
        {
            clickSFX.Play();
        }
        else
        {
            clickSFX.Stop();
            clickSFX.Play();
        }
    }
}
