﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Globalization;

namespace _31857086_TreinoPerceptron
{
    class Program
    {
        static void Main(string[] args)
        {
            string m_TryAgain = "yes";
            while (m_TryAgain.ToLower() != "no")
            {
                NumberFormatInfo number = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
                number.NumberDecimalSeparator = ".";

                Console.WriteLine("Forneça a quantidade de neurônios na camada oculta.");
                int m_QtyNeuronsHiddenLayer = int.Parse(Console.ReadLine());

                Console.WriteLine("Forneça a quantidade de neurônios na camada de saída.");
                int m_QtyNeuronsOutputLayer = int.Parse(Console.ReadLine());

                int m_NumOfGens = 50000;
                double m_LearningRate = 0.005;

                string m_LinkTrain = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\iris.train.txt";
                string m_LinkTest = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\iris.test.txt";

                //PERCEPTRON TRAINING VARIABLES
                Random m_Random = new Random((int)DateTime.UtcNow.Ticks);
                List<double[]> m_Inputs = new List<double[]>(); //All inputs for one perceptron
                List<double> m_ReadInputs = new List<double>(); //Inputs read from the file line to line
                List<double[]> m_Input_HiddenWeights = new List<double[]>();
                List<double[]> m_Hidden_OutputWeights = new List<double[]>();
                double m_Bias = 0.0;
                double m_BreakCondition = 0.02;
                double m_Percentage = 0.8; //a reference value for backpropagation break condition calculation

                RandomizeWeights();

                List<string> m_ExpectedResults = new List<string>(); //the string results from the file
                List<double> m_ConvertedResults = new List<double>(); //the double-converted results from the file

                Console.WriteLine("\n");
                Console.WriteLine("Before training:\n");

                //---------------------------DEBUGGING---------------------------
                DebugWeights(m_Input_HiddenWeights, m_Hidden_OutputWeights, false);

                ReadFile(m_LinkTrain); //reading training file

                //-----------------TRAINING-----------------
                //Calc each line in dataset and train weights
                List<double> m_HidLayerOutputs = new List<double>(); //Outputs of hidden layer
                List<double> m_OutLayerOutputs = new List<double>(); //Outputs of output layer
                Perceptron m_HiddenPerceptron = new Perceptron();
                Perceptron m_OutPerceptron = new Perceptron();

                int currentGen = 0;
                Train();

                Console.WriteLine("\n\n");
                Console.WriteLine("After training:\n");
                //---------------------------DEBUGGING---------------------------
                DebugWeights(m_Input_HiddenWeights, m_Hidden_OutputWeights, false);

                //-----------------VALIDATING-----------------
                Console.WriteLine("\n\n");
                Console.WriteLine("Results of the test dataset:\n");
                ReadFile(m_LinkTest);

                for (int i = 0; i < m_Inputs.Count - 1; ++i) //for some reason, the test dataset has an additional empty line, for is iterating until .Count - 1 to exclude it
                {
                    m_HidLayerOutputs.Clear();
                    m_OutLayerOutputs.Clear();

                    for (int j = 0; j < m_QtyNeuronsHiddenLayer; ++j)
                    {
                        m_HiddenPerceptron.SetPerceptron(m_Inputs[i], m_Input_HiddenWeights[j], m_Bias);
                        m_HidLayerOutputs.Add(m_HiddenPerceptron.Output);
                    }

                    for (int j = 0; j < m_QtyNeuronsOutputLayer; ++j)
                    {
                        m_OutPerceptron.SetPerceptron(m_HidLayerOutputs, m_Hidden_OutputWeights[j], m_Bias);
                        m_OutLayerOutputs.Add(m_OutPerceptron.Result(m_OutPerceptron.Output));
                        //Console.WriteLine("Output: " + m_OutPerceptron.Output); //line used to debug the results of the test file. Uncomment to see the results of the output layer
                    }

                    for (int j = 0; j < m_OutLayerOutputs.Count; j += m_QtyNeuronsOutputLayer)
                    {
                        Console.WriteLine("Result: " + ConvertToIrisName(m_OutLayerOutputs[j]));
                    }
                }

                Console.WriteLine("\nRun again?");
                m_TryAgain = Console.ReadLine().ToLower();

                //TRAINING FILE READING
                void ReadFile(string link)
                {
                    string fileToReadTraining = "";
                    m_Inputs.Clear();
                    m_ExpectedResults.Clear();
                    m_ConvertedResults.Clear();

                    try
                    {
                        WebClient wc = new WebClient();
                        fileToReadTraining = wc.DownloadString(link);
                    }
                    catch (WebException we)
                    {
                        Console.WriteLine(we);
                    }

                    string[] lines = fileToReadTraining.Split("\n");

                    for (int i = 0; i < lines.Length; ++i)
                    {
                        m_ReadInputs.Clear();
                        string[] tempLine = lines[i].Split(',');

                        double[] tempValues = new double[tempLine.Length - 1];

                        for (int j = 0; j < tempLine.Length - 1; ++j)
                        {
                            m_ReadInputs.Add(double.Parse(tempLine[j], number));
                        }

                        m_ReadInputs.CopyTo(tempValues);
                        m_Inputs.Add(tempValues);

                        m_ExpectedResults.Add(tempLine[tempLine.Length - 1]);
                    }
                    m_ConvertedResults = ConvertExpected(m_ExpectedResults);
                }

                void RandomizeWeights()
                {
                    for (int i = 0; i < m_QtyNeuronsHiddenLayer; ++i)
                    {
                        m_Input_HiddenWeights.Add(new double[] { m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble() });
                    }

                    for (int i = 0; i < m_QtyNeuronsOutputLayer; ++i)
                    {
                        double[] rdnWeightsHidden_Output = new double[m_QtyNeuronsHiddenLayer]; //temp array to choose random values of weights in the hidden -> output layers
                        for (int j = 0; j < rdnWeightsHidden_Output.Length; ++j)
                        {
                            rdnWeightsHidden_Output[j] = m_Random.NextDouble();
                        }
                        m_Hidden_OutputWeights.Add(rdnWeightsHidden_Output);
                    }
                }

                void DebugWeights(List<double[]> hidden, List<double[]> output, bool isRecalculating)
                {
                    for (int i = 0; i < hidden.Count; ++i)
                    {
                        for (int j = 0; j < hidden[i].Length; ++j)
                        {
                            Console.WriteLine(isRecalculating ? "New Hidden weight: " + hidden[i][j] : "Hidden weight: " + hidden[i][j]);
                        }
                    }

                    Console.WriteLine();

                    for (int i = 0; i < output.Count; ++i)
                    {
                        for (int j = 0; j < output[i].Length; ++j)
                        {
                            Console.WriteLine(isRecalculating ? "New Output weight: " + output[i][j] : "Output weight: " + output[i][j]);
                        }
                    }
                }

                void Train()
                {
                    //while (currentGen <= m_NumOfGens)
                    while (true)
                    {
                        int mseCounter = 0;

                        for (int i = 0; i < m_Inputs.Count; ++i)
                        {
                            //----------FEEDFORWARD----------
                            m_HidLayerOutputs.Clear();
                            m_OutLayerOutputs.Clear();
                            for (int j = 0; j < m_QtyNeuronsHiddenLayer; ++j)
                            {
                                m_HiddenPerceptron.SetPerceptron(m_Inputs[i], m_Input_HiddenWeights[j], m_Bias);
                                m_HidLayerOutputs.Add(m_HiddenPerceptron.Output);
                            }

                            for (int j = 0; j < m_QtyNeuronsOutputLayer; ++j)
                            {
                                m_OutPerceptron.SetPerceptron(m_HidLayerOutputs, m_Hidden_OutputWeights[j], m_Bias);
                                m_OutLayerOutputs.Add(m_OutPerceptron.Output);
                            }

                            //----------BACKPROPAGATION----------
                            //------Operation Funcs------
                            Func<double, double, double> DeltaError = (y, f) => y - f; //delta used in the formula of error of the output layer
                            Func<double, double> SigmoidDerivative = x => x * (1.0 - x);
                            Func<double, double, double, double, double> UpdateWeight = (weight, learningRate, input, error) => weight + (learningRate * input * error);

                            //UPDATING WEIGHTS OF THE OUTPUT TO HIDDEN LAYER
                            List<double> deltaErrors = new List<double>();
                            List<double> derivativesOut = new List<double>(); //derivatives of the output layer neurons
                            for (int k = 0; k < m_OutLayerOutputs.Count; ++k)
                            {
                                deltaErrors.Add(DeltaError(m_ConvertedResults[i], m_OutLayerOutputs[k]));
                                derivativesOut.Add(SigmoidDerivative(m_OutLayerOutputs[k]));
                            }

                            if (MSE(deltaErrors) < m_BreakCondition)
                                ++mseCounter;

                            double error = 0.0; //error that will be used in the update weight formula

                            double sumOfOutLayerErrors = 0.0; //sum of errors of the output layer to use in the step of updating weights of hidden to input layer
                            for (int k = 0; k < derivativesOut.Count; ++k) //k is for each neuron in output layer
                            {
                                error = derivativesOut[k] * deltaErrors[k];
                                sumOfOutLayerErrors += error;

                                for (int n = 0; n < m_Hidden_OutputWeights[k].Length; ++n) //n is for each weight in array of weights in the k index of the output weights list
                                {
                                    m_Hidden_OutputWeights[k][n] = UpdateWeight(m_Hidden_OutputWeights[k][n], m_LearningRate, m_HidLayerOutputs[n], error); //updating weight
                                }
                            }

                            //UPDATING WEIGHTS OF THE HIDDEN TO INPUT LAYER
                            List<double> derivativesHid = new List<double>(); //derivatives of the hidden layer neurons
                            for (int k = 0; k < m_HidLayerOutputs.Count; ++k)
                            {
                                derivativesHid.Add(SigmoidDerivative(m_HidLayerOutputs[k]));
                            }

                            for (int k = 0; k < derivativesHid.Count; ++k) //k is for each neuron in hidden layer
                            {
                                error = derivativesHid[k] * sumOfOutLayerErrors;
                                for (int n = 0; n < m_Input_HiddenWeights[k].Length; ++n) //n is for each weight in array of weights in the k index of the hidden weights list
                                {
                                    m_Input_HiddenWeights[k][n] = UpdateWeight(m_Input_HiddenWeights[k][n], m_LearningRate, m_Inputs[i][n], error); //updating weight
                                }
                            }
                        }

                        ++currentGen; //used for second break condition calculation

                        if (mseCounter >= m_Inputs.Count * m_Percentage)
                        {
                            break;
                        }
                        else if (currentGen >= m_NumOfGens)
                        {
                            Console.WriteLine("\nTraining got wrong. Generating new weights...");
                            currentGen = 0;

                            m_Input_HiddenWeights.Clear();
                            m_Hidden_OutputWeights.Clear();

                            RandomizeWeights();

                            //-------------DEBUGGING-------------
                            DebugWeights(m_Input_HiddenWeights, m_Hidden_OutputWeights, true);

                            Train();

                            break;
                        }
                    }
                }
            }
            
            Console.ReadKey();
        }

        static List<double> ConvertExpected(List<string> expected)
        {
            List<double> converted = new List<double>();

            for (int i = 0; i < expected.Count; ++i)
            {
                switch (expected[i])
                {
                    case "Iris-setosa":
                        converted.Add(0.0);
                        break;
                    case "Iris-versicolor":
                        converted.Add(0.5);
                        break;
                    case "Iris-virginica":
                        converted.Add(1.0);
                        break;
                }
            }

            return converted;
        }

        static double MSE(List<double> errors)
        {
            double mse = 0.0;

            for (int i = 0; i < errors.Count; ++i)
            {
                mse += errors[i] * errors[i];
            }

            return mse;
        }

        static string ConvertToIrisName(double result)
        {
            string converted = "";

            switch (result)
            {
                case 0.0:
                    converted = "Iris-setosa";
                    break;
                case 0.5:
                    converted = "Iris-versicolor";
                    break;
                case 1.0:
                    converted = "Iris-virginica";
                    break;
            }

            return converted;
        }
    }

    class Perceptron
    {
        double[] m_Inputs;
        double[] m_Weights;
        double m_Bias;

        double m_ResultSumForActivation;

        double m_Output;
        public double Output { get => m_Output; }

        public Perceptron() { }

        public void SetPerceptron(double[] inputs, double[] weights, double bias)
        {
            m_Inputs = inputs;
            m_Weights = weights;
            m_Bias = bias;

            CalcNeuron();
        }

        public void SetPerceptron(List<double> inputs, double[] weights, double bias)
        {
            m_Inputs = new double[inputs.Count];
            inputs.CopyTo(m_Inputs);
            m_Weights = weights;
            m_Bias = bias;

            CalcNeuron();
        }

        void CalcNeuron()
        {
            m_ResultSumForActivation = SumInputs(m_Inputs, m_Weights);
            m_Output = SigmoidFunction(m_ResultSumForActivation);
        }

        double SumInputs(double[] inputs, double[] weights)
        {
            double sum = 0.0;

            for (int i = 0; i < inputs.Length; ++i)
            {
                sum += inputs[i] * weights[i];
            }

            sum += m_Bias;

            return sum;
        }

        public double Result(double output)
        {
            if (output >= 0.0 && output < 0.33)
            {
                return 0.0; //iris-setosa
            }
            if (output >= 0.33 && output < 0.67)
            {
                return 0.5; //iris-versicolor
            }
            if (output >= 0.67)
            {
                return 1.0; //iris-virginica
            }

            return -1.0;
        }

        Func<double, double> SigmoidFunction = x => 1 / (1 + Math.Exp(-x));
    }
}