﻿using UnityEngine;
using UnityEngine.UI;
public class LocalizationManager : MonoBehaviour
{
    [SerializeField] private Dropdown m_Languages;
    [SerializeField] private LanguageDict[] m_LanguagesSOs;
    [SerializeField] private Text[] m_MenuTexts;

    void Awake()
    {
        ChangeTextLabelsMenu(m_LanguagesSOs[PlayerPrefs.GetInt("Current Language")]);
        m_Languages.value = PlayerPrefs.GetInt("Current Language");
    }

    void OnEnable()
    {
        m_Languages.onValueChanged.AddListener(delegate
        { 
            DropdownValueChanged(m_Languages);
        });  
    }

    void DropdownValueChanged(Dropdown change)
    {
        PlayerPrefs.SetInt("Current Language", change.value);
        ChangeTextLabelsMenu(m_LanguagesSOs[change.value]);
    }

    void ChangeTextLabelsMenu(LanguageDict language)
    {
        m_MenuTexts[0].text = language.play_button;
        m_MenuTexts[1].text = language.options_button;
        m_MenuTexts[2].text = language.credits_button;
        m_MenuTexts[3].text = language.quit_button;
        m_MenuTexts[4].text = language.language_dropdown_label;
    }
}