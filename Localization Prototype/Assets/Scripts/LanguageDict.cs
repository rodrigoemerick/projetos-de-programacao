﻿using UnityEngine;

[CreateAssetMenu]
public class LanguageDict : ScriptableObject
{
    public string play_button;
    public string options_button;
    public string credits_button;
    public string quit_button;
    public string english_language;
    public string portuguese_language;
    public string spanish_language;
    public string language_dropdown_label;
}