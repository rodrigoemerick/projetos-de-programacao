﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toucan : MonoBehaviour
{
    Animator animator;

    private float toucanHealth;
    [SerializeField]
    private float initialHealth;

    SpriteRenderer m_SpriteRenderer;

    public Transform[] m_InterpolationPoints;
    public int m_CurrentPoint;

    public float m_Speed = 2.0f;
    public float m_MinDistance = 0.1f;

    private Vector3 m_StartPoint;
    private Vector3 m_EndPoint;

    private float m_StartTime;
    private float m_DistanceTotal;
    private float m_DistanceCovered;
    private float m_DistancePercent;

    public Vector3 ScaleDirection;
    public float FlipValue;

    private Vector3 m_InitialPosition;

    public bool TookDamage;
    public float TookDamageTime;

    void Start()
    {
        m_InitialPosition = transform.position;
        ScaleDirection = transform.localScale;
        FlipValue = ScaleDirection.x;
        animator = GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        toucanHealth = 30.0f;
        Reset();
    }

    private void Reset()
    {
        transform.position = m_InitialPosition;
        m_CurrentPoint = -1;
        ScaleDirection.x = FlipValue;
        transform.localScale = ScaleDirection;
        NextPoint();
    }

    void Update()
    {
        if (m_InterpolationPoints.Length > 0)
        {
            m_DistanceCovered = (Time.time - m_StartTime) * m_Speed;
            m_DistancePercent = m_DistanceCovered / m_DistanceTotal;
			//Debug.Log (m_DistanceTotal + "," + m_DistancePercent);

            transform.position = Vector3.Lerp(m_StartPoint, m_EndPoint, m_DistancePercent);
            if (Vector3.Distance(transform.position, m_EndPoint) < m_MinDistance)
            {
                ScaleDirection.x = FlipValue;
                NextPoint();
            }

        }

        

        if (TookDamage){
            Damage();
        }
    }

    private void Damage(){
        //Debug.Log("Tomou dano");
        
        Physics2D.IgnoreLayerCollision(8, 9);
        m_SpriteRenderer.color = Color.red;
		TookDamage = false;
		StartCoroutine(AfterDamage());
    }

    private IEnumerator AfterDamage(){
        yield return new WaitForSeconds(TookDamageTime);
        Physics2D.IgnoreLayerCollision(8, 9, false);
        m_SpriteRenderer.color = Color.white;
		if (toucanHealth <= 0)
		{
			Destroy(gameObject);
		}
    }

    private void NextPoint()
    {
        if (m_InterpolationPoints.Length > 0)
        {
            ++m_CurrentPoint;
            ScaleDirection.x = FlipValue;
            if (m_CurrentPoint == m_InterpolationPoints.Length)
            {
                ScaleDirection.x = -FlipValue;
                m_CurrentPoint = 0;
            }
            transform.localScale = ScaleDirection;

            m_StartPoint = transform.position;
            m_EndPoint = m_InterpolationPoints[m_CurrentPoint].position;

            m_StartTime = Time.time;
            m_DistanceTotal = Vector3.Distance(m_StartPoint, m_EndPoint);

        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
		//Debug.Log ("Tucano" + other.gameObject.tag);
        if (other.gameObject.tag == "Flecha")
        {
			Arrow arrowDamage = other.gameObject.GetComponent<Arrow>();
            toucanHealth -= arrowDamage.DamageValue;
            TookDamage = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.tag == "Player"){
            animator.SetBool("Attacking", true);
        }
    }

    void OnTriggerExit2D(Collider2D other){
        if(other.gameObject.tag == "Player"){
            animator.SetBool("Attacking", false);
        }
    }
}