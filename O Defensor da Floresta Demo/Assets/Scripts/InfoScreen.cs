﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class InfoScreen : MonoBehaviour {

    Canvas canvas;

	void Start () {
        canvas = GetComponent<Canvas>();
        canvas.enabled = true;
	}
	
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)){
            InfoScreenControl();
        }	
	}

    private void InfoScreenControl(){
        canvas.enabled = !canvas.enabled;
        //Time.timeScale = Time.timeScale == 0 ? 1 : 0;
    }
}
