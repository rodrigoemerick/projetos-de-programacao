﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruits : MonoBehaviour {

    public bool FruitCollected;

    public float ajuruValue = 5.0f;
    public float aracaValue = 10.0f;
    public float acaiValue = 20.0f;

    public void DestroyFruit(){
        if (FruitCollected){
            Destroy(gameObject);
        }
        else{
            gameObject.SetActive(false);
        }
    }
}
