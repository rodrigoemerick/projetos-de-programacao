﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    [SerializeField]
    private float ArrowSpeed;

	public float DamageValue;

    [SerializeField]
    private float DestroyTime;

    private Vector3 TargetMousePosition;
    private Vector3 InitialPosition;

    private float Timer;
	
	void Start () {
        Physics2D.IgnoreLayerCollision(8, 10, true);
        Destroy(gameObject, DestroyTime);
        InitialPosition = transform.position;

        //TargetMousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
		TargetMousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		TargetMousePosition.z = 0.0f;

        Timer = 0.0f;
	}
	
	void Update () {
        Timer += Time.deltaTime;
        transform.position = Vector3.Lerp(InitialPosition, TargetMousePosition, Timer / ArrowSpeed);
		//transform.LookAt (TargetMousePosition);
		if (transform.position == TargetMousePosition) {
			Destroy (gameObject);
		}
    }

    void OnCollisionEnter2D(Collision2D other){
        if (other.gameObject.tag == "Toucan" || other.gameObject.tag == "Sloth"){
            Destroy(gameObject);
        }
    }
}
