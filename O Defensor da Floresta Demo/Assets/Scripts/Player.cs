﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    
    Rigidbody2D m_RigidBody2D;
    public Vector2 Speed;

    Animator animator;

    SpriteRenderer m_SpriteRenderer;

    [SerializeField]
    private HealthStatus playerHealth;
    [SerializeField]
    private float initialHealth;

    public float HorizontalSpeed;
    public float JumpValue;
    public float FallValue;
    public bool OnGround;
	public bool IsRunning;
    public bool TookDamage;

    private float ToucanDamage = 10.0f;
    private float SlothDamage = 35.0f;

    public Vector3 StartPosition;
    public Vector3 ResetGamePosition;
    //public Vector3 DamageTakenPosition;

    public Vector3 ScaleDirection;
    public float FlipValue;

    public float TookDamageTime;

    public GameObject ArrowPrefab;
    public Transform ArrowSpawner;

    void Awake(){
        m_RigidBody2D = GetComponent<Rigidbody2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        StartPosition = transform.position;
        ResetGamePosition = transform.position;
        //DamageTakenPosition = transform.position;
        ScaleDirection = transform.localScale;
        FlipValue = ScaleDirection.x;
        playerHealth.Init(initialHealth, initialHealth);

        ResetGame();
    }


    private void ResetGame () {
        Speed = Vector2.zero;
        FallValue = 0.0f;
        OnGround = false;
        ScaleDirection.x = FlipValue;
        transform.localScale = ScaleDirection;
        transform.position = StartPosition;
        playerHealth.CurrentHealthStatus = 100;
	}
	
	void Update () {

        if (Input.GetButtonDown("Pulo") && OnGround){
            Speed.y = JumpValue;
            FallValue = 0.0f;
            animator.SetBool("Pulou", true);
        }

        if (!OnGround){
            if (Speed.y > -JumpValue){
                FallValue += Physics2D.gravity.y * 0.2f * Time.deltaTime; //multiplicar por 0.2f aumenta o tempo de duração no ar
                Speed.y += FallValue;
            }
        }


        //--------------BOTOES CRIADOS PARA TESTE-------------------
        //if (Input.GetKeyDown(KeyCode.F)){
        //    playerHealth.CurrentHealthStatus -= 10;
        //}

        //if (Input.GetKeyDown(KeyCode.G)){
        //    playerHealth.CurrentHealthStatus += 10;
        //}

        if(playerHealth.CurrentHealthStatus <= 0){
            ResetGame();
        }

        Speed.x = HorizontalSpeed * Input.GetAxis("Horizontal");
		if (Speed.x > 0.0f) {
			ScaleDirection.x = FlipValue;
			animator.SetFloat ("Andar", 0.1f);
		} else if (Speed.x < 0.0f) {
			ScaleDirection.x = -FlipValue;
			animator.SetFloat ("Andar", 0.1f);
		}
        transform.localScale = ScaleDirection;

		if (Speed.x == 0.0f) {
			animator.SetFloat ("Andar", -0.1f);
		}

		if (Input.GetButtonDown ("Correr")) {
			IsRunning = true;
			HorizontalSpeed = 10.0f;
		}

		if (Input.GetButtonUp ("Correr") && IsRunning) {
			IsRunning = false;
			HorizontalSpeed = 5.0f;
		}

        if(Input.GetAxis("Vertical") < 0.0f){
            animator.SetFloat("Agachar", -0.1f);
			HorizontalSpeed = 0.0f;
        }else if(Input.GetAxis("Vertical") > 0.0f){
            animator.SetFloat("Agachar", 0.1f);
			HorizontalSpeed = 5.0f;
        }

        if (Input.GetMouseButtonDown(0)){
            animator.SetBool("Mirar", true);
            HorizontalSpeed = 0.0f;
        }else if (Input.GetMouseButtonUp(0)){
            animator.SetBool("Mirar", false);
            HorizontalSpeed = 5.0f;
			Instantiate(ArrowPrefab, ArrowSpawner.position, ArrowSpawner.rotation);

        }

        if (TookDamage){
            Damage();
        }
    }

    void FixedUpdate(){
        OnGround = false;
        m_RigidBody2D.velocity = Speed;
    }

    private void Damage(){
        
        Physics2D.IgnoreLayerCollision(8, 9);
        m_SpriteRenderer.color = Color.red;
        TookDamage = false;
		StartCoroutine(AfterDamage());
    }

    private IEnumerator AfterDamage(){
        yield return new WaitForSeconds(TookDamageTime);
        Physics2D.IgnoreLayerCollision(8, 9, false);
        m_SpriteRenderer.color = Color.white;
    }

    void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.tag == "DeathFall"){
            ResetGame();
        }
        if(other.gameObject.tag == "Checkpoint"){
            StartPosition = other.gameObject.transform.position;
        }
        if (other.gameObject.tag == "Finish")
        {
            SceneManager.LoadScene("Game Demo");
        }
    }

    void OnTriggerStay2D(Collider2D other){
        if(other.gameObject.tag == "Plataforma"){
            OnGround = true;
            FallValue = 0.0f;
            Speed.y = 0.0f;
            animator.SetBool("Pulou", false);
            //Debug.Log(OnGround);
        }

        if(other.gameObject.tag == "Parede"){
            OnGround = false;
        }

        if (other.gameObject.tag == "Ajuru" && gameObject.activeSelf && Input.GetKeyDown(KeyCode.C)){
            Fruits ajuru = other.gameObject.GetComponent<Fruits>();
            if (ajuru != null)
            {
                playerHealth.CurrentHealthStatus += ajuru.ajuruValue;
                ajuru.DestroyFruit();
            }
        }

        if (other.gameObject.tag == "Araçá" && gameObject.activeSelf && Input.GetKeyDown(KeyCode.C))
        {
            Fruits araca = other.gameObject.GetComponent<Fruits>();
            if (araca != null)
            {
                playerHealth.CurrentHealthStatus += araca.aracaValue;
                araca.DestroyFruit();
            }
        }

        if (other.gameObject.tag == "Açaí" && gameObject.activeSelf && Input.GetKeyDown(KeyCode.C))
        {
            Fruits acai = other.gameObject.GetComponent<Fruits>();
            if (acai != null)
            {
                playerHealth.CurrentHealthStatus += acai.acaiValue;
                acai.DestroyFruit();
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.tag == "Toucan"){
            playerHealth.CurrentHealthStatus -= ToucanDamage;
            TookDamage = true;
        }

        if (other.gameObject.tag == "Sloth"){
            playerHealth.CurrentHealthStatus -= SlothDamage;
            TookDamage = true;
        }
    }
}