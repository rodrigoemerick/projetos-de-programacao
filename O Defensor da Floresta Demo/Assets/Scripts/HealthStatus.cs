﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthStatus : MonoBehaviour {

    private Image status;

    [SerializeField]
    private Text statusValue;

    private float StatusFill;
    public float MaxHealthValue { get; set; }

    public float CurrentHealthStatus
    {
        get { return currentStatus; }
        set { currentStatus = value;
            if (value > MaxHealthValue){
                currentStatus = MaxHealthValue;
            }else if (value < 0){
                currentStatus = 0;
            }
            else{
                currentStatus = value;
            }
            StatusFill = currentStatus / MaxHealthValue;
            statusValue.text = currentStatus + "/" + MaxHealthValue;
        }
    }

    private float currentStatus;

    void Start () {
        MaxHealthValue = 100;
        status = GetComponent<Image>();
	}
	
	
	void Update () {
        status.fillAmount = StatusFill;
	}

    public void Init(float currentStatus, float maxValue){
        MaxHealthValue = maxValue;
        CurrentHealthStatus = currentStatus;
    }
}