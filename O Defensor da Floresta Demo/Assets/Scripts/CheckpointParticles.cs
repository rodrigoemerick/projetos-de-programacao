﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointParticles : MonoBehaviour {

    ParticleSystem m_ParticleSystem;

    SpriteRenderer m_SpriteRenderer;

    private float ActivateSystem = 2.5f;

	void Start () {
        m_ParticleSystem = GetComponent<ParticleSystem>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	
	void OnTriggerEnter2D (Collider2D other) {
		if(other.gameObject.tag == "Player"){
            m_ParticleSystem.Play();
            m_SpriteRenderer.color = Color.green;
            StartCoroutine(StopSystem());
        }
	}

    private IEnumerator StopSystem(){
        yield return new WaitForSeconds(ActivateSystem);
        m_SpriteRenderer.color = Color.white;
    }
}
