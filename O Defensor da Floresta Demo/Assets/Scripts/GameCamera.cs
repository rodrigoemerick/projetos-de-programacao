﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour {

    public Transform m_Player;
    public Vector3 m_Position;
    public float m_DistanceCameraX;
    public float m_DistanceCameraY;

	void Start () {
        m_DistanceCameraX = m_Player.transform.position.x - transform.position.x;
        m_DistanceCameraY = m_Player.transform.position.y - transform.position.y;
        m_Position = transform.position;
	}
	
	void FixedUpdate () {
        m_Position.x = m_Player.transform.position.x - m_DistanceCameraX;
        m_Position.y = m_Player.transform.position.y - m_DistanceCameraY;
        transform.position = m_Position;
	}
}
