﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    [SerializeField]
    HingeJoint2D m_HingeJoint2D;
    [SerializeField]
    private float m_RestartSceneTime;

    DistanceJoint2D m_DistanceJoint2D;

    private TargetJoint2D m_TargetJoint2D;

    private Vector3 m_NewPosition;
    private Vector3 m_OldPosition;

    //private Vector3 m_Position;
    private Vector3 m_ScreenStart;
    private Vector3 m_ScreenSize;

	void Start ()
    {
        m_TargetJoint2D = GetComponent<TargetJoint2D>();
        m_TargetJoint2D.enabled = false;

        m_DistanceJoint2D = GetComponent<DistanceJoint2D>();

        //m_NewPosition = Vector3.zero;

        m_ScreenStart = Camera.main.ScreenToWorldPoint(Vector3.zero);
        m_ScreenSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
    }

    void Update ()
    {
        m_NewPosition = transform.position;
        if(m_NewPosition.y < m_ScreenStart.y)
          {
              SceneManager.LoadScene("Physics Puzzle");
          }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Goal")
        {
            Destroy(gameObject);
        }        
    }

    void OnMouseDown()
    {
        m_TargetJoint2D.enabled = true;    
    }

    void OnMouseDrag()
    {
        m_NewPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_NewPosition.z = 0.0f;

        m_TargetJoint2D.target = m_NewPosition;
    }

    void OnMouseUp()
    {
        m_TargetJoint2D.enabled = false;
        m_DistanceJoint2D.enabled = false;
        m_HingeJoint2D.connectedBody = null;
    }
}
