﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    ParticleSystem m_ParticleSystem;

    [SerializeField]
    private Transform[] m_Points;
    public int m_CurrentPoint;

    public float m_Speed = 3.0f;

    public float m_MinDistance = 0.1f;

    private Vector3 m_StartPoint;
    private Vector3 m_EndPoint;

    private float m_StartTime;
    private float m_DistanceTotal;
    private float m_DistanceCovered;
    private float m_DistancePercent;

	[SerializeField]
	private float ToReloadTime;

    private Vector3 m_InitialPosition;

	void Start () {
        m_InitialPosition = transform.position;
        m_ParticleSystem = GetComponent<ParticleSystem>();
        Reset();
	}

    private void Reset()
    {
        transform.position = m_InitialPosition;
        m_CurrentPoint = -1;
        NextPoint();
    }

    void Update () {
        if (m_Points.Length > 0){
            m_DistanceCovered = (Time.time - m_StartTime) * m_Speed;
            m_DistancePercent = m_DistanceCovered / m_DistanceTotal;

            transform.position = Vector3.Lerp(m_StartPoint, m_EndPoint, m_DistancePercent);
            if(Vector3.Distance (transform.position, m_EndPoint) < m_MinDistance){
                NextPoint();
            }
        }
	}

    private void NextPoint(){
        if(m_Points.Length > 0){
            m_CurrentPoint++;
            if(m_CurrentPoint == m_Points.Length){
                m_CurrentPoint = 0;
            }

            m_StartPoint = transform.position;
            m_EndPoint = m_Points[m_CurrentPoint].position;

            m_StartTime = Time.time;
            m_DistanceTotal = Vector3.Distance(m_StartPoint, m_EndPoint);
        }
    }

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Ball") 
		{
			m_ParticleSystem.Play ();
			StartCoroutine (ToReload ());
		}
	}

	private IEnumerator ToReload()
	{
		yield return new WaitForSeconds (ToReloadTime);
		SceneManager.LoadScene ("Physics Puzzle");
	}
		
}
