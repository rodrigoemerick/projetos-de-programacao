﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chains : MonoBehaviour
{

    private Rigidbody2D m_RigidBody2D;
    //public DistanceJoint2D m_DistanceJoint2D;

    [SerializeField]
    private HingeJoint2D m_HingeJoint2D;

    private TargetJoint2D m_TargetJoint2D;

    private Vector3 m_NewPosition;
    private Vector3 m_OldPosition;

    void Start()
    {
        m_RigidBody2D = GetComponent<Rigidbody2D>();
        //m_DistanceJoint2D = GetComponent<DistanceJoint2D>();

        m_NewPosition = Vector3.zero;
    }
}