﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    [SerializeField] private Strings m_SSO;
    [SerializeField] private Levels m_LSO;
    [SerializeField] private TransitionFade m_Transition;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            m_SSO.isVictory = false;
            m_LSO.touchedSquare = true;
            m_Transition.StartTransition("Game");
        }
        if (other.gameObject.CompareTag("Victory") && !m_LSO.touchedSquare)
        {
            m_SSO.isVictory = true;

            if (m_LSO.maxLevel == m_LSO.currentLevel)
            {
                if (m_LSO.maxLevel < m_LSO.levels.Length - 1)
                {
                    ++m_LSO.maxLevel;
                    PlayerPrefs.SetInt("MaxLevel", m_LSO.maxLevel);
                    m_LSO.currentLevel = m_LSO.maxLevel;
                    m_Transition.StartTransition("Game");
                }
                else
                {
                    m_LSO.currentLevel = m_LSO.maxLevel;
                    m_Transition.StartTransition("Final");
                }
            }
            else
            {
                ++m_LSO.currentLevel;
                m_Transition.StartTransition("Game");
            }
        }
    }
}