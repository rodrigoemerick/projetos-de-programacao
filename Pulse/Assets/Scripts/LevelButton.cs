﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour
{
    [SerializeField] private Levels m_LSO;
    [SerializeField] private Text m_Label;
    [SerializeField] private GameObject m_Selected1, m_Selected2;
    [SerializeField] private TransitionFade m_Transition;
    [SerializeField] private Animator m_Animator;
    [SerializeField] private float m_AnimationTime;

    public int m_Index;

    void Update()
    {
        m_Index = m_LSO.selectedLevel;
        m_Label.text = (m_Index + 1).ToString();    
    }

    public void CallCoroutine()
    {
        StartCoroutine(Separate());
    }

    IEnumerator Separate()
    {
        m_Animator.SetTrigger("Separate");
        m_Label.gameObject.SetActive(false);
        m_Selected1.SetActive(false);
        m_Selected2.SetActive(false);
        yield return new WaitForSecondsRealtime(m_AnimationTime);
        GoToScene();
    }

    void GoToScene()
    {
        m_LSO.currentLevel = m_Index;
        m_Transition.StartTransition("Game");
    }
}