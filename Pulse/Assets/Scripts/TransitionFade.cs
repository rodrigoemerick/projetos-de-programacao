﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionFade : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;
    [SerializeField]
    private float m_TransitionDuration;

    public void StartTransition(string sceneName)
    {
        StartCoroutine(Transition(sceneName, m_TransitionDuration));
    }

    IEnumerator Transition(string sceneName, float transitionDuration)
    {
        m_Animator.SetTrigger("SwitchScene");
        yield return new WaitForSecondsRealtime(transitionDuration);
        LoadNewScene(sceneName);
    }

    void LoadNewScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}