﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudios : MonoBehaviour
{
    [SerializeField] private AudioSource m_Select, m_Next, m_Back;

    public void PlaySelect()
    {
        //if (!m_Select.isPlaying)
        //{
            m_Select.Play();
        //}
    }

    public void PlayNext()
    {
        //if (!m_Next.isPlaying)
        //{
            m_Next.Play();
        //}
    }

    public void PlayBack()
    {
        //if (!m_Back.isPlaying)
        //{
            m_Back.Play();
        //}
    }
}
