﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalScene : MonoBehaviour
{
    [SerializeField] private float m_GoToMenuAfter, m_AppearPulse;
    [SerializeField] private GameObject m_Pulse;
    [SerializeField] private TransitionFade m_Transition;

    void Start()
    {
        StartCoroutine(GoToMenu());
    }

    IEnumerator GoToMenu()
    {
        yield return new WaitForSecondsRealtime(m_AppearPulse);
        m_Pulse.SetActive(true);
        yield return new WaitForSecondsRealtime(m_GoToMenuAfter);
        m_Transition.StartTransition("Menu");
    }
}