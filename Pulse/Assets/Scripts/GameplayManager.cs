﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] private Levels m_LSO;

    void Start()
    {
        Instantiate(m_LSO.levels[m_LSO.currentLevel], Vector3.zero, Quaternion.identity);
    }
}