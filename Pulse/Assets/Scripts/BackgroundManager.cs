﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    [SerializeField] private Levels m_LSO;
    //[SerializeField] private SpriteRenderer m_SpriteRenderer;

    void Awake()
    {
        //m_SpriteRenderer.sprite = m_LSO.backgrounds[m_LSO.currentLevel];
    }

    void Start()
    {
        RenderSettings.skybox = m_LSO.skyboxMaterials[m_LSO.currentLevel];
    }
}