﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class BallsManager : MonoBehaviour
{
    [SerializeField] private Transform m_BodyBall;
    [SerializeField] private Transform m_MindBall;
    [SerializeField] private Transform m_Selected;
    [SerializeField] private BallStateManager m_BSMSO;
    [SerializeField] private Levels m_LSO;
    [SerializeField] private float m_RotationAngle;
    [SerializeField] private float m_SmoothSelected;
    [SerializeField] private AudioSource m_Ball1, m_Ball2;

    private int m_Index; //Controla se o player já leu o tutorial
    Rewired.Player m_RePlayer;

    void Awake()
    {
        m_RePlayer = ReInput.players.GetPlayer("Player");
        m_LSO.touchedSquare = false;

        if (m_LSO.currentLevel == 0)
        {
            m_LSO.reginaldo = false;
        }
        else
        {
            m_LSO.reginaldo = true;
        }
    }

    void OnEnable()
    {
        m_BSMSO.SCEvent += OnSwitchCamera;
    }

    void OnDisable()
    {
        m_BSMSO.SCEvent -= OnSwitchCamera;
    }

    void Start()
    {
        m_BSMSO.m_IsMind = true;
    }

    void Update()
    {
        if (m_RePlayer.GetButtonDown("Change Ball"))
        {
            if (m_LSO.currentLevel == 0 && !m_LSO.reginaldo)
            {
                m_LSO.reginaldo = true;
            }
            
            m_BSMSO.SCEvent?.Invoke();

            if (m_BSMSO.m_IsMind)
            {
                //if (!m_Ball1.isPlaying)
                //{
                    m_Ball1.Play();
                //}
            }
            else
            {
                //if (!m_Ball2.isPlaying)
                //{
                    m_Ball2.Play();
                //}
            }
        }

        if (m_LSO.reginaldo)
        {
            if (m_BSMSO.m_IsMind)
            {
                m_BodyBall.RotateAround(m_MindBall.transform.position, Vector3.forward, m_RotationAngle * Time.deltaTime);
                m_Selected.position = Vector3.Lerp(m_Selected.position, new Vector3(m_MindBall.transform.position.x, m_MindBall.transform.position.y, m_Selected.transform.position.z), m_SmoothSelected * Time.deltaTime);
            }
            else
            {
                m_MindBall.RotateAround(m_BodyBall.transform.position, Vector3.forward, -m_RotationAngle * Time.deltaTime);
                m_Selected.position = Vector3.Lerp(m_Selected.position, new Vector3(m_BodyBall.transform.position.x, m_BodyBall.transform.position.y, m_Selected.transform.position.z), m_SmoothSelected * Time.deltaTime);
            }
        }
    }

    public void OnSwitchCamera()
    {
        m_BSMSO.m_IsMind = !m_BSMSO.m_IsMind;
    }
}