﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class Levels : ScriptableObject
{
    public GameObject[] levels;
    public Sprite[] backgrounds;
    public Material[] skyboxMaterials;
    public int currentLevel;
    public int maxLevel;
    public int selectedLevel;
    public bool reginaldo; //controla se o jogador já viu o tutorial
    public bool touchedSquare;
}