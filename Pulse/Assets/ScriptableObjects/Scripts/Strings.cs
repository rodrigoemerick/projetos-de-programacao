﻿using UnityEngine;

[CreateAssetMenu]
public class Strings : ScriptableObject
{
    public bool isVictory;
    public string popupTitle
    {
        get
        {
            if (isVictory)
            {
                return "Congratulations!";
            }
            else
            {
                return "Game Over";
            }
        }
    }
    public string playLevelText { get
        {
            if (isVictory)
            {
                return "Next Level";
            }
            else
            {
                return "Try Again";
            }
        } 
    }
}