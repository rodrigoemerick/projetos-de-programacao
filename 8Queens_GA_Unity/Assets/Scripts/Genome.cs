﻿using System;
using System.Collections.Generic;

namespace _31857086_GA_Operations
{
    class Genome<T>
    {
        private int[] m_Genes;
        public int[] Genes
        {
            get => m_Genes;
            set => m_Genes = value;
        }

        private double m_Fitness;
        public double Fitness
        {
            get => m_Fitness;
            set => m_Fitness = value;
        }

        private Random m_Random;
        List<int> posNumbers;

        public Genome(int chromosomeLength)
        {
            m_Random = new Random((int)DateTime.UtcNow.Ticks);
            posNumbers = new List<int>(new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            m_Genes = new int[chromosomeLength];

            
            for (int i = 0; i < chromosomeLength; ++i)
            {
                int pos = m_Random.Next(0, posNumbers.Count);
                m_Genes[i] = posNumbers[pos];
                posNumbers.RemoveAt(pos);
            }
            m_Fitness = 0.0f;
        }
    }
}