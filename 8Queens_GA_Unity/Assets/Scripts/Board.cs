﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _31857086_GA_Operations
{
    public class Board : MonoBehaviour
    {
        private int[,] m_Board;

        [SerializeField]
        private GameObject[] m_Prefabs;
        [SerializeField]
        private float m_CamOffsetX;
        [SerializeField]
        private float m_CamOffsetY;
        [SerializeField]
        private int m_PopulationSize;
        [SerializeField]
        private int m_ChromosomeLength;
        [SerializeField]
        private float m_CrossoverRate;
        [SerializeField]
        private double m_MutationRate;
        [SerializeField]
        private float m_FitDiscount;
        [SerializeField]
        private Text m_GenerationText;

        private int m_Generation;
        private int m_FittestGenome;
        private double m_BestFitnessScore;
        private double m_TotalFitnessScore;

        private GAOperations<int> m_GA;
        private List<GameObject> m_FittestGenomesDraws;
    
        void Start()
        {
            m_Board = new int[,]
            {
                {0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0},
            };

            DrawBoard();

            m_GA = new GAOperations<int>(m_PopulationSize, m_ChromosomeLength, m_CrossoverRate);
            m_FittestGenomesDraws = new List<GameObject>();
        }

        void Update()
        {
            if (m_GA.IsRunning)
            {
                Epoch();
                m_GenerationText.text = "Generation: " + m_Generation.ToString();
            }
        }

        private void DrawBoard()
        {
            GameObject go;
            for (int i = 0; i < m_Board.GetLength(0); ++i)
            {
                for (int j = 0; j < m_Board.GetLength(1); ++j)
                {
                    if (m_Board[i, j] == 0)
                    {
                        go = Instantiate(m_Prefabs[0], new Vector3(i - m_CamOffsetX, -j + m_CamOffsetY, 1.0f), Quaternion.identity);
                    }
                    if (m_Board[i, j] == 1)
                    {
                        go = Instantiate(m_Prefabs[1], new Vector3(i - m_CamOffsetX, -j + m_CamOffsetY, 1.0f), Quaternion.identity);
                    }
                }
            }
        }

        void CalculateFitnessScores()
        {
            m_FittestGenome = 0;
            m_BestFitnessScore = 0.0;

            for (int i = 0; i < m_PopulationSize; ++i)
            {
                m_GA.Genomes[i].Fitness = TestSolution(m_GA.Genomes[i].Genes);
                m_TotalFitnessScore += m_GA.Genomes[i].Fitness;

                if (m_GA.Genomes[i].Fitness > m_BestFitnessScore)
                {
                    m_BestFitnessScore = m_GA.Genomes[i].Fitness;
                    m_FittestGenome = i;
                }

                if (m_GA.Genomes[i].Fitness >= 1.0)
                {
                    m_GA.IsRunning = false;
                    ShowSolution(m_GA.Genomes[m_FittestGenome].Fitness);
                    Debug.LogWarning("Solução encontrada!");
                    return;
                }
            }
        }

        double TestSolution(int[] positions)
        {
            double fitness = 1.0; //inicializando fitness como 1, será descontado dessa toda vez que houver um ataque, caso não haja ataques, essa será a melhor fitness

            int qAttackHorizontal = 0;
            int qAttackDiagonal = 0;

            //Cálculo do fitness
            for (int i = 0; i < positions.Length - 1; ++i) // percorrendo cada rainha
            {
                int posX = i;
                int posY = positions[i];

                ////HORIZONTAIS
                //horizontal pra direita da minha posição
                for (int x = posX + 1; x < positions.Length; ++x)
                {
                    CheckHorizontal(x, posY);
                }

                //horizontal pra esquerda da minha posição
                for (int x = posX - 1; x >= 0; --x)
                {
                    CheckHorizontal(x, posY);
                }

                //DIAGONAIS
                //diagonal pra cima e direita
                for (int x = i + 1, y = positions[i] - 1; x < positions.Length && y >= 0; ++x, --y)
                {
                    CheckDiagonal(x, y);
                }

                //diagonal pra baixo e direita
                for (int x = i + 1, y = positions[i] + 1; x < positions.Length && y < positions.Length; ++x, ++y)
                {
                    CheckDiagonal(x, y);
                }

                //diagonal pra cima e esquerda
                for (int x = i - 1, y = positions[i] - 1; x >= 0 && y >= 0; --x, --y)
                {
                    CheckDiagonal(x, y);
                }

                //diagonal pra baixo e esquerda
                for (int x = i - 1, y = positions[i] + 1; x >= 0 && y < positions.Length; --x, ++y)
                {
                    CheckDiagonal(x, y);
                }
            }

            void CheckHorizontal(int x, int y)
            {
                if (positions[x] == y) //se o Y de outra rainha é o mesmo dessa...
                {
                    ++qAttackHorizontal;
                }
            }

            void CheckDiagonal(int x, int y)
            {
                if (positions[x] == y)
                {
                    ++qAttackDiagonal;
                }
            }

            int finalNumOfAttacks = qAttackHorizontal + qAttackDiagonal;
            fitness -= m_FitDiscount * finalNumOfAttacks;

            if (fitness < 0.0) //para garantir que a soma total para a roleta de seleção não conte com números negativos
            {
                fitness = 0.0;
            }

            return fitness;
        }

        void Epoch()
        {
            CalculateFitnessScores();

            int populationCurrentSize = 0;

            Genome<int>[] newGenomes = new Genome<int>[m_PopulationSize];

            while (populationCurrentSize < m_PopulationSize)
            {
                Genome<int> parent1 = m_GA.RouletteWheelSelection(m_TotalFitnessScore);
                Genome<int> parent2 = m_GA.RouletteWheelSelection(m_TotalFitnessScore);

                Genome<int> child1 = new Genome<int>(parent1.Genes.Length);
                Genome<int> child2 = new Genome<int>(parent2.Genes.Length);

                m_GA.MultiPointCrossover(parent1.Genes, parent2.Genes, child1.Genes, child2.Genes, 4);

                m_GA.InverseMutation(child1.Genes, m_MutationRate);
                m_GA.InverseMutation(child2.Genes, m_MutationRate);

                newGenomes[populationCurrentSize] = child1;
                newGenomes[populationCurrentSize + 1] = child2;

                populationCurrentSize += 2;
            }

            for (int i = 0; i < m_GA.Genomes.Length; ++i)
            {
                m_GA.Genomes[i] = newGenomes[i];
            }

            ++m_Generation;
        }

        void ShowSolution(double fitness)
        {
            Genome<int> fittestGenome = m_GA.Genomes[m_FittestGenome];
            GameObject go;

            if (fitness >= 1.0)
            {
                for (int i = 0; i < fittestGenome.Genes.Length; ++i)
                {
                    go = Instantiate(m_Prefabs[2], new Vector3(i - m_CamOffsetX, -fittestGenome.Genes[i] + m_CamOffsetY, 0.0f), Quaternion.identity);
                }
            }
        }
    }
}