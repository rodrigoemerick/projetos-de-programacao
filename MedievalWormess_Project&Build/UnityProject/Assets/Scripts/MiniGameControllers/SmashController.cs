﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmashController : MiniGameController
{
    PlayerSmashGame[] m_PlayersInScene;

    [SerializeField] GameObject m_TimerBackGround;
 
    private int m_PlayersInArena;

    private void Start()
    {
        m_PSO.playerWinner = -1;
    }


    void Update()
    {
        m_PlayersInScene = FindObjectsOfType<PlayerSmashGame>();

        if (m_CanStart)
        {
            m_PlayersInArena = FindObjectsOfType<PlayerSmashGame>().Length;

            int counter = m_PlayersInArena;
            for (int i = 0; i < m_PlayersInScene.Length; ++i)
            {
                if (m_PlayersInScene[i].HasFallen)
                {
                    m_PSO.playerIsDead[i] = true;
                    --counter;
                }
                else
                {
                    m_PSO.playerIsDead[i] = false;
                }
            }

            if (counter == 1)
            {
                PlayerSmashGame winner = null;
                for (int i = 0; i < m_PlayersInScene.Length; ++i)
                {
                    if (!m_PlayersInScene[i].HasFallen)
                    {
                        m_PSO.playerIsDead[i] = false;
                        winner = m_PlayersInScene[i];
                    }
                    else
                    {
                        m_PSO.playerIsDead[i] = true;
                    }
                }

                OnMiniGameIsFinish();
            }
        }
    }

    public override void StartMiniGame()
    {
        m_Timer.gameObject.SetActive(false);
        m_TimerBackGround.SetActive(false);
        m_CanStart = true;
    }
}
