﻿using UnityEngine;

public class MiniGameController : MonoBehaviour
{
    [SerializeField] protected PointsSO m_PSO;
    [SerializeField] protected Timer m_Timer;
    [SerializeField] protected float m_GameTime;
    protected bool m_CanStart;

    protected int m_NumOfPlayers;
    public int NumOfPlayers
    {
        set => m_NumOfPlayers = value;
    }

    protected GameObject[] m_Players;
    public GameObject[] Players
    {
        set
        {
            m_Players = new GameObject[m_NumOfPlayers];
            m_Players = value;
        }
    }

    public delegate void GameIsFinishHandler();
    public event GameIsFinishHandler GameIsFinish;
  
    public virtual void StartMiniGame()
    {
        m_Timer.StartTimer(m_GameTime);
        if (!m_CanStart) m_Timer.TimeFinish += OnMiniGameIsFinish;
        m_CanStart = true;
    }

    public void OnGameSetup() 
    {
        StartMiniGame();
    }

    public virtual void OnMiniGameIsFinish()
    {
        GameIsFinish?.Invoke();
    }
}
