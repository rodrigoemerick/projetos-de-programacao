﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapDecoder : MonoBehaviour
{
    [SerializeField] WormsInfoSO m_WISO;
    [SerializeField] GameplayPositionsSO m_GPSO;
    [SerializeField] GeneralInfoSO m_GGISO;

    Vector2 m_TileSize;

    int[] m_ColumLine = {20,20};
    int[][] m_Predefinition;

    Vector2 m_InitialPos;
    Vector2 m_FinalPos;

    public void SetMapInfo()
    {
        string map = m_GGISO.selectedMap;
        switch (map)
        {
            case "forest":
                m_InitialPos[0] = m_GPSO.forestMapTips[0].x;
                m_InitialPos[1] = m_GPSO.forestMapTips[0].y;
                m_FinalPos[0] = m_GPSO.forestMapTips[1].x;
                m_FinalPos[1] = m_GPSO.forestMapTips[1].y;
                break;
            case "ice":
                m_InitialPos[0] = m_GPSO.iceMapTips[0].x;
                m_InitialPos[1] = m_GPSO.iceMapTips[0].y;
                m_FinalPos[0] = m_GPSO.iceMapTips[1].x;
                m_FinalPos[1] = m_GPSO.iceMapTips[1].y;
                break;
            case "fire":
                m_InitialPos[0] = m_GPSO.fireMapTips[0].x;
                m_InitialPos[1] = m_GPSO.fireMapTips[0].y;
                m_FinalPos[0] = m_GPSO.fireMapTips[1].x;
                m_FinalPos[1] = m_GPSO.fireMapTips[1].y;
                break;
        }

        m_Predefinition = m_GPSO.mapPredefinition[Random.Range(0, m_GPSO.mapPredefinition.Count)];

        DeterminateTileSize();

    }

    private void DeterminateTileSize() 
    {
        m_TileSize[0] = (m_FinalPos[0] - m_InitialPos[0]) / m_ColumLine[0];
        m_TileSize[1] = (m_FinalPos[1] - m_InitialPos[1]) / m_ColumLine[1];

        DecodeMap();
    }

    private void DecodeMap()
    {
        m_WISO.womsList.Clear();

        for (int i = 0; i < m_Predefinition.Length; i++)
        {
            for(int j = 0; j < m_Predefinition[i].Length; j++)
            {
                if (m_Predefinition[i][j] == 1)
                {
                    Vector2 temp = DeterminateVertex(j, i);
                    Instantiate(m_WISO.holePrefab, new Vector3(temp.x, 2.92f, temp.y), Quaternion.identity);
                    m_WISO.womsList.Add(Instantiate(m_WISO.wormPrefab, new Vector3(temp.x, -5.0f, temp.y), Quaternion.Euler(0.0f, 180.0f, 0.0f))); 
                }
            }
        }
    }

    private Vector2 DeterminateVertex(int colum, int line)
    {
        Vector2 coord;

        coord.x = m_FinalPos.x-(m_TileSize.x * colum);
        coord.y = m_FinalPos.y- (m_TileSize.y * line);

        Vector2 spawnCoords = DeterminateMiddle(coord);

        return spawnCoords;
    }

    private Vector2 DeterminateMiddle(Vector2 coord)
    {
       
        if (coord.x < 0)
        {
            coord.x = coord.x + m_TileSize.x / 2;
        }
        else
        {
            coord.x = coord.x - m_TileSize.x / 2;
        }

        if (coord.y < 0)
        {
            coord.y = coord.y + m_TileSize.y / 2;
        }
        else
        {
            coord.y = coord.y - m_TileSize.y / 2;
        }

        return coord;
    }
}
