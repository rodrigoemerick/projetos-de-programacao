﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem
{
    [SerializeField]
    private GameObject m_ObjectToPool;
    public GameObject ObjectToPool
    {
        get { return m_ObjectToPool; }
    }
    [SerializeField]
    private int m_AmountOfHoles;
    public int AmountOfHoles
    {
        get { return m_AmountOfHoles; }
    }
    [SerializeField]
    private bool m_ShouldExpand;
    public bool ShouldExpand
    {
        get { return m_ShouldExpand; }
    }
}

public class Pool : MonoBehaviour
{
    private static Pool m_SharedInstance;
    public static Pool SharedInstance
    {
        get { return m_SharedInstance; }
    }

    private List<ObjectPoolItem> m_ItemsToPool;

    private List<GameObject> m_PooledObjects;

    void Awake()
    {
        m_SharedInstance = this;
    }

    void Start()
    {
        m_PooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in m_ItemsToPool)
        {
            for (int i = 0; i < item.AmountOfHoles; ++i)
            {
                GameObject obj = (GameObject)Instantiate(item.ObjectToPool);
                obj.SetActive(false);
                m_PooledObjects.Add(obj);
            }
        }
    }

    public GameObject GetPooledObject(string gameObjectTag)
    {
        for (int i = 0; i < m_PooledObjects.Count; ++i)
        {
            if (!m_PooledObjects[i].activeInHierarchy && m_PooledObjects[i].tag == gameObjectTag)
            {
                return m_PooledObjects[i];
            }
        }
        foreach(ObjectPoolItem item in m_ItemsToPool)
        {
            if (item.ObjectToPool.tag == gameObjectTag)
            {
                if (item.ShouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.ObjectToPool);
                    obj.SetActive(false);
                    m_PooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }
}
