﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsMenu : BaseMenu
{
    [SerializeField] GameObject m_MenuGO;
    public override void EnterInMenu()
    {
        m_MenuGO.SetActive(true);
    }

    public override void ExitMenu()
    {
        m_MenuGO.SetActive(false);
    }

    public override void Confirm(string playerID)
    {
        
    }

    public override void MenuMovimentation(string movimentSide, string playerID)
    {
        
    }
}
