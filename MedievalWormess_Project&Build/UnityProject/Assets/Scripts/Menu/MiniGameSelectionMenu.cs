﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameSelectionMenu : BaseMenu
{
    [SerializeField] Selector[] m_Selector;
    string m_CurrentSelector;
    Dictionary<string, Selector> m_SelectorDict;

    public delegate void MiniMapSetupIsFinishHendler(string miniGame, string map);
    public event MiniMapSetupIsFinishHendler MiniMapSetUpIsFinish;

    private void Awake()
    {
        m_SelectorDict = new Dictionary<string, Selector>();
        SetSelectorsDictionary();
        NavegationButtonsDict = new Dictionary<string, NavegationButtons>();
        SetNavegationButtonsDict();
        
    }

    private void Update()
    {
        PreviousMenu = "player";
    }

    public override void Confirm(string playerID)
    {
        if (m_CurrentSelector == "mini")
        {
            m_SelectorDict[m_CurrentSelector].Confirm.SetActive(true);
            m_SelectorDict[m_CurrentSelector].SelectedItem(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
            m_SelectorDict[m_CurrentSelector].LowLight(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
            m_CurrentSelector = "map";
            m_SelectorDict[m_CurrentSelector].Confirm.SetActive(false);
            m_SelectorDict[m_CurrentSelector].HighLigth(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
        }
        else
        {
            m_SelectorDict[m_CurrentSelector].SelectedItem(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
            OnMiniMapSetupIsFinish(m_SelectorDict["mini"].CurrentItemID, m_SelectorDict["map"].CurrentItemID);
        }
    }

    public override void Back(string playerID)
    {
        if(m_CurrentSelector == "mini")
        {
            m_SelectorDict[m_CurrentSelector].SelectedItem(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
            base.Back(playerID);
        }
        else
        {
            m_SelectorDict[m_CurrentSelector].Confirm.SetActive(true);
            m_SelectorDict[m_CurrentSelector].LowLight(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
            m_CurrentSelector = "mini";
            m_SelectorDict[m_CurrentSelector].Confirm.SetActive(false);
            m_SelectorDict[m_CurrentSelector].SelectedItem(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
            m_SelectorDict[m_CurrentSelector].HighLigth(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
        }
    }

    public override void MenuMovimentation(string movimentSide, string playerID)
    {
        if (movimentSide == "left")
        {
            m_SelectorDict[m_CurrentSelector].ChageCurrentItemIndex("left");
        }
        else if (movimentSide == "right")
        {
            m_SelectorDict[m_CurrentSelector].ChageCurrentItemIndex("right");
        }
    }

    public override void EnterInMenu()
    {
        foreach (string key in m_SelectorDict.Keys)
        {
            m_SelectorDict[key].gameObject.SetActive(true);
            m_SelectorDict[key].Confirm.SetActive(true);
        }
        m_CurrentSelector = "mini";
        m_SelectorDict[m_CurrentSelector].CurrentItemIndex = 0;
        m_SelectorDict[m_CurrentSelector].Confirm.SetActive(false);
        m_SelectorDict[m_CurrentSelector].HighLigth(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
    }

    public override void ExitMenu()
    {
        foreach (string key in m_SelectorDict.Keys)
        {
            m_SelectorDict[key].Confirm.SetActive(true);
            m_SelectorDict[key].gameObject.SetActive(false);
        }
        m_SelectorDict[m_CurrentSelector].LowLight(m_SelectorDict[m_CurrentSelector].CurrentItemIndex);
    }

    void SetSelectorsDictionary()
    {
        for (int i = 0; i < m_Selector.Length; i++)
        {
            m_SelectorDict.Add(m_Selector[i].Reference, m_Selector[i]);
        }
    }

    void OnMiniMapSetupIsFinish(string minigame, string map)
    {
        if (MiniMapSetUpIsFinish != null)
            MiniMapSetUpIsFinish(minigame, map);
    }
}
