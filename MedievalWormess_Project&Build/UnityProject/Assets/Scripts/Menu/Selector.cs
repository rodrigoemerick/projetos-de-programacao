﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour
{
    [SerializeField] GameObject[] m_Itens;
    [SerializeField] GameObject[] m_SelectedImg;
    [SerializeField] GameObject m_Confirm;

    [SerializeField] float m_SizeAmount;

    string m_CurrentItemID;
    public string CurrentItemID
    {
        get => m_CurrentItemID;
    }
    public GameObject Confirm
    {
        get => m_Confirm;
        set => m_Confirm = value;
    }

    [SerializeField] string m_Reference;
    public string Reference
    {
        get => m_Reference;
    }

    int m_CurrentItemIndex = 0;
    public int CurrentItemIndex
    {
        get => m_CurrentItemIndex;
        set => m_CurrentItemIndex = value;
    }
    int m_PreviousItemIndex;

    

    public void ChageCurrentItemIndex(string side)
    {
        m_PreviousItemIndex = m_CurrentItemIndex;
        if (side == "left")
        {
            m_CurrentItemIndex--;
            if (m_CurrentItemIndex == -1)
            {
                m_CurrentItemIndex = m_Itens.Length - 1;
            }
        }
        else
        {
            m_CurrentItemIndex++;
            if (m_CurrentItemIndex == m_Itens.Length)
            {
                m_CurrentItemIndex = 0;
            }
        }
        HighLigth(m_CurrentItemIndex);
        LowLight(m_PreviousItemIndex);
    }

    public void SelectedItem(int index)
    {
        m_SelectedImg[index].SetActive(!m_SelectedImg[index].activeSelf);
    }

    public void HighLigth(int index)
    {
        m_CurrentItemID = m_Itens[m_CurrentItemIndex].gameObject.name;
        Vector3 temp2 = m_Itens[index].GetComponent<RectTransform>().localScale;
        temp2.x += m_SizeAmount;
        temp2.y += m_SizeAmount;
        m_Itens[index].GetComponent<RectTransform>().localScale = temp2;
    }

    public void LowLight(int index)
    {
        Vector3 temp = m_Itens[index].GetComponent<RectTransform>().localScale;
        temp.x -= m_SizeAmount;
        temp.y -= m_SizeAmount;
        m_Itens[index].GetComponent<RectTransform>().localScale = temp;
    }
}
