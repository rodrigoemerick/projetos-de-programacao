﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinSelector : MonoBehaviour
{
    [SerializeField] Image m_PlayerFace;
    [SerializeField] Image m_Confirm;
    [SerializeField] Image m_Backgroud;
    [SerializeField] Sprite[] m_BackgroundSprite;
    [SerializeField] Sprite[] m_ConfirmSprite;
    [SerializeField] Sprite[] m_PlayerFaceSprite;

    [SerializeField] string m_PlayerReference;
    public string PlayerReference
    {
        get => m_PlayerReference;
    }

    bool m_PlayerConected;
    public bool PlayerConected
    {
        get => m_PlayerConected;
    }

    bool m_PlayerReady;
    public bool PlayerReady
    {
        get => m_PlayerReady;
    }

    public void ConnectPLayer(bool status)
    {
        m_PlayerConected = status;
    }

    public void PlayerIsReady(bool status)
    {
        m_PlayerReady = status;
    }

    private void Update()
    {
        if (m_PlayerConected)
        {
            m_PlayerFace.sprite = m_PlayerFaceSprite[1];
            m_Backgroud.sprite = m_BackgroundSprite[1];   

            if (PlayerReady)
            {
                m_Confirm.sprite = m_ConfirmSprite[1];
            }
            else
            {
                m_Confirm.sprite = m_ConfirmSprite[0];
            }
        }
        else
        {
            m_Backgroud.sprite = m_BackgroundSprite[0];
            m_PlayerFace.sprite = m_PlayerFaceSprite[0];
            m_Confirm.sprite = m_ConfirmSprite[2];
        }
    }
}
