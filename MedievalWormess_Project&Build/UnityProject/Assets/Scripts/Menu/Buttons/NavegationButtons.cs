﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavegationButtons : MonoBehaviour
{
    string m_ButtonID;
    public string ButtonID
    {
        get => m_ButtonID;
        set => m_ButtonID = value;
    }

    string[] m_Keys = { "left", "right", "up", "down" };
    protected string[] Keys
    {
        get => m_Keys;
    }

    [SerializeField] protected NavegationButtons[] m_Neighbors;

    protected Dictionary<string, string> m_NeighborsButtons;
    public Dictionary<string, string> NeighborsButtons
    {
        get => m_NeighborsButtons;
    }

    private void Awake()
    {
        m_NeighborsButtons = new Dictionary<string, string>();
        AddNeighbors(m_Neighbors);
    }

    protected void AddNeighbors(NavegationButtons[] neighbors)
    {
        for (int i = 0; i < neighbors.Length; i++)
        {
            if (neighbors[i] != null)
            {
                m_NeighborsButtons.Add(m_Keys[i], neighbors[i].ButtonID);
            }
            else
            {
                m_NeighborsButtons.Add(m_Keys[i], "0");
            }
        }
    }

    public virtual void OnHighLite()
    {
      
    }

    public virtual void OnChange()
    {
      
    }

    public virtual void OnClick()
    {
         this.gameObject.GetComponent<Button>().onClick.Invoke();
    }
}
