﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] GeneralInfoSO m_GGISO;

    [SerializeField] string m_InitialMenu;
    string m_CurrentMenu;

    [SerializeField] BaseMenu[] m_GameMenus;

    Dictionary<string, BaseMenu> m_GameMenuDict;
    
    [Header("Audios")]
    [SerializeField] AudioSource m_ClickAudio;
    [SerializeField] AudioSource m_ClickBackAudio;

    public delegate void SetupIsFinishHandler();
    public event SetupIsFinishHandler SetUpIsFinish;

    public delegate void CloseGameSelectedHandler();
    public event CloseGameSelectedHandler CloseGameSelected;

    bool m_BackCommand;
    private void Awake()
    {
        m_GameMenuDict = new Dictionary<string, BaseMenu>();
    }
    
    private void Start()
    {
        for (int i = 0; i < m_GameMenus.Length; i++)
        {
            m_GameMenuDict.Add(m_GameMenus[i].MenuID, m_GameMenus[i]);
        }
        m_CurrentMenu = m_InitialMenu;
        ChangeCurrentMenu(m_CurrentMenu);
    }

    public void ChangeCurrentMenu(string newMenu)
    {
        m_GameMenuDict[m_CurrentMenu].ExitMenu();
        
        if (!m_BackCommand)
        {
            m_GameMenuDict[newMenu].PreviousMenu = m_CurrentMenu;
        }
        m_CurrentMenu = newMenu;
        m_GameMenuDict[m_CurrentMenu].EnterInMenu();

        if(m_GameMenuDict[newMenu].gameObject.TryGetComponent<PlayerSelectionMenu>(out PlayerSelectionMenu temp))
        {
            temp.PlayerSetUpIsFinish += OnPlayerSetupIsFinish;
        }

        if (m_GameMenuDict[newMenu].gameObject.TryGetComponent<MiniGameSelectionMenu>(out MiniGameSelectionMenu temp2))
        {
            temp2.MiniMapSetUpIsFinish += OnMiniMapSetupIsFinish;
        }
    }

    public void ReturnToLastMenu()
    {
        if (m_GameMenuDict[m_CurrentMenu].PreviousMenu != " ")
        {
            m_BackCommand = true;
            ChangeCurrentMenu(m_GameMenuDict[m_CurrentMenu].PreviousMenu);
            m_BackCommand = false;
            m_ClickBackAudio.Play();
        }
    }

    public void ChangeConnectedPlayers(int[] playersConnect)
    {
        m_GameMenuDict["player"].GetComponent<PlayerSelectionMenu>().ChangeConnectedPlayers(playersConnect);
    }

    void OnSetupIsFinish()
    {
        if (SetUpIsFinish != null)
            SetUpIsFinish();
    }

    void OnPlayerSetupIsFinish(int numberOfPlayers, int[] playersConnect)
    {
        m_GGISO.numOfPlayersConnect = numberOfPlayers;
        m_GGISO.playersConnect = playersConnect;
        ChangeCurrentMenu("mg");
    }

    void OnMiniMapSetupIsFinish(string miniGame, string map)
    {
        m_GGISO.selectedMiniGame = miniGame;
        m_GGISO.selectedMap = map;
        OnSetupIsFinish();
    }

    public void OnCloseGameSelect()
    {
        if (CloseGameSelected != null)
            CloseGameSelected();
    }


    public void OnConfirmCommand(string playerIndex)
    {
        m_GameMenuDict[m_CurrentMenu].Confirm(playerIndex);
        m_ClickAudio.Play();
    }

    public void OnDenyCommand(string playerIndex)
    {
        m_GameMenuDict[m_CurrentMenu].Back(playerIndex);
    }

    public void OnMoveCommand(string side, string playerIndex)
    {
        m_GameMenuDict[m_CurrentMenu].MenuMovimentation(side, playerIndex);
    }
}
