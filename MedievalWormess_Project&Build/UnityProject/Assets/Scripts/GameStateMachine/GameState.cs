﻿using System.Collections;

public abstract class GameState<T>
{
    public abstract void Enter();
    public abstract void Update();
    public abstract void Exit();
    public abstract IEnumerator ChangeState();

    protected T parent;

    public GameState(T Parent)
    {
        parent = Parent;
    }
}

