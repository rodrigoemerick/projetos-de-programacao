﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateMiniGame : GameState<GameManager>
{
    public delegate void StateTransitionIsFinishHandler();
    public event StateTransitionIsFinishHandler StateTransitionIsFinish;

    GeneralInfoSO m_GGISO;

    bool m_SetupIsFinish;
    public StateMiniGame(GameManager parent, GeneralInfoSO general) : base(parent)
    {
        m_GGISO = general;
    }

    public override IEnumerator ChangeState()
    {
        yield return new WaitForSeconds(2.0f);
        parent.ChangeState("tmg");
    }

    public override void Enter()
    {
        SceneManager.LoadScene("Game");
    }

    public override void Exit()
    {
        GameObject.Find("MiniGameManager").GetComponent<MiniGameManager>().GameIsFinish += parent.OnGameIsFinish;
        StateTransitionIsFinish -= GameObject.Find("MiniGameManager").GetComponent<MiniGameManager>().OnsStateTransitionIsFinish;
        SceneManager.LoadScene("Menu");
        m_SetupIsFinish = false;
    }

    public override void Update()
    {
        if (!m_SetupIsFinish)
        {
            
            MiniGameManager temp = GameObject.Find("MiniGameManager").GetComponent<MiniGameManager>();
            temp.PlayerCommanders = parent.PlayerCommanders;
            temp.GameIsFinish += parent.OnGameIsFinish;
            StateTransitionIsFinish += temp.OnsStateTransitionIsFinish;

            for (int i = 0; i < m_GGISO.numOfPlayersConnect; i++)
            {
                temp.PlayerCommanders[i].ChangeControlMap(false, true);
            }

            m_SetupIsFinish = true;

            OnsStateTransitionIsFinish();
        }

        if (parent.GameIsFinish)
        {
            parent.StartCoroutine(ChangeState());
        }
    }

    void OnsStateTransitionIsFinish()
    {
        StateTransitionIsFinish?.Invoke();
    }
}
