﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager : MonoBehaviour
{
    [SerializeField] GeneralInfoSO m_GGISO;
    [SerializeField] GameplayPositionsSO m_GPSO;
    [SerializeField] PointsSO m_PSO;

    PlayerCommander[] m_PlayerCommanders;
    public PlayerCommander[] PlayerCommanders 
    {
        get => m_PlayerCommanders;
        set => m_PlayerCommanders = value; 
    }

    [SerializeField] UIController m_UIConntroller;

    [SerializeField] GameObject m_GameCamera;

    [SerializeField] GameObject[] m_Players;
    public GameObject[] Players
    {
        get => m_Players;
    }

    [SerializeField] GameObject[] m_MiniGameObj;
    [SerializeField] GameObject[] m_Maps;

    [SerializeField] string[] m_MiniGamesNames;
    [SerializeField] AudioSource m_ArenaTrack;

    PlayerClass[] m_Temps;

    Dictionary<string, GameObject> m_MiniGameManagersObjDict;
    Dictionary<string, GameObject> m_MapsDict;
    Dictionary<string, Vector3> m_CameraPositionDict;
    Dictionary<string, Vector3> m_PlayersPositionDict;

    public delegate void GameSetupHandeler();
    public event GameSetupHandeler GameSetup;

    public delegate void GameIsFinishHandeler();
    public event GameIsFinishHandeler GameIsFinish;

    private void Awake()
    {
        m_MiniGameManagersObjDict = new Dictionary<string, GameObject>();
        m_MapsDict = new Dictionary<string, GameObject>();
        m_CameraPositionDict = new Dictionary<string, Vector3>();
        m_PlayersPositionDict = new Dictionary<string, Vector3>();
        m_PlayerCommanders = new PlayerCommander[4];
    }

    private void Start()
    {
        CreateDictionarys();
    }

    void CreateDictionarys()
    {
        for(int i = 0; i < m_MiniGamesNames.Length; i++)
        {
            m_MiniGameManagersObjDict.Add(m_MiniGamesNames[i], m_MiniGameObj[i]);
        }

        for (int i = 0; i < m_Maps.Length; i++)
        {
            m_MapsDict.Add(m_GPSO.mapsNames[i], m_Maps[i]);
        }
    }
   
    void SetMap()
    {
        m_MapsDict[m_GGISO.selectedMap].gameObject.SetActive(true);
        m_GameCamera.gameObject.transform.position = ConfigCamPos(m_GGISO.selectedMap);
    }

    void SetPlayers()
    {
        RemovePlayersOfTheGame();
        m_Temps = new PlayerClass[m_GGISO.numOfPlayersConnect];
        for (int i = 0; i < m_GGISO.numOfPlayersConnect; i++)
        {
            m_PSO.playerIsInGame[i] = true;
            m_Players[i].transform.position = ConfigPlayerPos(m_GGISO.selectedMap, i);
            m_Temps[i] = ChangePlayerClass(m_GGISO.selectedMiniGame, m_Players[i]);
            m_Temps[i].MyIndex = i;
            m_Temps[i].RespawnPosition = ConfigPlayerPos(m_GGISO.selectedMap, i);
            m_Players[i].SetActive(true);
            m_PlayerCommanders[i].Smash += m_Temps[i].OnSmash;
            m_PlayerCommanders[i].MoveHorizontal += m_Temps[i].OnMoveHorizontal;
            m_PlayerCommanders[i].MoveVertical += m_Temps[i].OnMoveVertical;
            m_Players[i].GetComponent<SkinCare>().PlayerID = i;
        }
    }

    void SetMiniGame()
    {
        m_MiniGameManagersObjDict[m_GGISO.selectedMiniGame].SetActive(true);
        m_MiniGameManagersObjDict[m_GGISO.selectedMiniGame].GetComponent<MiniGameController>().NumOfPlayers = m_GGISO.numOfPlayersConnect;
        m_MiniGameManagersObjDict[m_GGISO.selectedMiniGame].GetComponent<MiniGameController>().Players = Players;
        GameSetup += m_MiniGameManagersObjDict[m_GGISO.selectedMiniGame].GetComponent<MiniGameController>().OnGameSetup;
        m_MiniGameManagersObjDict[m_GGISO.selectedMiniGame].GetComponent<MiniGameController>().GameIsFinish += OnGameIsFinish;
    }

    void SetUI()
    {
        m_UIConntroller.ActiveUI();
    }

    PlayerClass ChangePlayerClass(string mini, GameObject currentPlayer)
    {
        PlayerSmashGame playerSmashGame = currentPlayer.GetComponent<PlayerSmashGame>();
        PlayerPotatoGame playerPotatoGame = currentPlayer.GetComponent<PlayerPotatoGame>();
        PlayerWormGame playerWormGame = currentPlayer.GetComponent<PlayerWormGame>();

        switch (mini)
        {
            case "smashery":
                playerSmashGame.enabled = !playerSmashGame.enabled;
                return playerSmashGame;

            case "potato":
                playerPotatoGame.enabled = !playerPotatoGame.enabled;
                return playerPotatoGame;

            case "worms":
                playerWormGame.enabled = !playerWormGame.enabled;
                return playerWormGame;
        }

        return playerSmashGame;
    }

    void OnGameSetup()
    {
        ConfigArenaTrackClip(m_GGISO.selectedMap);
        if (!m_ArenaTrack.isPlaying)
            m_ArenaTrack.Play();
        GameSetup?.Invoke();
    }

    void OnGameIsFinish()
    {
        m_UIConntroller.ActiveLaderboard();
        StartCoroutine(ChangeScene());
    }

    public void OnsStateTransitionIsFinish()
    {
        SetMap();
        SetPlayers();
        SetMiniGame();
        SetUI();
        OnGameSetup();
    }

    void RemoveCommands()
    {
        for (int i = 0; i < m_GGISO.numOfPlayersConnect; ++i)
        {
            m_PlayerCommanders[i].Smash -= m_Temps[i].OnSmash;
            m_PlayerCommanders[i].MoveHorizontal -= m_Temps[i].OnMoveHorizontal;
            m_PlayerCommanders[i].MoveVertical -= m_Temps[i].OnMoveVertical;
        }
    }

    void ConfigArenaTrackClip(string map)
    {
        switch (map)
        {
            case "forest":
                m_ArenaTrack.clip = m_GPSO.arenasTracks[0];
                break;
            case "ice":
                m_ArenaTrack.clip = m_GPSO.arenasTracks[1];
                break;
            case "fire":
                m_ArenaTrack.clip = m_GPSO.arenasTracks[2];
                break;
        }
    }

    void RemovePlayersOfTheGame()
    {
        for (int i = 0; i < 4; i++)
        {
            m_PSO.playerIsInGame[i] = false;
        }
    }

    Vector3 ConfigCamPos(string map)
    {
        if (map == "forest")
            return m_GPSO.forestCamPos;
        else if (map == "ice")
            return m_GPSO.iceCamPos;
        else if (map == "fire")
            return m_GPSO.fireCamPos;

        return Vector3.zero;
    }

    Vector3 ConfigPlayerPos(string map, int index)
    {
        if (map == "forest")
            return m_GPSO.forestPlayersPos[index];
        else if (map == "ice")
            return m_GPSO.icePlayersPos[index];
        else if (map == "fire")
            return m_GPSO.firePlayersPos[index];

        return Vector3.zero;
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2.0f);
        GameIsFinish?.Invoke();
        if (m_ArenaTrack.isPlaying)
            m_ArenaTrack.Stop();
        RemoveCommands();
    }
}