﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    float m_CurrentTime;
    public float CurrentTime { get => m_CurrentTime; }

    [SerializeField] Text m_TimerText;

    public delegate void OnTimerFinishHandeler();
    public event OnTimerFinishHandeler TimeFinish;

    void Start()
    {
        m_TimerText.text = (m_CurrentTime - 1).ToString();
    }

    public void StartTimer(float time)
    {
        m_CurrentTime = time;
        StartCoroutine(TimerTime());
    }
    private void Update()
    {
        m_TimerText.text = m_CurrentTime.ToString();
    }

    public void OnTimerFinish()
    {
        TimeFinish?.Invoke();
    }

    IEnumerator TimerTime()
    {  
        while(m_CurrentTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            m_CurrentTime--;
        }

        OnTimerFinish();
    }

}