﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPotatoGame : PlayerClass
{
    private bool m_StartsWithPotato;
    private bool m_IsWithPotato;
    public bool IsWithPotato { get => m_IsWithPotato; set => m_IsWithPotato = value; }
    private bool m_HasExploded;
    public bool HasExploded { get => m_HasExploded; set => m_HasExploded = value; }

    [SerializeField] private GameObject m_PotatoPlace;
    private Vector3 m_PotatoPosition;
    public Vector3 PotatoPosition { get => m_PotatoPlace.transform.position; }

    protected override void Awake()
    {
        base.Awake();
        m_PotatoPosition = transform.position + new Vector3(0.0f, 2.0f, 0.0f);
        
        if (m_MyIndex == 0)
        {
            m_StartsWithPotato = true;
        }

        GameObject go = transform.Find("StunAudio").gameObject;
        go.SetActive(false);
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
        
        if (m_StartsWithPotato)
        {
            m_IsWithPotato = true;
            m_StartsWithPotato = false;
        }

        m_Animator.SetBool("StunPotato", m_HasExploded);
        
        if (m_HasExploded)
        {
            m_Speed = 0.0f;
        }
        else
        {
            m_Speed = m_TempSpeed;
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (enabled)
        {
            if (other.gameObject.CompareTag("RespawnFall"))
            {
                StartCoroutine(RespawnAfterFall());
            }

            if (other.gameObject.CompareTag("Hammer"))
            {
                PlayerPotatoGame whoHitMe = other.gameObject.GetComponent<HammerPotatoParent>().Parent.GetComponent<PlayerPotatoGame>();
                if (whoHitMe.IsWithPotato)
                {
                    whoHitMe.IsWithPotato = false;
                    m_IsWithPotato = true;
                }

                if (!m_SmashHit.isPlaying)
                    m_SmashHit.Play();
            }
        }
    }

    protected override void OnCollisionEnter(Collision other)
    {
        if (enabled)
        {
            base.OnCollisionEnter(other);
        }
    }
}