﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass : MonoBehaviour
{
    //Movement
    [Header("Movement")]
    [SerializeField]
    protected float m_Speed;
    protected float m_TempSpeed;
    [SerializeField]
    private float m_SpeedAtSmash;
    [SerializeField]
    private float m_TimeToResetAfterFall;
    [SerializeField]
    private float m_SmashAnimTime;
    [SerializeField]
    private float m_TimeToResetSmash;
    [SerializeField]
    protected Animator m_Animator;

    private string m_Name;
    public string Name { get => m_Name; }

    protected int m_MyIndex;
    public int MyIndex
    {
        get => m_MyIndex;
        set => m_MyIndex = value;
    }

    private bool m_IsIdle;
    private bool m_IsWalking;
    public bool IsWalking { get => m_IsWalking; }
    private bool m_IsSmashing;
    private bool m_CanSmash;

    private Vector3 m_RespawnPosition;
    public Vector3 RespawnPosition { set => m_RespawnPosition = value; }
    private Vector3 m_FacingRotation;
    private Vector3 m_InputValues;

    protected Rigidbody m_Rigidbody;

    private BoxCollider m_Collider;

    [Header("Hammer")]
    [SerializeField]
    private GameObject m_PlayerGO;
    [SerializeField]
    private GameObject m_HammerWithCollider;

    //Audios
    [Header("Audios")]
    [SerializeField]
    protected AudioSource m_SmashAction;
    [SerializeField]
    protected AudioSource m_SmashHit;

    float m_MoveValueX;
    float m_MoveValueZ;

    protected virtual void Awake()
    {
        //m_Animator = m_PlayerGO.GetComponent<Animator>();
        m_Collider = m_HammerWithCollider.GetComponent<BoxCollider>();
        m_Rigidbody = GetComponent<Rigidbody>();

        ControlCollider(false);

        m_InputValues = Vector3.zero;

        m_Name = "Player " + (m_MyIndex + 1);

        m_CanSmash = true;

        m_TempSpeed = m_Speed;

        //m_RespawnPosition = transform.position;
    }

    protected virtual void Start()
    {
        //m_Animator = m_PlayerGO.GetComponent<Animator>();
    }

    protected virtual void Update()
    {
        //Velocidade quando se está ou não no estado Smash
        if (m_IsSmashing)
        {
            m_Speed = m_SpeedAtSmash;
        }
        else
        {
            m_Speed = m_TempSpeed;
        }

        //Garantir que o personagem está olhando para frente
        m_FacingRotation = Vector3.Normalize(m_InputValues);
        if (m_FacingRotation != Vector3.zero) //prevents the Debug "Look rotation viewing vector is zero"
        {
            transform.forward = m_FacingRotation;
        }

        //Animação de movimentação
        if (m_InputValues != Vector3.zero)
        {
            m_IsWalking = true;
            m_IsIdle = false;
        }
        else
        {
            m_IsWalking = false;
            m_IsIdle = true;
        }

        //Prevenir que o personagem não ande mais rápido nas diagonais
        if (Mathf.Abs(m_InputValues.x) > Mathf.Epsilon && Mathf.Abs(m_InputValues.z) > Mathf.Epsilon)
        {
            m_Speed *= 0.7071f;
        }

        //Movimentação
        m_InputValues.x = m_MoveValueX * m_Speed * Time.deltaTime;
        m_InputValues.z = m_MoveValueZ * m_Speed * Time.deltaTime;


        //bools for animator
        m_Animator.SetBool("Idle", m_IsIdle);
        m_Animator.SetBool("Walk", m_IsWalking);
    }

    protected virtual void FixedUpdate()
    {
        m_Rigidbody.MovePosition(transform.position + m_InputValues);
    }

    public virtual void Smash()
    {
        if (m_CanSmash && !m_IsSmashing)
        {
            m_Animator.SetTrigger("Smash");
            m_IsSmashing = true;
            StartCoroutine(CanSmashAgain());
            StartCoroutine(ActivateHammerColliderDuringAnimation());
            StartCoroutine(SmashDuration());
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.CompareTag("RespawnFall"))
        //{
        //    StartCoroutine(RespawnAfterFall());
        //}

        if (other.gameObject.CompareTag("Hammer"))
        {
            if (!m_SmashHit.isPlaying)
                m_SmashHit.Play();
        }
    }

    protected virtual void OnCollisionEnter(Collision other)
    {

    }

    public void ControlCollider(bool isActivated)
    {
        m_Collider.GetComponent<BoxCollider>().enabled = isActivated;
    }

    public void OnSmash()
    {
        Smash();
    }

    public void OnMoveHorizontal(float moveValue)
    {
        m_MoveValueX = moveValue;
    }

    public void OnMoveVertical(float moveValue)
    {
        m_MoveValueZ = moveValue;
    }

    //Coroutines
    IEnumerator CanSmashAgain()
    {
        yield return new WaitForSeconds(m_TimeToResetSmash);
        m_CanSmash = true;
    }

    IEnumerator SmashDuration()
    {
        yield return new WaitForSeconds(m_SmashAnimTime);
        m_IsSmashing = false;
        ControlCollider(false);
    }

    IEnumerator ActivateHammerColliderDuringAnimation()
    {
        yield return new WaitForSeconds(m_SmashAnimTime / 3);
        if (!m_SmashAction.isPlaying)
            m_SmashAction.Play();
        ControlCollider(true);
    }

    protected IEnumerator RespawnAfterFall()
    {
        yield return new WaitForSeconds(m_TimeToResetAfterFall);
        transform.position = m_RespawnPosition;
    }
}