﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSmashGame : PlayerClass
{
    [SerializeField]
    private float m_KnockBack;

    private bool m_HasFallen;
    public bool HasFallen { get => m_HasFallen; }

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (enabled)
        {
            if (other.gameObject.CompareTag("RespawnFall"))
            {
                m_HasFallen = true;
            }

            Vector3 moveDirection = Vector3.zero;
            if (other.gameObject.CompareTag("Hammer"))
            {
                moveDirection = m_Rigidbody.transform.position - other.transform.position;
                m_Rigidbody.AddForce(new Vector3(moveDirection.x, 0.0f, moveDirection.z) * m_KnockBack, ForceMode.Impulse);
            }
        }
    }

    protected override void OnCollisionEnter(Collision other)
    {
        
    }
}
