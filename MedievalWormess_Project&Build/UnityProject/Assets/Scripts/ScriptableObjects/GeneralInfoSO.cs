﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "Scriptable Objects/General Game Info", order = 2)]
public class GeneralInfoSO : ScriptableObject
{
    public string[] mapsNames;
    public string[] miniGamesNames;
    public string selectedMap;
    public string selectedMiniGame;
    public int numOfPlayersConnect;
    public int[] playersConnect;
    public Color[] playersColors;
}

