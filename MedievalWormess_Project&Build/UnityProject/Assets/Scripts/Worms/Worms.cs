﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worms : MonoBehaviour
{
    [SerializeField] GeneralInfoSO m_GGISO;
    [SerializeField] WormsInfoSO m_WISO;
    [SerializeField] PointsSO m_PSO;

    [SerializeField] SkinnedMeshRenderer m_Skin;

    Vector3 m_DesactivatedPosition;
    Vector3 m_ActivatedPosition;
    float m_JourneyDistance;
    [SerializeField]bool m_IsActive;
    public bool IsActive { get => m_IsActive; }

    void Start()
    {
        if (m_GGISO.selectedMiniGame == "worms")
        {
            m_DesactivatedPosition = transform.position;
            m_ActivatedPosition = new Vector3(m_DesactivatedPosition.x, m_DesactivatedPosition.y + m_WISO.upDistance, m_DesactivatedPosition.z);
            m_JourneyDistance = Vector3.Distance(m_DesactivatedPosition, m_ActivatedPosition);
        }
    }

    void Update()
    {
        if (m_GGISO.selectedMiniGame == "worms")
        {
            WormMovement(m_IsActive);
        }
    }

    public void Appear(Color color, float time)
    {
        m_Skin.material.color = color;
        m_IsActive = true;
        WormMovement(m_IsActive);
        StartCoroutine(ChangeWormState(time));
    }

    public void Disappear()
    {
        StopCoroutine(ChangeWormState(0.0f));
        m_IsActive = !m_IsActive;
    }

    IEnumerator ChangeWormState(float time)
    {
        yield return new WaitForSeconds(time);
        m_IsActive = !m_IsActive;
        WormMovement(m_IsActive);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Hammer"))
        {
            int indx = other.GetComponent<HammerPotatoParent>().Parent.GetComponent<PlayerWormGame>().MyIndex;
            MarkPoint(indx);
            Disappear();
        }
    }

    void WormMovement(bool active)
    {
        if (active)
        {
            transform.position = Vector3.Lerp(transform.position, m_ActivatedPosition, m_WISO.smooth * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, m_DesactivatedPosition, m_WISO.smooth * Time.deltaTime);
        }
    }

    void MarkPoint(int indx)
    {
        int color = CheckWormsColor();

        if (indx == color)
        {
            m_PSO.playerPoints[indx] += 2;
        }
        else
        {
            m_PSO.playerPoints[indx]++;
        }       
    }

    int CheckWormsColor() 
    {
        for (int i = 0; i < m_GGISO.playersColors.Length; i++)
        {
            if(m_Skin.material.color == m_GGISO.playersColors[i])
            {
                return i;
            }
        }

        return 4;
    }
}